﻿#target indesign

//@include "./json.jsx"
//@include "./json2.js"
//@include "./console.jsxlib"
var console = new ConsoleLog();
var myScript = $.fileName;  // 사용하지 않는것 같음
var red_global = this;
var cur_doc = null;
var red_version = "0.2.5";
var build_date = "20181011";
var DEBUG = false;
var REMOVE_PATHS = true;
var TEXT_LABEL_KEY = "TextKey";  // 사용하지 않음
var rootPath = File($.fileName).parent.parent+"/"; // 사용하지 않는것같음
var MAX_PATHS_LIMIT = 0; // 0 : unlimited
var _productCode = ""
var variableType = ""
var pngData = []
var backgroundType = ''

if(typeof myMainMenu != "undefined") {
    var obj2svgFile = new File(myParentFolder+"/REDObjToSVG.jsx");
    $.evalFile(obj2svgFile);
}
// 소수점 자릿수 연동 Kraken 20190704
decimalPointLength  = "" // 소수점 자릿수 연동 Kraken 20190704
whiteLayerVisible   =  false // 화이트 숨김여부 Kraken
isStickerSelectable =  false
isStickerMovable    =  false
isTextSelectable    =  false
isTextMovable       =  false
isCellMovable       =  false

function capitalization(val)
{
    switch(val) {
        case Capitalization.NORMAL :
            return "NORMAL";
        case Capitalization.SMALL_CAPS:
            return "SMALL_CAPS";
        case Capitalization.ALL_CAPS:
            return "ALL_CAPS";
        case Capitalization.CAP_TO_SMALL_CAP:
            return "CAP_TO_SMALL_CAP";
         }
    }

function position(val)
{
    switch(val) {
        case Position.NORMAL:
            return "NORMAL";
        case Position.SUPERSCRIPT:
            return "SUPERSCRIPT";
        case Position.SUBSCRIPT:
            return "SUBSCRIPT";
        case Position.OT_SUPERSCRIPT:
            return "OT_SUPERSCRIPT";
        case Position.OT_SUBSCRIPT:
            return "OT_SUBSCRIPT";
        case Position.OT_NUMERATOR:
            return "OT_NUMERATOR";
        case Position.OT_DENOMINATOR:
            return "OT_DENOMINATOR";
        }
    }

function justification(val)
{
    switch(val) {
        case Justification.LEFT_ALIGN:
            return "LEFT_ALIGN";
        case Justification.CENTER_ALIGN:
            return "CENTER_ALIGN";
        case Justification.RIGHT_ALIGN:
            return "RIGHT_ALIGN";
        case Justification.LEFT_JUSTIFIED:
            return "LEFT_JUSTIFIED";
        case Justification.RIGHT_JUSTIFIED:
            return "RIGHT_JUSTIFIED";
        case Justification.CENTER_JUSTIFIED:
            return "CENTER_JUSTIFIED";
        case Justification.FULLY_JUSTIFIED:
            return "FULLY_JUSTIFIED";
        case Justification.TO_BINDING_SIDE:
            return "TO_BINDING_SIDE";
        case Justification.AWAY_FROM_BINDING_SIDE:
            return "AWAY_FROM_BINDING_SIDE";
        }
    }

function image_flip(val)
{
     switch(val) {
        case Flip.NONE:
            return "NONE";
        case Flip.HORIZONTAL:
            return "HORIZONTAL";
        case Flip.VERTICAL:
            return "VERTICAL";
        case Flip.HORIZONTAL_AND_VERTICAL:
            return "HORIZONTAL_AND_VERTICAL";
        case Flip.BOTH:
            return "BOTH";
         }
    }

function pageitem_flip(val)
{
     switch(val) {
        case Flip.NONE:
            return "NONE";
        case Flip.HORIZONTAL:
            return "HORIZONTAL";
        case Flip.VERTICAL:
            return "VERTICAL";
        case Flip.HORIZONTAL_AND_VERTICAL:
            return "HORIZONTAL_AND_VERTICAL";
        case Flip.BOTH:
            return "BOTH";
         }
    }

// flipPathEffect
function flip_values(val)
{
    switch(val) {
        case FlipValues.NOT_FLIPPED:
            return "NOT_FLIPPED";
        case FlipValues.FLIPPED:
            return "FLIPPED";
        case FlipValues.UNDEFINED_FLIP_VALUE:
            return "UNDEFINED_FLIP_VALUE";
    }
}

// pathEffect
function text_path_effects(val)
{
    switch(val) {
        case TextPathEffects.RAINBOW_PATH_EFFECT:
            return "RAINBOW_PATH_EFFECT";
        case TextPathEffects.SKEW_PATH_EFFECT:
            return "SKEW_PATH_EFFECT";
        case TextPathEffects.RIBBON_PATH_EFFECT:
            return "RIBBON_PATH_EFFECT";
        case TextPathEffects.STAIR_STEP_PATH_EFFECT:
            return "STAIR_STEP_PATH_EFFECT";
        case TextPathEffects.GRAVITY_PATH_EFFECT:
            return "GRAVITY_PATH_EFFECT";
    }
}

// pathAlignment
function path_type_alignments(val)
{
    switch(val) {
        case PathTypeAlignments.TOP_PATH_ALIGNMENT:
            return "TOP_PATH_ALIGNMENT";
        case PathTypeAlignments.BOTTOM_PATH_ALIGNMENT:
            return "BOTTOM_PATH_ALIGNMENT";
        case PathTypeAlignments.CENTER_PATH_ALIGNMENT:
            return "CENTER_PATH_ALIGNMENT";
    }
}

function path_type(val)
{
    switch(val) {
        case PathType.OPEN_PATH:
            return "OPEN_PATH";
        case PathType.CLOSED_PATH:
            return "CLOSED_PATH";
    }
}
//////////////////////////////////////////////////  Util Fuctions
function fixedfloat(val)
{

        // template.json에 소수점 포함으로 변경 20190722    
        // var str = val.toFixed (decimalPointLength);
        // var retval = Number(str);
        // return retval;

        return val
}
function mm2pt(mm)
{
    var ret = mm * 2.834646;
    return ret;
    }


function Dwriteln(str)
{
    if(DEBUG == true) {
        $.writeln(str);
    }
}


function dpi_for_thumb_size(w, h)
{
    var width = mm2pt(w);
    var height = mm2pt(h);
    var max = width > height ? width : height;
    var ret_dpi  =  (800 *72.0) /  max ;
    return ret_dpi;
    }

function get_file_ext(path)
{
        var file_comp = path.split(".");
        var ext = file_comp[file_comp.length-1].toLocaleLowerCase();
        return ext;
    }

function zeroFilledStrFromNum(num)
{

    if(num < 10 ) {
        return "000"+num;
    }
    else if(num < 100) {
        return "00"+num;
    }
    else if(num < 1000) {
        return "0"+num;
    }
    return ""+num;
}

function remove_ext(path)
{
    var i, icnt = path.length;
    for(i=0;i<icnt;i++) {
        var idx = icnt - i - 1;
        var ch = path[idx];
        if(ch == '.') {
            var retstr = path.substr(0, idx);
            return retstr;
        }
    }
    return path;
}


function IsSpline(item)
{
    if(item.toString () == "[object Recangle]" ) {
        return true;
        }
    if(item.toString () == "[object Oval]" ) {
        return true;
        }
    if(item.toString () == "[object Polygon]" ) {
        return true;
        }
    if(item.toString () == "[object GraphicLine]" ) {
        return true;
        }
    if(item.toString () == "[object SplineItem]" ) {
        return true;
        }
    return false;
    }
 
function ReplaceEndLine(str) 
{
    var retstr = "";
    var i, icnt = str.length;
    for(i=0;i<icnt;i++) {
        var ch = str[i];
        if(ch == '\r' ) {
            retstr += '\n';
            }
        else {
            retstr += ch;
            }
        }
    return retstr;
    }

var USE_ANTIALIAS = true;
function exportItemAsImage(cap_item, img_file, dpi, quality)  // makePNG, REDPDF, REDImage
{
    app.pngExportPreferences.antiAlias = true;
//    app.pngExportPreferences.pngExportRange = PNGExportRangeEnum.EXPORT_RANGE;  
    app.pngExportPreferences.pngColorSpace = PNGColorSpaceEnum.RGB;  
	
    app.pngExportPreferences.exportResolution = dpi;
    app.pngExportPreferences.pngQuality = quality;

    app.pngExportPreferences.transparentBackground = true
    cap_item.exportFile (ExportFormat.PNG_FORMAT, img_file);
    return false;
}


function exportItemAsJPEG(cap_item, img_file, dpi, quality)  // makePNG, REDPDF, REDImage
 {
 //   var img_file = new File(file_path);

    app.jpegExportPreferences.antiAlias = USE_ANTIALIAS;
 //   app.jpegExportPreferences.jpegExportRange = ExportRangeOrAllPages.exportRange;  
    app.jpegExportPreferences.jpegColorSpace = JpegColorSpaceEnum.RGB;  
 
    app.jpegExportPreferences.embedColorProfile = true;
    app.jpegExportPreferences.jpegRenderingStyle = JPEGOptionsFormat.PROGRESSIVE_ENCODING;
 //   app.jpegExportPreferences.pageString = pgname;     
    app.jpegExportPreferences.exportResolution =  dpi;
    app.jpegExportPreferences.jpegQuality =  quality; //jpg_quality_list[this.section.quality];
    cap_item.exportFile (ExportFormat.jpg, img_file ); 
}

function removeFolder(src_folder)
{    
    var files = src_folder.getFiles();
    var i, icnt = files.length;
    for(i=0;i<icnt;i++) {
        var afile = files[i];
       if(afile instanceof Folder) {
            removeFolder(afile);
        }
        else {
            afile.remove();
        }
    }
    src_folder.remove();
}


function toIntList(floatList)
{
    var ret = new Array;
    var i, icnt = floatList.length;
    for(i=0;i<icnt;i++) {
        var f_val = floatList[i];
        var i_val = Math.round(f_val);
        ret.push(i_val);
    }
    return ret;
}
    
/////  Movable / Selectable Event Routines
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
////addScriptToMenu('Selectable', selectable, selectable_disp, "$ID/RtMouseLayout");
function addScriptToMenu(menuTitle, menuAction, dispAction, menuID, unCheck, unDispAction)
{

	try {

        var i
        var subMenuList = []
        var subMenuListObj = {
            Selectable : {
                    subMenu : ["Select", "UnSelect"],
                    checkFunc   : menuAction,
                    unCheckFunc : unCheck,
                    dispAction  : dispAction,
                    unDispAction : unDispAction
            },
            Movable  : {
                    subMenu : ["Move", "UnMove"],
                    checkFunc : menuAction,
                    unCheckFunc : unCheck,
                    dispAction  : dispAction,
                    unDispAction : unDispAction
            }
        }

        //
        for(i=0; i < subMenuListObj[menuTitle].subMenu.length; i++){
            
            //Movable / Selectable Event를 drop down list로 변경하면서 변경 Kraken
            //var old_menu = app.menus.item(menuID).menuItems.item(menuTitle);
            var old_menu = app.menus.item("$ID/RtMouseLayout").submenus.item(menuTitle);

            if(old_menu.isValid) {
                old_menu.remove();
            }
        }


        var mySampleScriptMenu = app.menus.item("$ID/RtMouseLayout").submenus.add(menuTitle);
        for(i=0; i < subMenuListObj[menuTitle].subMenu.length; i++){
            var mySampleScriptAction = app.scriptMenuActions.add(subMenuListObj[menuTitle].subMenu[i]); 

            if(subMenuListObj[menuTitle].subMenu[i].includes("Un")){
                var myEventListener1 = mySampleScriptAction.eventListeners.add("onInvoke", subMenuListObj[menuTitle].unCheckFunc);
                var myEventListener2 = mySampleScriptAction.eventListeners.add("beforeDisplay", subMenuListObj[menuTitle].unDispAction);	

            }else {
                var myEventListener3 = mySampleScriptAction.eventListeners.add("onInvoke", subMenuListObj[menuTitle].checkFunc);
                var myEventListener4 = mySampleScriptAction.eventListeners.add("beforeDisplay", subMenuListObj[menuTitle].dispAction);
            }

            var mySampleScriptMenuItem = mySampleScriptMenu.menuItems.add(mySampleScriptAction);
        }
        //
        

        // var myScriptAction = app.scriptMenuActions.add(menuTitle);
		// var myEventListener1 = myScriptAction.eventListeners.add("onInvoke", menuAction);
		// var myEventListener2 = myScriptAction.eventListeners.add("beforeDisplay", dispAction);	
        // app.menus.item(menuID).menuItems.add(myScriptAction);
        
	}
	catch(err) {
		alert(err);
	}
}

/////////////////////////////// Name
function getItemName(item)
{
    var str = item.extractLabel("name");
    if(typeof str != "undefined" && str.length > 0) {
		return str;
    }
    return "";
}

function setItemName(item, name)
{
    item.insertLabel("name", name);
}


function setNameAll(name)
{
	if(name == null)
		return "ok";
	
	if(typeof name == "undefined")
		return "ok";
	
    var i, icnt = app.selection.length;
    for(i=0;i<icnt;i++) {
        var item = app.selection[i];
        setItemName(item, name);
    }
	return "ok";
//    Dwriteln (event);
}


/////////////////////////////// Selectable
function isSelectable(item)
{
    var str = item.extractLabel("selectable");
    if(typeof str != "undefined" && str.length > 0) {
        if(str == "yes") {
            return true;
        }
    }
    return false;
}

function setSelectable(item, flag)
{
    if(flag) {
        item.insertLabel("selectable", "yes");
    }
    else {
        item.insertLabel("selectable", "");
    }
}

/////////////////////////////// UnSelectable
function isUnSelectable(item)
{
    var str = item.extractLabel("unSelectable");
    if(typeof str != "undefined" && str.length > 0) {
        if(str == "yes") {
            return true;
        }
    }
    return false;
}

function setUnSelectable(item, flag)
{
    if(flag) {
        item.insertLabel("unSelectable", "yes");
    }
    else {
        item.insertLabel("unSelectable", "");
    }
}


function setSelectableAll(flag)
{
    var i, icnt = app.selection.length;
    for(i=0;i<icnt;i++) {
        var item = app.selection[i];
        setSelectable(item, flag);
    }
	
    app.redprinterObject.setAdornment();

//    Dwriteln (event);
	return "ok";
}


function selectable(event)  // toggle
{
    var i, icnt = app.selection.length;
    for(i=0;i<icnt;i++) {
        var item = app.selection[i];
        
        if(isSelectable(item)) { // toggle
            setSelectable(item, false);
        }
        else {
            setSelectable(item, true);
        }
    }
//    Dwriteln (event);
}

function unSelectable(event)  // toggle
{
    
    var i, icnt = app.selection.length;
    for(i=0;i<icnt;i++) {
        var item = app.selection[0];
        if(isUnSelectable(item)) { // toggle
            setUnSelectable(item, false);
        }
        else {
            setUnSelectable(item, true);
        }
    }
//    Dwriteln (event);
}

function selectable_disp(event)
{
    var is_selectable = true;
    var i, icnt = app.selection.length;
    for(i=0;i<icnt;i++) {
        var item = app.selection[i];
        if(isSelectable(item) == false) {
            is_selectable = false;
        }
    }
    event.target.checked = is_selectable;
//    Dwriteln (event);
}

function unSelectable_disp(event)
{
    var is_selectable = true;
    var i, icnt = app.selection.length;
    for(i=0;i<icnt;i++) {
        var item = app.selection[i];
        if(isUnSelectable(item) == false) {
            is_selectable = false;
        }
    }
    event.target.checked = is_selectable;
//    Dwriteln (event);
}

/////////////////////////////// Movable
function isMovable(item)
{
    var str = item.extractLabel("movable");
    if(typeof str != "undefined" && str.length > 0) {
        if(str == "yes") {
            return true;
        }
    }
    return false;    
}

function setMovable(item, flag)
{
    
    if(flag) {
        item.insertLabel("movable", "yes");
    }
    else {
        item.insertLabel("movable", "");
    }
}


/////////////////////////////// UnMovable
function isUnMovable(item)
{
    var str = item.extractLabel("unMovable");
    if(typeof str != "undefined" && str.length > 0) {
        if(str == "yes") {
            return true;
        }
    }
    return false;    
}

function setUnMovable(item, flag)
{
    
    if(flag) {
        item.insertLabel("unMovable", "yes");
    }
    else {
        item.insertLabel("unMovable", "");
    }
}


function setMovableAll(flag)
{
    var i, icnt = app.selection.length;
    for(i=0;i<icnt;i++) {
        var item = app.selection[i];
        setMovable(item, flag);
    }
//    Dwriteln (event);
    app.redprinterObject.setAdornment();
	return "ok";
}

function movable(event)   // Toggle
{
    var i, icnt = app.selection.length;
    for(i=0;i<icnt;i++) {
        var item = app.selection[i];
        if(isMovable(item)) {
            setMovable(item, false);
        }
        else {
            setMovable(item, true);
        }
    }
    
//    Dwriteln (event);
}

function unMovable(event)   // Toggle
{
    var i, icnt = app.selection.length;
    for(i=0;i<icnt;i++) {
        var item = app.selection[i];
        if(isUnMovable(item)) {
            setUnMovable(item, false);
        }
        else {
            setUnMovable(item, true);
        }
    }
    
//    Dwriteln (event);
}

function movable_disp(event)
{
    var is_movable = true;
    var i, icnt = app.selection.length;
    for(i=0;i<icnt;i++) {
        var item = app.selection[i];
        if(isMovable(item) == false) {
            is_movable = false;
        }
    }
    event.target.checked = is_movable;
//   Dwriteln (event);
}

function unMovable_disp(event)
{
    var is_movable = true;
    var i, icnt = app.selection.length;
    for(i=0;i<icnt;i++) {
        var item = app.selection[i];
        if(isUnMovable(item) == false) {
            is_movable = false;
        }
    }
    event.target.checked = is_movable;
//   Dwriteln (event);
}

/////////////////////////////// Variable
function isVariable(item)
{
    var str = item.extractLabel("variable");
    if(typeof str != "undefined" && str.length > 0) {
        if(str == "yes") {
            return true;
        }
    }
    return false;    
}

function setVariable(item, flag)
{
    if(flag) {
        item.insertLabel("variable", "yes");
    }
    else {
        item.insertLabel("variable", "no");
    }
}

function setVariableAll(flag)
{
    var i, icnt = app.selection.length;
    for(i=0;i<icnt;i++) {
        var item = app.selection[i];
        setVariable(item, flag);
    }
    app.redprinterObject.setAdornment();
	return "ok";
}

/////////////////////////////// Spine
function isSpine(item)
{
    var str = item.extractLabel("spine");
    if(typeof str != "undefined" && str.length > 0) {
        if(str == "yes") {
            return true;
        }
    }
    return false;    
}

function setSpine(item, flag)
{
    if(flag) {
        item.insertLabel("spine", "yes");
    }
    else {
        item.insertLabel("spine", "no");
    }
}

function setSpineAll(flag)
{
    var i, icnt = app.selection.length;
    for(i=0;i<icnt;i++) {
        var item = app.selection[i];
        setSpine(item, flag);
    }
    app.redprinterObject.setAdornment();
	return "ok";
}

/////////////////////////////// Chain
function isChain(item)
{
    var str = item.extractLabel("chain");
    if(typeof str != "undefined" && str.length > 0) {
        if(str == "yes") {
            return true;
        }
    }
    return false;    
}

function setChain(item, flag)
{
    if(flag) {
        item.insertLabel("chain", "yes");
    }
    else {
        item.insertLabel("chain", "no");
    }
}

function setChainAll(flag)
{
    var i, icnt = app.selection.length;
    for(i=0;i<icnt;i++) {
        var item = app.selection[i];
        setChain(item, flag);
    }
    app.redprinterObject.setAdornment();
	return "ok";
}

/////////////////////////////// Mask
function isMask(item)
{
    var str = item.extractLabel("mask");
    if(typeof str != "undefined" && str.length > 0) {
        if(str == "yes") {
            return true;
        }
    }
    return false;    
}

function setMask(item, flag)
{
    if(flag) {
        item.insertLabel("mask", "yes");
    }
    else {
        item.insertLabel("mask", "no");
    }
}

function setMaskAll(flag)
{
    var i, icnt = app.selection.length;
    for(i=0;i<icnt;i++) {
        var item = app.selection[i];
        setMask(item, flag);
    }
    app.redprinterObject.setAdornment();
	return "ok";
}


function getAttributes()
{
	var retobj = new Object;
	var retstr = '{\n';
    var i, icnt = app.selection.length;
	if(icnt == 0) {
		retobj.state = 0;
		retstr += '    "res_type" : 0\n';
	}
	else if(icnt == 1) {
        var item = app.selection[0];
        
		retstr += '    "res_type" : 1,\n';  // "state" 로 하게되면 문제가 생김
		retstr += '    "name" : "'+getItemName(item)+'",\n';
		retstr += '    "selectable" : "'+isSelectable(item)+'",\n';
		retstr += '    "movable" : "'+isMovable(item)+'",\n';
		retstr += '    "variable" : "'+isVariable(item)+'",\n';
		retstr += '    "spine" : "'+isSpine(item)+'",\n';
		retstr += '    "chain" : "'+isChain(item)+'",\n';
		retstr += '    "mask" : "'+isMask(item)+'"\n';
		//retobj.attr = new Object;
		retobj.name = getItemName(item);
		retobj.selectable = isSelectable(item);
		retobj.movable = isMovable(item);
		retobj.variable = isVariable(item);
		retobj.spine = isSpine(item);
		retobj.chain = isChain(item);
		retobj.mask = isMask(item);
	}
	else {
		retobj.state = 2;
		retstr += '    "res_type" : 2\n';
	}
	retstr += '}\n';
    //alert("test : "+ "JSON Test.");
    //var retstr = JSON.stringify(retobj, null, "\t");
	return retstr;
}

addScriptToMenu('Selectable', selectable, selectable_disp, "$ID/RtMouseLayout", unSelectable, unSelectable_disp);
addScriptToMenu('Movable', movable, movable_disp, "$ID/RtMouseLayout", unMovable, unMovable_disp);


/*
_addscripttomenu('WW_Duplicate_Article.jsx', "$ID/RtMouseLayout");
_addscripttomenu('applyObjectStyles.jsx', "$ID/RtMouseText");
_addscripttomenu('WW_Duplicate_Article.jsx', "$ID/RtMouseText");
*/


//////////////////////////////////////////////////  REDObject
function REDObject() {
    this.type = "Object";
    }

REDObject.prototype.constructor = REDObject;

REDObject.prototype.jsonObj = function() {
    return this;
    }

REDObject.prototype.equalClass = function(class_name) {
    var org_class_name = org_class_str.substring(8, org_class_str.length - 1);
    if(obj_class_name.equals(class_name)){
        return true;
        }
    return false;
    }



//////////////////////////////////////////////////  REDPoint
function REDPoint() {
    this.type = "Point";
    this.x = 0;
    this.y = 0;
    }
    
REDPoint.prototype = new REDObject();
REDPoint.prototype.constructor = REDPoint;

REDPoint.prototype.jsonObj = function() {
    var jsonobj = new Object;
    jsonobj["x"] = this.x;
    jsonobj["y"] = this.y;
    return jsonobj;
    }

//////////////////////////////////////////////////  REDSize
function REDSize() {
    this.type = "Size";
    this.width = 0;
    this.height = 0;
    }

//컷레이어,컷사이즈 강제설정 값을 알기위해 인자에 layerName, itemName 추가 // 20190712 kraken    
function REDSize(w, h, layerName, itemName) {
    this.layerName = layerName
    this.itemName  = itemName
    this.type = "Size";
    this.width = w;
    this.height = h;
    }

REDSize.prototype = new REDObject();
REDSize.prototype.constructor = REDSize;

REDSize.prototype.jsonObj = function() {
    
    var jsonobj = new Object;
    
    //Cut레이어 강제설정시 소수점 제한 무시한채 값 설정 // 20190712 kraken 
    if (this.layerName === "Cut" || this.layerName === "Cut@{render:false}"){
        if(this.itemName.charAt(0) === "$"){
            var itemName = this.itemName.substring(1, this.itemName.length)
            var itemNameObj = JSON.parse(itemName)
            jsonobj["width"] = this.width;
            jsonobj["height"] = this.height;
            jsonobj["individual_width"] = Number(itemNameObj.width);
            jsonobj["individual_height"] = Number(itemNameObj.height);
            jsonobj["forceSetting"] = true;
        }else{
            jsonobj["width"] = fixedfloat(this.width);
            jsonobj["height"] = fixedfloat(this.height);
            jsonobj["forceSetting"] = false;
        }
    }else{
        jsonobj["width"] = this.width;
        jsonobj["height"] = this.height;
    }

    
    return jsonobj;
    }

//////////////////////////////////////////////////  REDRect
function REDRect(arr, layerName, itemName) {

    this.type = "Rect";
    this.origin = new REDPoint();
    if(arr != null) {
        this.origin.y = arr[0];
        this.origin.x = arr[1];
        }
    this.size = new REDSize('','',layerName, itemName);
    
    if(arr != null) {
        this.size.height = arr[2] - this.origin.y;
        this.size.width = arr[3] - this.origin.x;
        }
}

REDRect.prototype = new REDObject();
REDRect.prototype.constructor = REDRect;

REDRect.prototype.jsonObj = function() {
    var jsonobj = new Object;
    jsonobj["origin"] = this.origin.jsonObj();
    jsonobj["size"] = this.size.jsonObj();
    return jsonobj;
    }


REDRect.RectFromArray = function(arr)
{
    var rect = new REDRect();
    rect.origin.y = arr[0];
    rect.origin.x = arr[1];
    rect.size.height = arr[2] - rect.origin.y;
    rect.size.width = arr[3] - rect.origin.x;
    return rect;
}

/////////////////////////////////////////////////// //////////////////////////////////////////////////  REDColor
var color_dict = null;
function REDColor(org_color, tint) {
 //   REDObject.call(this, org_item);
//    if(org_item instanceof Color) {
    var color_class_name = null;
    try {
        color_class_name = org_color.toString();
    }
    catch (e) {
        color_class_name = null;
    }
    
    if(color_class_name == "[object Color]") {
        this.color_name = org_color.name;

        this.convertColor(org_color, tint);
        if(this.space == "CMYK" && this.name != "Paper") {
            /*
            var newColor = org_color.duplicate();
            newColor.space = ColorSpace.RGB;
            this.rgb_color = newColor.colorValue;
            newColor.remove();
            */
        }
    }
    else {
        this.space = "Unknown";
        this.color_value = null;
        this.name = "Unknown";
    }
}
REDColor.prototype = new REDObject();
REDColor.prototype.constructor = REDColor;

REDColor.prototype.convertColor = function(src_color, tint)
{
    this.src_color = toIntList(src_color.colorValue);
    this.src_color_space = this.colorSpaceName(src_color.space);
    if(src_color.space == ColorSpace.CMYK) {
        if(src_color.colorValue[0] == 0) {
            if(src_color.colorValue[1] == 0) {
                if(src_color.colorValue[2] == 0) { 
                    if(src_color.colorValue[3] == 50)  {
                        this.color_name = "c0m0y0k50";
                    }
                    else if(src_color.colorValue[3] == 100) {
                        if(tint != null && tint == 50) {
                            this.color_name = "c0m0y0k50";
                        }
                     }
                }
            }
        }
    }

    if(src_color.space == ColorSpace.RGB) {
        this.rgb_color = toIntList(src_color.colorValue);
    }
    else {    
        var color_name = src_color.name;
        var rgb_color = null;
        if(color_name != null && color_name.length > 0) {
            rgb_color = color_dict[color_name];
        }

        if(rgb_color != null) {
            this.rgb_color = rgb_color;
        }
        else {
            try {
                var src_color_space = src_color.space;
                var src_color_values = [4];
                src_color_values[0] = src_color.colorValue[0];
                src_color_values[1] = src_color.colorValue[1];
                src_color_values[2] = src_color.colorValue[2];
                src_color_values[3] = src_color.colorValue[3];
                src_color.space = ColorSpace.RGB;
                this.rgb_color =  toIntList(src_color.colorValue);
                src_color.space = src_color_space;
                src_color.colorValue = src_color_values;
                /*
                src_color.colorValue[0] = src_color_values[0];
                src_color.colorValue[1] = src_color_values[1];
                src_color.colorValue[2] = src_color_values[2];
                src_color.colorValue[3] = src_color_values[3];
                */
            }
            catch (ex) {
                var msg = "Exception occurd...["+ex+"]";
                Dwriteln (msg);
                if(ex.number == 90907) {
                    if(color_name == "Black") {
                        var def_color = []; // 35,31,32
                        def_color.push(0);
                        def_color.push(0);
                        def_color.push(0);
                        this.rgb_color =def_color;
                    }
                    else {
                        this.rgb_color = ex.message;
                     }
                }
                else {
                    this.rgb_color = ex.message;
                }
            }
            if(color_name != null && color_name.length > 0) {
                color_dict[color_name] = this.rgb_color;
            }
        }
    }
}
    

REDColor.prototype.convertColor__ = function(org_color)
{
    if(org_color.space == ColorSpace.CMYK) {
        this.cmyk_color = org_color.colorValue;
        org_color.space = ColorSpace.RGB;
       this.rgb_color =  org_color.colorValue;
        org_color.space = ColorSpace.CMYK;

        }
    else if(org_color.space = ColorSpace.RGB){
        this.rgb_color = org_color.colorValue;
        org_color.space = ColorSpace.CMYK;
         this.cmyk_color =  org_color.colorValue;
        org_color.space = ColorSpace.RGB;       
        }
    else {
        this.color_value = org_color.value;
        this.color_space = this.colorSpaceName(org_color.space);
        }
    }
    
REDColor.prototype.convertColor___ = function(org_color)
{
    if(org_color.space == ColorSpace.CMYK) {
        this.cmyk_color = org_color.colorValue;
        org_color.properties = {space:ColorSpace.RGB};
       this.rgb_color =  org_color.colorValue;
        org_color.properties = {space:ColorSpace.CMYK};

        }
    else if(org_color.space = ColorSpace.RGB){
        this.rgb_color = org_color.colorValue;
        org_color.properties = {space:ColorSpace.CMYK};
         this.cmyk_color =  org_color.colorValue;
        org_color.properties = {space:ColorSpace.RGB};
       
        }
    else {
        this.color_value = org_color.value;
        this.color_space = this.colorSpaceName(org_color.space);
        }
    }
    


REDColor.prototype.convertColor_ = function(org_color)
{
        var space = this.colorSpaceName(org_color.space);
        
        if(space == "CMYK") {
            this.cmyk_color = org_color.colorValue;
            var rgb_color = color_dict[org_color.name];
            if(rgb_color != null) {
                this.rgb_color = rgb_color;
            }
            else {
                var cname = org_color.name;
                if(cname == "Paper" || cname == "None" || cname == "Registration") {
                    return;
                    }
               var new_color = org_color.duplicate();
                new_color.space = ColorSpace.RGB;
                rgb_color = new_color.colorValue;
                color_dict[org_color.name] = rgb_color;
                this.rgb_color = rgb_color;
                new_color.remove();
            }
        }
        else if(space == "RGB") {
            this.rgb_color = org_color.colorValue;
            var cmyk_color = color_dict[org_color.name];
            if(cmyk_color != null) {
                this.cmyk_color = cmyk_color;
            }
            else {
                var new_color = org_color.duplicate();
                new_color.space = ColorSpace.CMYK;
                cmyk_color = new_color.colorValue;
                color_dict[org_color.name] = cmyk_color;
                this.cmyk_color = cmyk_color;
                new_color.remove();
            }
        }
    
}



REDColor.prototype.colorSpaceName = function(col_enum)
{
    var ret_val = "Unknown";
    switch(col_enum) {
        case ColorSpace.CMYK: 
            ret_val = "CMYK";
            break;
        case ColorSpace.RGB:
            ret_val = "RGB";
            break;
        case ColorSpace.LAB:
            ret_val = "LAB";
            break;
        case ColorSpace.MIXEDINK:
            ret_val = "MIXEDINK";
            break;
        }
    return ret_val;
    }

////////////////////////////////////////////////// //////////////////////////////////////////////////  REDSpread
function REDSpread() {
    this.type = "Spread";  // <Type> Tag
    this.page_size = new REDRect();
    this.number_of_pages = 0;
    this.pages = new Array();
    
    }

REDSpread.prototype = new REDObject();
REDSpread.prototype.constructor = REDSpread;


////////////////////////////////////////////////// //////////////////////////////////////////////////  REDPageItem
function MyException (name, message) {
    this.message= message;
    this.name= name;
}

function REDPageItem(org_item, doc) {
    this.parent = null;
    this.type = "PageItem";  // <Type> Tag
    this.org_item = org_item;
    
    this.document = doc;
    if(typeof org_item != "undefined") {
        this.layer = this.org_item.itemLayer;
    }
    this.page_items = new Array();
    if(this.org_item != null) {
        
        // angle 이 0이 아닐때 중심점을 가운데로 지정한뒤 좌표를 얻는다.
        this.angle = -this.org_item.rotationAngle;
        this.shear = this.org_item.shearAngle;
        if(this.angle != 0 || this.shear != 0) {
            if(this.org_item.locked) {
                var myUserException=new MyException("Object Locked", "잠겨있는 객체가 있습니다.");
                throw myUserException;
            }
            var org_anchor = app.activeWindow.transformReferencePoint;
            app.activeWindow.transformReferencePoint = AnchorPoint.CENTER_ANCHOR       
            try {
                this.org_item.rotationAngle = 0;
                this.org_item.shearAngle = 0;
            }
            catch (e) {
                $.writeln(e);
            }
        }
        
        this.abs_frame = new REDRect(this.org_item.visibleBounds, this.layer.name, this.org_item.name);
        // 변경된 angle을 다시 되돌린다.
        if(this.angle != 0) {
            this.org_item.rotationAngle = -this.angle;
            this.org_item.shearAngle = this.shear;
            app.activeWindow.transformReferencePoint = org_anchor;         
        }
        
        if(typeof org_item.pageItems === "object") {

            var child_items = this.org_item.pageItems;
            if(child_items != undefined && child_items != null) {
                var i, icnt = child_items.length;
                if(icnt > 0) {
                    // 도큐먼트(this.document) 값을 전달(유지)하기 위해 도큐먼트에서 pageItem들을 읽어들인다.
                    doc.load_page_items(this.org_item, this);
                }
            }
        }
    }
    else {
        this.abs_frame = new REDRect();
        this.frame = new REDRect();
        this.angle = 0;
    }    
}

REDPageItem.prototype = new REDObject();
REDPageItem.prototype.constructor = REDPageItem;

REDPageItem.prototype.jsonObj = function() {
    
    var jsonobj = new Object;
    jsonobj["type"] = this.type;
    jsonobj["id"] = this.org_item.id.toString();
    jsonobj["frame"] = this.frame.jsonObj();
    jsonobj["h_scale"] = this.org_item.horizontalScale;
    jsonobj["v_scale"] = this.org_item.verticalScale;
    jsonobj["frame_abs"] = this.abs_frame.jsonObj();
    jsonobj["index"] = this.org_item.index;
   // jsonobj["geo_bounds"] = this.org_item.geometricBounds;
    jsonobj["layer"] = this.layer.name;
    jsonobj["name"] = this.org_item.name;

    //화이트 render true여도 에디터에서는 안보이게 처리 Kraken 
    if(this.layer.name == "White" || this.layer.name == "White@{render:true}"){
        jsonobj["hidden"] = whiteLayerVisible; //화이트시 숨김여부 설정
    }

    jsonobj["rotation_angle"] = this.angle;
    jsonobj["shear_angle"] = this.shear;
    jsonobj["flip"] = pageitem_flip(this.org_item.flip);
    jsonobj["locked"]  = this.org_item.locked;
    jsonobj["visible"] = this.org_item.visible;
    jsonobj["opacity"] = this.org_item.transparencySettings.blendingSettings.opacity;
    
    //플로팅 옵션 설정값 가져오기 Kraken
    if(this.layer.name.includes('Text') || this.layer.name.includes('Sticker')){

        var selectable     = this.org_item.extractLabel("selectable");
        var unSelectable   = this.org_item.extractLabel("unSelectable");
        var movable        = this.org_item.extractLabel("movable");
        var unMovable      = this.org_item.extractLabel("unMovable");

        if(unSelectable == "yes"){
            jsonobj["selectable"] = false
        }else if(selectable == "yes"){
            jsonobj["selectable"] = true
        }else{
            jsonobj["selectable"] = (this.layer.name.includes('Sticker')) ? isStickerSelectable : isTextSelectable
        }

        if(unMovable == "yes"){
            jsonobj["movable"] = false
        }else if(movable == "yes"){
            jsonobj["movable"] = true 
        }else{
            jsonobj["movable"] = (this.layer.name.includes('Sticker')) ? isStickerMovable : isTextMovable
        }
    }

    // var variable = this.org_item.extractLabel("variable"); root field
    // if(typeof variable != "undefined") {
    //     jsonobj["variable"] = variable;
    // }
    var label_str = this.org_item.extractLabel("FIELD");
    
    var name = this.org_item.extractLabel("name");
    if(typeof name != "undefined" && name != "") {
        jsonobj["name"] = name;
    }

    // 20180918 redirect로 명명된 레이어에 대해서 연동을 위한 세팅을 한다. - Irelander_kai
    // 200103 특정상품 variableId 임의설정 -Kraken
    
    if((label_str != null && typeof label_str != "undefined" && label_str != "") || this.layer.name.includes("redirect") || variableType != "") {
        var layerName = "";
        if (this.layer.name.includes("@")){
            layerName = this.layer.name.split(":")[1].replace("}", "");
        }else {
            layerName = this.layer.name
        }
        

        //타입에 따라서 객체 옵션 설정 - KRAKEN
        var typeInfo = {text : {},sticker : {}, varialbleId : ""}
        if (variableType == "typeA" || variableType == "typeB"){
        
            if (variableType == "typeA"){
                // typeInfo.text.selectable        =  false
                // typeInfo.text.movable           =  false
                typeInfo.text.deletable         =  false
                // typeInfo.sticker.selectable     =  false
                // typeInfo.sticker.movable        =  false
                typeInfo.sticker.deletable      =  false
                typeInfo.textId                 =  'commonText'
                typeInfo.stickerId              =  'commonSticker'

            }else if(variableType == "typeB"){ 
                // typeInfo.text.selectable        =  false
                // typeInfo.text.movable           =  false
                typeInfo.text.deletable         =  false
                // typeInfo.sticker.selectable     =  false
                // typeInfo.sticker.movable        =  false
                typeInfo.sticker.deletable      =  false
                typeInfo.textId                 =  this.org_item.name
                typeInfo.stickerId              =  'commonSticker'
            }

            if(this.layer.name == "Text"){
                // jsonobj["selectable"]   = typeInfo.text.selectable
                // jsonobj["movable"]      = typeInfo.text.movable
                jsonobj["deletable"]    = typeInfo.text.deletable
                typeInfo.varialbleId    = typeInfo.textId
                
            }else if(this.layer.name == "Sticker"){
                // jsonobj["selectable"] = typeInfo.sticker.selectable
                // jsonobj["movable"]    = typeInfo.sticker.movable
                jsonobj["deletable"]  = typeInfo.sticker.deletable
                typeInfo.varialbleId  = typeInfo.sticker.stickerId
            }
        }

        jsonobj["name"]     = layerName;
        jsonobj["variable"] = {
            type : (this.layer.name == "Text") ? "input" : this.layer.name,
            id : (typeInfo.varialbleId != "") ? typeInfo.varialbleId : makeId(),
            title : label_str || layerName,
            group_id : null,
            extra : null
        }
    }

    var spine = this.org_item.extractLabel("spine");
    if(typeof spine != "undefined") {
        jsonobj["spine"] = spine;
    }
    var chain = this.org_item.extractLabel("chain");
    if(typeof chain != "undefined") {
        jsonobj["chain"] = chain;
    }
    var mask  = this.org_item.extractLabel("mask ");
    if(typeof mask  != "undefined") {
        jsonobj["mask "] = mask ;
    }

    // Todo... deletable, focusible, hidden 값 넘버링을 위해 임의 추가 20180918
    if(label_str == "Number"){
        jsonobj["selectable"] = true;
        jsonobj["movable"] = true;
        jsonobj["deletable"] = false;
        jsonobj["focusible"] = false;
        jsonobj["hidden"] = false;
    }

    if(this.page_items.length > 0) {
        var jsonitems = new Array();
        var i, icnt = this.page_items.length;
        for(i=0;i<icnt;i++) {
            var pageitem = this.page_items[i];
            var jsonitem = pageitem.jsonObj();
            jsonitems.push(jsonitem);
            }
        jsonobj["page_items"] = jsonitems;
        }
    return jsonobj;
    }

REDPageItem.prototype.getPage = function()
{
    var cur_obj = this.parent;
    while(cur_obj != null) {
        if(cur_obj.constructor.name == "REDPage") {
            return cur_obj;
        }
        cur_obj = cur_obj.parent;
    }
    return null;
}

function CountPaths(pathitem)
{
    var tot = 0;
    if(typeof pathitem.paths != "undefined") {
        tot += pathitem.paths.length;
    }
    var itemlist = pathitem.allPageItems;
    var i, icnt = itemlist.length;
    for(i=0;i<icnt;i++) {
        var item = itemlist[i];
        if(typeof item.paths != "undefined") {
            tot += item.paths.length;
        }
    }
    return tot;
}

REDPageItem.prototype.makeSVG = function()
{
    var num_of_paths = CountPaths(this.org_item);
    if(MAX_PATHS_LIMIT != 0 && num_of_paths > MAX_PATHS_LIMIT) {
        Dwriteln("number of paths : " + num_of_paths);
        var ret_file = this.makePNG();
        return ret_file;
    }
    var objlist = new Array();
    objlist.push(this.org_item);
        
    var org_angle = this.org_item.rotationAngle;
    try {
        this.org_item.rotationAngle = 0;
    }
    catch (e) {
        Dwriteln(e);
    }
    var prev_hunit = cur_doc.viewPreferences.horizontalMeasurementUnits;
    var prev_vunit = cur_doc.viewPreferences.verticalMeasurementUnits;
    cur_doc.viewPreferences.horizontalMeasurementUnits = MeasurementUnits.POINTS
    cur_doc.viewPreferences.verticalMeasurementUnits = MeasurementUnits.POINTS;
    Dwriteln("Root Group ID:"+this.org_item.id);
    var svg = convertToSVG(objlist);
        
    cur_doc.viewPreferences.horizontalMeasurementUnits = prev_hunit;
    cur_doc.viewPreferences.verticalMeasurementUnits = prev_vunit;
       
   
    var docpath = this.document.path;
    var docfile = new File(docpath);
    var docfolder = docfile.parent;


    //Background Anotation시 처리 //20190401 Kraken
    // if(this.org_item.itemLayer.name == "Background") {
    //     Dwriteln("Background SVG...");
    // }
    if(this.org_item.itemLayer.name.includes("Background")) {
        Dwriteln("Background SVG...");
    }



    var svgpath = docfolder + "/" + this.org_item.id + ".svg";
    var svgfile = new File(svgpath);
    saveSVGFile(svg, svgfile);
    try {
        this.org_item.rotationAngle = org_angle;
    }
    catch (e) {
        Dwriteln(e);
    }
    return svgfile;
}

REDPageItem.prototype.makePNG = function()  // makeSVG,
{
    var org_angle = this.org_item.rotationAngle;
    this.org_item.rotationAngle = 0;
    var doc_file = new File(this.document.path);
    var red_folder_path = doc_file.parent.fullName;
    var fname = this.org_item.id;
    if(fname == "26070") {
        Dwriteln("26070.png");
    }
    var tar_path = red_folder_path+ "/" + fname;
    tar_path = tar_path + ".png";
    var tar_file = new File(tar_path);
    exportItemAsImage(this.org_item, tar_file, 300, PNGQualityEnum.MEDIUM);
    this.org_item.rotationAngle = org_angle;
    
    return tar_file;
}

REDPageItem.prototype.makeJPG = function()  // makeSVG,
{
    var org_angle = this.org_item.rotationAngle;
    var doc_file = new File(this.document.path);
    var red_folder_path = doc_file.parent.fullName;
    var fname = this.org_item.id;
    var tar_path = red_folder_path+ "/" + fname;
    tar_path = tar_path + ".jpg";
    var tar_file = new File(tar_path);
    var maxPixel = 16000;
    var dpi = 300;

    // max pixel calculate ( 16383x16383 ) validate ( 최대 픽셀값을 초과하는지 검사 후 dpi 결정 )
    var width_mm  = ( -1*this.org_item.visibleBounds[1] + this.org_item.visibleBounds[3] );
    var width_px = width_mm/25.4 * dpi;
    var height_mm = ( -1*this.org_item.visibleBounds[0] + this.org_item.visibleBounds[2] );
    var height_px = height_mm/25.4 * dpi;
    if ( width_px > maxPixel  || height_px > maxPixel ){
        var length = (width_mm > height_mm) ? width_mm : height_mm;
        dpi = Math.floor(maxPixel/(length/25.4))-1;
    }

    exportItemAsJPEG(this.org_item, tar_file, dpi, JPEGOptionsQuality.HIGH);
    this.org_item.rotationAngle = org_angle;
    return tar_file;
}

REDPageItem.prototype.copyImageFile = function(src_path, ext)
{
    var org_doc_path = this.document.org_doc.fullName.fullName;
    var src_file = new File(src_path);
    if(!src_file.exists && org_doc_path != null) {
        var src_name = src_file.name;
        var doc_file = new File(org_doc_path);
        var doc_folder_path = doc_file.parent;
        var link_path = doc_folder_path + "/Links/";
        src_path = link_path + src_name;
        src_file = new File(src_path);
        }
    if(!src_file.exists) {
        // error 
        return null;
        }
    var fname = src_file.name;
    if(this.layer.name == "PicFrame") {
        fname = this.org_item.id + "." + ext;
        }
    
    var red_doc_file = new File(this.document.path);
    var red_folder_path = red_doc_file.parent.fullName;
    var tar_path = red_folder_path + "/" + fname;
    var tar_file = new File(tar_path);
    if(src_file.copy(tar_file) == false) {
        // copy failed!!!
        }
    return tar_file.fullName;    
    
}


REDPageItem.prototype.relativeBounds = function(bounds) {
        var ret_bounds = new REDRect();
        ret_bounds.origin.x = bounds.origin.x;
        ret_bounds.origin.y = bounds.origin.y;
        ret_bounds.size.width = bounds.size.width;
        ret_bounds.size.height = bounds.size.height;
        if(this.parent.constructor.name != "REDPage") {   
            ret_bounds.origin.x -= this.parent.abs_frame.origin.x;
            ret_bounds.origin.y -= this.parent.abs_frame.origin.y;
        }
        return ret_bounds;
    }


REDPageItem.prototype.relativeFrameFrom = function(parent) {
        var retFrame = new REDRect();
        retFrame.origin.x = this.abs_frame.origin.x;
        retFrame.origin.y = this.abs_frame.origin.y;


        //작업 Cut레이어 강제설정시 소수점 무시한채 값 설정 
        if (this.layer.name === "Cut" || this.layer.name === "Cut@{render:false}" || this.layer.name.split('@')[0] === "Cut"){
            if(this.org_item.name.charAt(0) === "$"){
                var itemName = this.org_item.name.substring(1, this.org_item.name.length)
                var itemNameObj = JSON.parse(itemName)
                retFrame.size.width  = fixedfloat(this.abs_frame.size.width)
                retFrame.size.height = fixedfloat(this.abs_frame.size.height);
                retFrame.size.individual_width  = Number(itemNameObj.width);
                retFrame.size.individual_height = Number(itemNameObj.height);
                
            }else{
                retFrame.size.width = fixedfloat(this.abs_frame.size.width);
                retFrame.size.height = fixedfloat(this.abs_frame.size.height);
            }
        }else{
            retFrame.size.width = this.abs_frame.size.width;
            retFrame.size.height = this.abs_frame.size.height;
        }

        // retFrame.size.width = this.abs_frame.size.width;
        // retFrame.size.height = this.abs_frame.size.height;
        
        //retFrame = this.abs_frame;
        if(parent.constructor.name != "REDPage") {
            retFrame.origin.x -= parent.abs_frame.origin.x;
            retFrame.origin.y -= parent.abs_frame.origin.y;
        }
        return retFrame;
    }

// 최 말단의 org_item(인디자인 객체)을 모은다.
REDPageItem.prototype.getherItems = function(objlist)
{
    
    
    if(typeof this.page_items == "undefined" || this.page_items.length == 0) {
        return;
    }
    var i, icnt = this.page_items.length;
    for(i=0;i<icnt;i++) {
        var red_item = this.page_items[icnt-i-1];
        objlist.push(red_item.org_item);
        if(red_item.page_items.length > 0) {
        //if(item.constructor.name == "Group") {
            red_item.getherItems(objlist);
        }
    }
}


REDPageItem.prototype._hasEffects = function(setting)
{
    var opt = setting.bevelAndEmbossSettings;
    if(typeof opt != "undefined") {
        var applied = opt.applied;
        if(applied)
            return true;
    }
    opt = setting.dropShadowSettings;
    if(typeof opt != "undefined") {
        var mode = opt.mode;
        if(mode != ShadowMode.NONE) 
            return true;
    }
    opt = setting.innerShadowSettings;
    if(typeof opt != "undefined") {
        var applied = opt.applied;
        if(applied)
            return true;
    }
    opt = setting.innerGlowSettings;
    if(typeof opt != "undefined") {
        var applied = opt.applied;
        if(applied)
            return true;
    }
    opt = setting.outerGlowSettings;
    if(typeof opt != "undefined") {
        var applied = opt.applied;
        if(applied)
            return true;
    }
    opt = setting.featherSettings
     if(typeof opt != "undefined") {
        var mode = opt.mode;
        if(mode != FeatherMode.NONE) 
            return true;
    }
    opt = setting.gradientFeatherSettings;
    if(typeof opt != "undefined") {
        var applied = opt.applied;
        if(applied)
            return true;
    }
    opt = setting.directionalFeatherSettings;
    if(typeof opt != "undefined") {
        var applied = opt.applied;
        if(applied)
            return true;
    }
    opt = setting.satinSettings;
    if(typeof opt != "undefined") {
        var applied = opt.applied;
        if(applied)
            return true;
    }
       
    return false;
}

REDPageItem.prototype.hasEffects = function(org_item)
{
    // contentTransparencySettings
    var setting = org_item.contentTransparencySettings;
    if(typeof setting != "undefined") {
        var has = this._hasEffects(setting);
        if(has)
            return true;
    }
    // fillTransparencySettings
    setting = org_item.fillTransparencySettings
    if(typeof setting != "undefined") {
        var has = this._hasEffects(setting);
        if(has)
            return true;
    }
    // strokeTransparencySettings
     setting = org_item.strokeTransparencySettings
    if(typeof setting != "undefined") {
        var has = this._hasEffects(setting);
        if(has)
            return true;
    }
   // transparencySettings
     setting = org_item.transparencySettings
    if(typeof setting != "undefined") {
        var has = this._hasEffects(setting);
        if(has)
            return true;
    }
}

REDPageItem.prototype.IsAllPaths = function() {
    
    var child_items = new Array;
    
    this.getherItems(child_items);

    var i, icnt = child_items.length;

    if(icnt > 0) {
    
        for(i=0;i<icnt;i++) {
            
            var item = child_items[i];
            
            if(typeof item.paths == "undefined") {
                return false;
            }
            if(this.hasEffects(item)) {
                return false;
            }
            //Background Anotation시 처리 //20190401 Kraken
            // if (this.layer.name == "Background" && item.constructor.name == "TextFrame"){
            //     return false;
            // }
            if (this.layer.name.includes("Background") && item.constructor.name == "TextFrame"){
                return false;
            }
        }
    }
    if(this.hasEffects(this.org_item)) {
        return false;
    }

    return true;
}

REDPageItem.prototype.getTextAttrList = function(paragraphs) {
    var text_list = new Array;
    var icnt = paragraphs.length;
    var i
    for(i=0;i<icnt;i++){
        var para = paragraphs[i];

        var text_ranges = para.textStyleRanges;
        
        var j, jcnt =text_ranges.length;
        for(j=0;j<jcnt;j++) {
            var text_range = text_ranges[j];
            var text_item = new Object;
            var contents = text_range.contents;
            
            if(text_range.index + text_range.length > para.index + para.length) {
                var new_len = (para.index + para.length) - text_range.index;
                contents = text_range.contents.substr(0, new_len);
            }
            this.document.registerFont(text_range.appliedFont, text_range.fontStyle);
            text_item["contents"] = ReplaceEndLine(contents);
            text_item["char_style"] = text_range.appliedCharacterStyle.name;
            text_item["para_style"] = text_range.appliedParagraphStyle.name;
            text_item.font_family = getFontFamilyName(text_range.appliedFont); //.fontFamily;
            //text_item.font_family = text_range.appliedFont.toString();
            text_item.font_style = text_range.fontStyle ;
            text_item.font_size = text_range.pointSize;
            text_item.auto_leading = (text_range.leading == Leading.AUTO);
            text_item.leading = text_item.auto_leading  ? 0 :text_range.leading;
            text_item.tracking = text_range.tracking;
            text_item.kerning = text_range.kerningMethod;
            text_item.h_scale = text_range.horizontalScale;
            text_item.v_scale = text_range.verticalScale ;
            text_item.skew = text_range.skew;
            text_item.baseline = text_range.baselineShift;
            text_item.fill_color = new REDColor(text_range.fillColor, null);
            text_item.stroke_color = new REDColor(text_range.strokeColor, null);
            text_item.fill_tint = text_range.fillTint;
            text_item.stroket_tint = text_range.strokeTint;
            text_item.capitalization = capitalization(text_range.capitalization);
            text_item.position = position(text_range.position) ;
            text_item.underline = text_range.underline;
            text_item.strike_thru = text_range.strikeThru;
            text_item.justification = justification(text_range.justification);
            text_item.first_indent = text_range.firstLineIndent;
            text_item.left_indent = text_range.leftIndent;
            text_item.right_indent =text_range.rightIndent;
            text_item.space_before = text_range.spaceBefore;
            text_item.space_after = text_range.spaceAfter;
            text_item.auto_leading = text_range.autoLeading;
            text_item.leading = text_range.leading == Leading.AUTO ? "AUTO" :  text_range.leading ;
            text_item.tracking = text_range.tracking;
            text_item.horizontal_scale = text_range.horizontalScale;
            
            text_list.push(text_item);
        }
    }
    return text_list;
}
////////////////////////////////////////////////// //////////////////////////////////////////////////  REDSpline
function REDSpline(org_item, doc) {
    REDPageItem.call(this, org_item, doc);    
    this.type = "Spline";  // <Type> Tag    
    }

REDSpline.prototype = new REDPageItem();
REDSpline.prototype.constructor = REDSpline;

function isSVGLayer(layer_name)
{
    if(layer_name == "Cut") {
        return true;
    }
    if(layer_name == "Crease") {
        return true;
    }
    if(layer_name == "DashRule") {
        return true;
    }
    if(layer_name == "Punch") {
        return true;
    }
    if(layer_name == "Folding") {
        return true;
    }
    if(layer_name == "Scodix") {
        return true;
    }
    if(layer_name == "ScodixGold") {
        return true;
    }
    if(layer_name == "ScodixSilver") {
        return true;
    }
    if(layer_name == "Embossing") {
        return true;
    }
    if(layer_name == "GoldFoil") {
        return true;
    }
    if(layer_name == "SilverFoil") {
        return true;
    }
    if(layer_name == "KissCut") {
        return true;
    }
    if(layer_name == "White" || layer_name == "White@{render:true}") {
        return true;
    }
    //Background Anotation시 처리 //20190401 Kraken
    // if(layer_name == "Background") {
    //     return true;
    // }
    if(layer_name.includes("Background")) {
        return true;
    }
    if(layer_name == "Svgart") {
        return true;
    }
    if(layer_name == "BevelOutlineCut") {
        return true;
    }
    if(layer_name == "BevelCut") {
        return true;
    }
    if(layer_name.split('@')[0] == "Cut") {
        return true;
    }
    return false;
}

function end_cap(endcap)
{
    switch(endcap) {
            case EndCap.BUTT_END_CAP: 
                return "BUTT_END_CAP";
                break;
            case EndCap.PROJECTING_END_CAP: 
                return "PROJECTING_END_CAP";
                break;
            case EndCap.ROUND_END_CAP: 
                return "ROUND_END_CAP";
                break;
    }
}

function end_join(endjoin)
{
   switch(endjoin) {
            case EndJoin.BEVEL_END_JOIN: 
                return "BEVEL_END_JOIN";
                break;
            case EndJoin.MITER_END_JOIN: 
                return "MITER_END_JOIN";
                break;
            case EndJoin.ROUND_END_JOIN: 
                return "ROUND_END_JOIN";
                break;
    }
}

REDSpline.prototype.jsonObj = function() {
    var jsonobj = REDPageItem.prototype.jsonObj.call(this);
    var redfill_color = new REDColor(this.org_item.fillColor, this.org_item.fillTint);
    jsonobj["fill_color"] = redfill_color;
    jsonobj["stroke_color"] = new REDColor(this.org_item.strokeColor, this.org_item.fillTint);
    jsonobj["stroke_weight"] = this.org_item.strokeWeight;
    jsonobj["fill_tint"] = this.org_item.fillTint;
    jsonobj["stroke_tint"] = this.org_item.strokeTint;
    jsonobj["end_cap"] = end_cap(this.org_item.endCap);
    jsonobj["end_join"] = end_join(this.org_item.endJoin);
    if(redfill_color.color_name == "c0m0y0k50") {
        jsonobj["mask"] = "yes";
    }
    
    var angle = -this.org_item.rotationAngle;
    var shear = this.org_item.shearAngle;
    if(angle != 0) {
        var org_anchor = app.activeWindow.transformReferencePoint;
        app.activeWindow.transformReferencePoint = AnchorPoint.CENTER_ANCHOR         
        try {
            this.org_item.rotationAngle = 0;
            this.org_item.shearAngle = 0;
        }
        catch (e) {
            $.writeln(e);
        }
    }
    var text_cnt = this.org_item.textPaths.length;
    if(text_cnt > 0) {
        var text_paths = new Array;
        var i;
        for(i=0;i<text_cnt;i++) {
            var text_path_json =   new Object;

            var text_path = this.org_item.textPaths[i];
            var text_attr_list = this.getTextAttrList(text_path.paragraphs);
            text_path_json["text_list"] = text_attr_list;
            text_path_json["start_bracket"] = text_path.startBracket;
            text_path_json["end_bracket"] = text_path.endBracket;
            text_path_json["center_bracket"] = text_path.centerBracket;
            text_path_json["flip_path_effect"] = flip_values(text_path.flipPathEffect);  //
            text_path_json["path_alignment"] = path_type_alignments(text_path.pathAlignment);  //
            text_path_json["path_effect"] = text_path_effects(text_path.pathEffect);  //
            text_path_json["path_spacing"] = text_path.pathSpacing;
            text_paths.push(text_path_json);
        }
        jsonobj["text_paths"] = text_paths;
        
    }
    
    var org_paths = this.org_item.paths;

    if(org_paths != undefined && org_paths != null) {
        
        //Calendar상품 Path Layer Item들의 Paths정보를 추출하기위해서 this.layer.name == "Path-GridCalendar" || this.layer.name == "Path-LineCalendar" 를 if문에 추가함 -- 20191017 Kraken
        if(this.layer.name == "Path-GridCalendar" || this.layer.name == "Path-LineCalendar" || !REMOVE_PATHS || jsonobj.fill_color.color_name == "c0m0y0k50" || text_cnt > 0) {
            var path_list = new Array;
            var i, icnt = org_paths.length;
            for(i=0;i<icnt;i++) {
                var path_json = new Object;
                var org_path = org_paths[i];
                var path = this.relativePaths(org_path.entirePath);
                //var path = org_path.entirePath;

                // PageMask 레이어의 path를 소수점 3자리까지만 나오도록
                var newPath = [];
                
                if(this.layer.name == "PageMask") {
                    for(var a=0;a<path.length;a++) {
                        if(typeof path[a][0] == "number") {
                            var point = [];
                            for (var m=0; m<2; m++) {
                                if((path[a][m]).toString().includes("e")) {
                                    point.push(Number((path[a][m]).toExponential(3)));
                                } else {
                                    point.push(Number((path[a][m]).toFixed(3)));
                                }
                            }
                            newPath.push(point);
                        } else {
                            var firstPoint = []
                            for(var n=0;n<path[a].length;n++) {
                                var secondPoint = [];
                                for (var m=0; m<2; m++) {
                                    if ((path[a][n][m]).toString().includes("e")) {
                                        secondPoint.push(Number((path[a][n][m]).toExponential(3)));
                                    } else {
                                        secondPoint.push(Number((path[a][n][m]).toFixed(3)));
                                    }
                                }
                                firstPoint.push(secondPoint);
                            }
                            newPath.push(firstPoint);
                        }
                    }

                    path_json["path"] = newPath;
                    path_json["pathType"] = path_type(org_path.pathType); 
                } else {
                    path_json["path"] = path;
                    path_json["pathType"] = path_type(org_path.pathType); 
                }
                // path_json["path"] = path;
                // path_json["pathType"] = path_type(org_path.pathType); 
                path_list.push(path_json);
            }
            jsonobj["path_count"] = path_list.length;
            jsonobj["paths"] = path_list;
        }
        
        // 20180922 가이드의 경우 도 아래와 같은 처리를 하도록 한다. - Irelander_kai 
	    if( this.layer.name.includes("Guide") || this.layer.name.includes("Select") || (this.layer.name == "PicFrame" && this.document.suboption != "r")) {

            if(jsonobj.fill_color.color_name != "c0m0y0k50"){
                var parent = this.org_item.parent;
                if( parent.constructor.name.includes("Group")){
                    parent = parent.parent;
                    if(parent.constructor.name == "Spread") {
                        var img_file;
                        if(this.IsAllPaths()) {
                            img_file = this.makeSVG();
                        }
                        else {
                            img_file = this.makePNG(); // jsonObj()
                        }
                        var img_type = img_file.name.slice(img_file.name.length - 3);
                        
                        jsonobj.image_path = img_file.fullName;
                        jsonobj.image_file = img_file.name;
                        jsonobj.image_type = img_type.toUpperCase (); //"SVG";
                    }
                }else if(this.layer.name.includes("Guide")){
                    var img_file;
                    if(this.IsAllPaths()) {
                        img_file = this.makeSVG();
                    }else {
                        img_file = this.makePNG(); //jsonObj()
                    }
                    var img_type = img_file.name.slice(img_file.name.length - 3);
                    
                    jsonobj.image_path = img_file.fullName;
                    jsonobj.image_file = img_file.name;
                    jsonobj.image_type = img_type.toUpperCase (); //"SVG";
                    
                }else if(this.org_item.name.includes("Guide")){
                    
                    var img_file;
                    if(this.IsAllPaths()) {
                        img_file = this.makeSVG();
                    }else {
                        img_file = this.makePNG(); //jsonObj()
                    }
                    var img_type = img_file.name.slice(img_file.name.length - 3);
                    
                    jsonobj.image_path = img_file.fullName;
                    jsonobj.image_file = img_file.name;
                    jsonobj.image_type = img_type.toUpperCase (); //"SVG";
                }else if(this.org_item.name.includes("RenderBound")){
                    
                    var img_file;
                    if(this.IsAllPaths()) {
                        img_file = this.makeSVG();
                    }else {
                        img_file = this.makePNG(); //jsonObj()
                    }
                    var img_type = img_file.name.slice(img_file.name.length - 3);
                    
                    jsonobj.image_path = img_file.fullName;
                    jsonobj.image_file = img_file.name;
                    jsonobj.image_type = img_type.toUpperCase (); //"SVG";
                }

            }
        }
        //캘린더에서 Sticker-1레이어가 추가되어 분기문에 추가
        else if(this.layer.name == "Sticker"  || isSVGLayer(this.layer.name) || this.layer.name == "Sticker-1") {
            var parent = this.org_item.parent;
            //Background Anotation시 처리 //20190401 Kraken
            // if(parent.constructor.name == "Spread"  && this.layer.name != "Background") {
            if(parent.constructor.name == "Spread"  && this.layer.name.includes("Background") == false) {
                    var img_file;
                    if(this.IsAllPaths()) {
                        img_file = this.makeSVG();
                    }
                    else {
                        img_file = this.makePNG(); // jsonObj ()
                        if(this.layer.name == "Sticker" || this.layer.name == "Sticker-1") {
                            var imgfiledoc = {
                                fileName : img_file.name,
                                width : this.abs_frame.size.width,
                                height: this.abs_frame.size.height
                            };
                            pngData.push(imgfiledoc);
                        }
                    }
                    this.image_path
                    var img_type = img_file.name.slice(img_file.name.length - 3);
                    jsonobj.image_path = img_file.fullName;
                    jsonobj.image_file = img_file.name;
                    jsonobj.image_type = img_type.toUpperCase ();; //"SVG";
            }
        }
    }
    if(angle != 0) {
        this.org_item.rotationAngle = -angle;
        this.org_item.shearAngle = shear;
        app.activeWindow.transformReferencePoint = org_anchor;         
    }

    // oval, polygon 등이 spline으로 연결되어있음. background에 객체가 한개 있으면 spline으로 들어오는데 그룹일 떄와 마찬가지로 background type 지정 로직 추가해줌.
    // 스탬프 템플릿일 경우에만 해당하고 있어서.. 일단 productCode로 걸어둠..
    if (this.layer.name.includes("Background") && _productCode == 'GSSMTMP') {
        var parent = this.org_item.parent;
        if(parent.constructor.name == "Spread") {
            var img_file;
            if(this.layer.name.includes("Background") ) {
                // 기존 플로우 그대로 (default)
                if(backgroundType == 'default') {
                    // Background 도 path만 존재하는 경우에는 SVG로 변경해서 올린다.
                    if (this.IsAllPaths()){
                        img_file = this.makeSVG();
                    }else{
                        img_file = this.makeJPG();
                    }
                }
                // backgroundType 확장자를 jpg로 설정했을 때
                else if( backgroundType == 'jpg') {
                    img_file = this.makeJPG();
                }
                // backgroundType 확장자를 png로 설정했을 때
                else if( backgroundType == 'png') {
                    
                    img_file = this.makePNG();
                }
                // backgroundType 확장자를 svg로 설정했을 때
                else if( backgroundType == 'svg') {
                    img_file = this.makeSVG();
                }
            }else {
                img_file = this.makePNG();
            }
            var img_type = img_file.name.slice(img_file.name.length - 3);
            jsonobj.image_path = img_file.fullName;
            jsonobj.image_file = img_file.name;
            jsonobj.image_type = img_type.toUpperCase();
        }
    }
     return jsonobj;
}

REDSpline.prototype.relativePaths = function(path) {
    if(this.parent.constructor.name == "REDPage") {
        return path;
    }
    var new_path = new Array;
    var i, icnt = path.length;
    for(i=0;i<icnt;i++) {
        var new_pts = new Array;
        var pts = path[i];
        var j, jcnt = pts.length;
        for(j=0;j<jcnt;j++) {
            var pt = pts[j];
            if(pt.constructor.name == "Array") {
                var new_pt = new Array;
                var x = pt[0] - this.parent.abs_frame.origin.x;
                var y = pt[1] - this.parent.abs_frame.origin.y;
                new_pt.push(x);
                new_pt.push(y);
                new_pts.push(new_pt);
            }
            else {
                var new_pt = 0;
                if(j == 0) {
                    new_pt = pt - this.parent.abs_frame.origin.x;
                }
                else {
                    new_pt = pt - this.parent.abs_frame.origin.y;
                }
                new_pts.push(new_pt);
                
            }
        }
        new_path.push(new_pts);
     }
    return new_path;
}


////////////////////////////////////////////////// //////////////////////////////////////////////////  REDRectangle
function REDRectangle(org_item, doc) {

    REDSpline.call(this, org_item, doc);    
    this.type = "Rectangle";  // <Type> Tag    
    }

REDRectangle.prototype = new REDSpline();
REDRectangle.prototype.constructor = REDRectangle;

function corner_opt(corner)
{
    switch(corner) {
        case CornerOptions.NONE:
            return "NONE";
            break;
        case CornerOptions.ROUNDED_CORNER:
            return "ROUNDED_CORNER";
            break;
        case CornerOptions.INVERSE_ROUNDED_CORNER:
            return "INVERSE_ROUNDED_CORNER";
            break;
        case CornerOptions.INSET_CORNER:
            return "INSET_CORNER";
            break;
        case CornerOptions.BEVEL_CORNER:
            return "BEVEL_CORNER";
            break;
        case CornerOptions.FANCY_CORNER:
            return "FANCY_CORNER";
            break;
    }
}

REDRectangle.prototype.jsonObj = function() {
    var jsonobj = REDSpline.prototype.jsonObj.call(this);
    jsonobj["top_left_corner_opt"] = corner_opt(this.org_item.topLeftCornerOption);
    jsonobj["top_left_corner_radius"] = this.org_item.topLeftCornerRadius;
    jsonobj["top_right_corner_opt"] = corner_opt(this.org_item.topRightCornerOption);
    jsonobj["top_right_corner_radius"] = this.org_item.topRightCornerRadius;
    jsonobj["bottom_left_corner_opt"] = corner_opt(this.org_item.bottomLeftCornerOption);
    jsonobj["bottom_left_corner_radius"] = this.org_item.bottomLeftCornerRadius;
    jsonobj["bottom_right_corner_opt"] = corner_opt(this.org_item.bottomLeftCornerOption);
    jsonobj["bottom_right_corner_radius"] = this.org_item.bottomLeftCornerRadius;
     return jsonobj;
    }
////////////////////////////////////////////////// //////////////////////////////////////////////////  REDOval
function REDOval(org_item, doc) {
    REDSpline.call(this, org_item, doc);    
    this.type = "Oval";  // <Type> Tag    
    }

REDOval.prototype = new REDSpline();
REDOval.prototype.constructor = REDOval;

REDOval.prototype.jsonObj = function() {
    var jsonobj = REDSpline.prototype.jsonObj.call(this);
     return jsonobj;
    }
////////////////////////////////////////////////// //////////////////////////////////////////////////  REDPolygon
function REDPolygon(org_item, doc) {
    REDSpline.call(this, org_item, doc);    
    this.type = "Polygon";  // <Type> Tag    
    }

REDPolygon.prototype = new REDSpline();
REDPolygon.prototype.constructor = REDPolygon;

REDPolygon.prototype.jsonObj = function() {
    var jsonobj = REDSpline.prototype.jsonObj.call(this);
     return jsonobj;
    }

////////////////////////////////////////////////// //////////////////////////////////////////////////  REDGraphicLine
function REDGraphicLine(org_item, doc) {
    REDSpline.call(this, org_item, doc);    
    this.type = "GraphicLine";  // <Type> Tag    
    }

REDGraphicLine.prototype = new REDSpline();
REDGraphicLine.prototype.constructor = REDGraphicLine;

REDGraphicLine.prototype.jsonObj = function() {
    var jsonobj = REDSpline.prototype.jsonObj.call(this);
     return jsonobj;
    }

	
/////////////////////////////////////////////////// //////////////////////////////////////////////////  REDImage
function REDImage(org_image, doc) {
    REDPageItem.call(this, org_image, doc);    
    this.type = "Image";  // <Type> Tag    
    var image_path = null;
    var image_link = org_image.itemLink;    
    // if(image_link != null && (doc.suboption != "r" && this.layer.name === "PicFrame" )) {
        if(image_link != null && (doc.suboption != "r" && this.layer.name === "PicFrame" )) {   
        image_path = image_link.filePath;
        
    }
 //   if(image_path != null && this.layer.name != "PicFrame" && this.layer.name != "Background") {

     //Background Anotation시 처리 //20190401 Kraken
     //if(image_path != null && this.layer.name != "Background") {
     if(image_path != null && this.layer.name.includes("Background") == false) {
     var ext = get_file_ext(image_path);
            var src_file = new File(image_path);
            var doc_file = new File(this.document.path);
            var red_folder_path = doc_file.parent.fullName;
            var fname = this.org_item.id;;
            var tar_path = red_folder_path+ "/" + fname;
            tar_path = tar_path + ".png";
            var tar_file = new File(tar_path);
            exportItemAsImage(this.org_item, tar_file, 300, PNGQualityEnum.MEDIUM);
            this.image_path = tar_path;
            
    }
    this.bounds = new REDRect(org_image.geometricBounds).jsonObj();
    this.flip = image_flip(org_image.absoluteFlip);
    try {
       this.org_mage_type = org_image.imageTypeName;
    }
    catch (e) {
        this.org_mage_type = "unknown";
    }
    this.image_type = "PNG";
    try {
        this.color_space = org_image.space;
    }
    catch (e) {
        this.color_space = "unknown";
    }
    this.v_scale = org_image.verticalScale;
    this.h_scale = org_image.horizontalScale;
    //this.visible_bounds = new REDRect(org_image.visibleBounds).jsonObj();
    
}

REDImage.prototype = new REDPageItem();
REDImage.prototype.constructor = REDImage;

REDImage.prototype.jsonObj = function() {
    var jsonobj = REDPageItem.prototype.jsonObj.call(this);
    if(this.image_path != null) {
        jsonobj.image_path = this.image_path;
        
        var img_file = new File(jsonobj.image_path);
        jsonobj.image_file = img_file.name;
        }
    jsonobj.bounds = this.relativeBounds(this.bounds).jsonObj();
    if(this.org_image_type) {
        jsonobj.org_image_type = this.org_image_type;
    }
    jsonobj.image_type = this.image_type;
    jsonobj.color_space = this.color_space;
    jsonobj.v_scale = this.v_scale;
    jsonobj.h_scale = this.h_scale;
     return jsonobj;
    }

////////////////////////////////////////////////// //////////////////////////////////////////////////  REDPDF
function REDPDF(org_image, doc) {
    REDPageItem.call(this, org_image, doc);    
    this.type = "Image";  // <Type> Tag    
    var image_path = org_image.itemLink.filePath;
    //Background Anotation시 처리 //20190401 Kraken
    // if(this.layer.name != "PicFrame" && this.layer.name != "Background") {
    if(this.layer.name != "PicFrame" && this.layer.name.includes("Background") == false) {
        var src_file = new File(image_path);
        var doc_file = new File(this.document.path);
        var red_folder_path = doc_file.parent.fullName;
        var fname = this.org_item.id;
        var tar_path = red_folder_path+ "/" + fname;
        tar_path = tar_path + ".png";
        var tar_file = new File(tar_path);
        exportItemAsImage(this.org_item, tar_file, 300, PNGQualityEnum.MEDIUM); 
        this.image_path = tar_path;
        
    }

    this.bounds = new REDRect(org_image.geometricBounds).jsonObj();
    this.flip = image_flip(org_image.absoluteFlip);
    this.org_image_type = org_image.imageTypeName;
    this.image_type = "PNG";

//    this.color_space = org_image.space;
    this.v_scale = org_image.verticalScale;
    this.h_scale = org_image.horizontalScale;
    }


REDPDF.prototype = new REDPageItem();
REDPDF.prototype.constructor = REDPDF;

REDPDF.prototype.jsonObj = function() {
    var jsonobj = REDPageItem.prototype.jsonObj.call(this);
    if(this.image_path != null) {
        jsonobj.image_path = this.image_path;
        var img_file = new File(jsonobj.image_path);
        jsonobj.image_file = img_file.name;
        
    }
    jsonobj.bounds = this.relativeBounds(this.bounds).jsonObj();
    if(this.org_image_type) {
        jsonobj.org_image_type = this.org_image_type;
    }
    
    jsonobj.image_type = this.image_type;
    jsonobj.color_space = this.color_space;
    jsonobj.v_scale = this.v_scale;
    jsonobj.h_scale = this.h_scale;
     return jsonobj;
    }
////////////////////////////////////////////////// //////////////////////////////////////////////////  REDGroup
function REDGroup(org_item, doc) {
    
    REDPageItem.call(this, org_item, doc);    
    this.type = "Group";  // <Type> Tag    
    }

REDGroup.prototype = new REDPageItem();
REDGroup.prototype.constructor = REDGroup;


/*

REDGroup.prototype.makeSVG = function()
{
        var objlist = new Array();
        //this.getherItems(objlist);
        objlist.push(this.org_item);
        var svg = convertToSVG(objlist);
        var docpath = this.document.path;
        var docfile = new File(docpath);
        var docfolder = docfile.parent;
        var svgpath = docfolder + "/" + this.org_item.id + ".svg";
        var svgfile = new File(svgpath);
        saveSVGFile(svg, svgfile);
        return svgfile;
    }
*/

REDGroup.prototype.jsonObj = function() {
    var jsonobj = REDPageItem.prototype.jsonObj.call(this);
    
    if( this.layer.name.includes("Guide") || this.layer.name.includes("Select") || (this.layer.name == "PicFrame" && this.document.suboption != "r" )) {
        var parent = this.org_item.parent;
        if( parent.constructor.name.includes("Group") ) {
            parent = parent.parent;
            if(parent.constructor.name == "Spread") {
                var img_file;
                if(this.IsAllPaths()) {
                    img_file = this.makeSVG();
                }
                else {
                    img_file = this.makePNG(); // REDGroup.jsonObj()
                }
                var img_type = img_file.name.slice(img_file.name.length - 3);
                jsonobj.image_path = img_file.fullName;
                jsonobj.image_file = img_file.name;
                jsonobj.image_type = img_type.toUpperCase(); //"SVG";
                
            }
        }else if( this.layer.name.includes("Guide") ){
            var img_file;
            if(this.IsAllPaths()) {
                img_file = this.makeSVG();
            }
            else {
                img_file = this.makePNG();  // REDGroup.jsonObj()
            }
            var img_type = img_file.name.slice(img_file.name.length - 3);
            jsonobj.image_path = img_file.fullName;
            jsonobj.image_file = img_file.name;
            jsonobj.image_type = img_type.toUpperCase(); //"SVG";

        }else if(this.org_item.name.includes("Guide")){

            var img_file;
                if(this.IsAllPaths()) {
                    img_file = this.makeSVG();
                }
                else {
                    img_file = this.makePNG();  // REDGroup.jsonObj()
                }
                var img_type = img_file.name.slice(img_file.name.length - 3);
                jsonobj.image_path = img_file.fullName;
                jsonobj.image_file = img_file.name;
                jsonobj.image_type = img_type.toUpperCase(); //"SVG";
        }else if(this.org_item.name.includes("RenderBound")){

            var img_file;
                if(this.IsAllPaths()) {
                    img_file = this.makeSVG();
                }
                else {
                    img_file = this.makePNG();  // REDGroup.jsonObj()
                }
                var img_type = img_file.name.slice(img_file.name.length - 3);
                jsonobj.image_path = img_file.fullName;
                jsonobj.image_file = img_file.name;
                jsonobj.image_type = img_type.toUpperCase(); //"SVG";
        }
    }
    //Background Anotation시 처리 //20190401 Kraken
    //캘린더에서 Sticker-1레이어가 추가되어 분기문에 추가
    // else if(this.layer.name == "Background" || this.layer.name == "Sticker"  ||  isSVGLayer(this.layer.name)) {
    else if(this.layer.name.includes("Background") || this.layer.name == "Sticker"  ||  isSVGLayer(this.layer.name) || this.layer.name == "Sticker-1") {
        var parent = this.org_item.parent;
        if(parent.constructor.name == "Spread") {
            var img_file;
            //Background Anotation시 처리 //20190401 Kraken
            // if(this.IsAllPaths() && this.layer.name != "Background") {
            if(this.IsAllPaths() && this.layer.name.includes("Background") == false) {
                img_file = this.makeSVG();
            }
            //Background Anotation시 처리 //20190401 Kraken
            // else if(this.layer.name == "Background") {
            else if(this.layer.name.includes("Background") && _productCode == 'GSSMTMP') {
                // 기존 플로우 그대로 (default)
                if(backgroundType == 'default') {
                    // Background 도 path만 존재하는 경우에는 SVG로 변경해서 올린다. 
                    if (this.IsAllPaths()){
                        img_file = this.makeSVG();
                    }else{
                        img_file = this.makeJPG();
                    }
                } 
                // backgroundType 확장자를 jpg로 설정했을 때
                else if( backgroundType == 'jpg') {
                    img_file = this.makeJPG();
                }
                // backgroundType 확장자를 png로 설정했을 때
                else if( backgroundType == 'png') {
                    img_file = this.makePNG();
                } 
                // backgroundType 확장자를 svg로 설정했을 때
                else if( backgroundType == 'svg') {
                    img_file = this.makeSVG();
                }
            }else {
                img_file = this.makePNG();
            }
            var img_type = img_file.name.slice(img_file.name.length - 3);
            jsonobj.image_path = img_file.fullName;
            jsonobj.image_file = img_file.name;
            jsonobj.image_type = img_type.toUpperCase(); //"SVG";
        }
    }
    return jsonobj;
}

/////////////////////////////////////////////////// //////////////////////////////////////////////////  REDTextFrame
function REDTextFrame(org_item, doc) {
    REDPageItem.call(this,org_item, doc);    
    this.type = "TextFrame";  // <Type> Tag    
    this.text_ranges = new Array();
    
    }

REDTextFrame.prototype = new REDPageItem();
REDTextFrame.prototype.constructor = REDTextFrame;


REDTextFrame.prototype.jsonObj = function() {
    var jsonobj = REDPageItem.prototype.jsonObj.call(this);
    var preferences = this.org_item.textFramePreferences;
    jsonobj["column_count"] = preferences.textColumnCount;
    var insets = preferences.insetSpacing;
    jsonobj["inset_top"] = insets[0];
    jsonobj["inset_left"] = insets[1];
    jsonobj["inset_bottom"] = insets[2];
    jsonobj["inset_right"] = insets[3];
    jsonobj["gutter"] = preferences.textColumnGutter;
    var label_str = this.org_item.extractLabel("FIELD");
    if(label_str == null || typeof label_str == "undefined") {
        label_str = "";
    }
    jsonobj["label"] = label_str;
   /*
    var parent_page = this.getPage();
    if(typeof parent_page != "undefined" && parent_page != null) {
        var text_contents = this.org_item.contents;
        var text_label = this.org_item.extractLabel(TEXT_LABEL_KEY);
        parent_page.addLabelText(text_label, text_contents);
    }
   */
    var text_list = this.getTextAttrList(this.org_item.paragraphs);
    jsonobj["text_list"] = text_list;
    return jsonobj;
    }

///////////////////////////////////////////////// //////////////////////////////////////////////////  REDPage
//마스터정보 추출을 위해 REDPage에 페이지번호 인자값 doc_page_num를 추가 --20190619 Kraken
function REDPage(org_page, page_num, doc, doc_page_num) {
    
    this.type = "Page";  // <Type> Tag
    this.document = doc;
    var b;
    var masterDoc = app.documents[0];
    var active_page = masterDoc.pages[doc_page_num];
    
    if(doc.option == "P") {
        b = [0, 0, doc.page_size.height,doc.page_size.width];
    }
    else {
        b = org_page.bounds;
    }

    this.rect = REDRect.RectFromArray(b);
    this.abs_frame = REDRect.RectFromArray(b);
    this.page_number = page_num;
    this.org_page = org_page;
    this.text_list = new Array();
    this.page_items = new Array();
    this.buildObjectModel();
    this.masterName = active_page.appliedMaster.name.substring(2,active_page.appliedMaster.name.length)
    }



function featureExclusion  (data, out) {
    var e, k, results;
    if (out === null) {
        out = {};
    }
    if (!data) {
        return void 0;
    }
    if (typeof data === 'object') {
        results = [];
        for (k in data) {
            if (k === "theComp" || k === "parentFolder" || k === "source") {
                if (data[k]) {
                    out[k] = data[k].id;
                }
            }
        }
        return results;
    } else {
        return data;
    }
}

REDPage.prototype = new REDObject();
REDPage.prototype.constructor = REDPage;

REDPage.prototype.buildObjectModel = function() {
    // 레이어 순서를 핸들링 하기위해 레이어 별로 pageItem을 분류해서 담는다.
    
    var all_pageitems = this.org_page.allPageItems;
    if(this.document.option != "D") {
        var spread = this.org_page.parent;
        all_pageitems = spread.allPageItems;
    }

    var layers_arr = new Array;
    var i = 0, icnt = all_pageitems.length;
    var max_idx = -1;
    while(i<icnt) {
        var pitem = all_pageitems[i];
        var item_length = pitem.allPageItems.length;
        i += item_length + 1;
        
        var item_idx = pitem.itemLayer.index;
        if(item_idx > max_idx) {
            max_idx = item_idx;
        }
        var item_list = layers_arr[item_idx];
        if(item_list == null || typeof item_list == "undefined") {
            item_list = new Array;
            layers_arr[item_idx] = item_list;
        }
        item_list.push(pitem);
    }

    var sorted_items = new Array;
    icnt = max_idx+1;
    
    for(i=0;i<icnt;i++){
        var item_list = layers_arr[icnt - i -1];
        if(item_list != null && typeof item_list != "undefined") {
            var j, jcnt = item_list.length;
            if(jcnt > 0) {
                var first = item_list[0];
                //Background Anotation시 처리 //20190401 Kraken
                // if(first.itemLayer.name == "Background") {
                if(first.itemLayer.name.includes("Background")) {
                    // Backround 레이어는 그룹 하나로 묶는다.
                    if(jcnt > 1) {
                        var group_items = new Array;
                        var have_locked = false;
                        for(j=0;j<jcnt;j++) {
                            var pitem = item_list[jcnt-j-1];
                            group_items.push(pitem);
                            if(pitem.locked) {
                                have_locked = true;
                            }
                        }
                        if(have_locked) {
                            for(j=0;j<jcnt;j++) {
                                var pitem = item_list[j];
                                sorted_items.push(pitem);
                            }
                        }
                        else {
                            var new_group = this.org_page.groups.add(group_items);
                            this.document.added_groups.push(new_group);
                            sorted_items.push(new_group);
                        }
                    }
                    else {   
                         sorted_items.push(first);
                    }
                }
                else {
                    for(j=0;j<jcnt;j++) {
                        var pitem = item_list[jcnt-j-1];
                        sorted_items.push(pitem);
                    }
                }
            }
        }
    }
    
    this.document.load_page_items(this.org_page, this, sorted_items);
//    this.document.load_page_items(this.org_page, this);
    if(this.document.option == "P") {
        if(this.org_page.pages.length == 1) {
            var a_page = this.org_page.pages[0];
            if(a_page.side == PageSideOptions.RIGHT_HAND) {
                var org_page_width = this.document.org_doc.documentPreferences.pageWidth;
                icnt = this.page_items.length;
                for(i=0;i<icnt;i++) {
                    var red_page_item = this.page_items[i];
                    var type_str = red_page_item.org_item.extractLabel("TYPE");
                    if(type_str != "TEMP") {
                            //red_page_item.frame.origin.x += org_page_width;
                    }
                }
            }
        }
        this.side = "single";
    }
    else {
        switch (this.org_page.side) {
            case PageSideOptions.RIGHT_HAND:
                this.side = "right";
                break;
            case PageSideOptions.LEFT_HAND:
                this.side = "left";
                break;
            case PageSideOptions.SINGLE_SIDED:
                this.side = "single";
                break;
            default:
                this.side = "single";
                break;
        }
    }
}

REDPage.prototype.jsonObj = function() {
    
    var jsonobj = new Object;
    jsonobj["id"] = this.org_page.id;
    jsonobj["frame"] = this.rect.jsonObj();
    jsonobj["page_number"] = this.page_number;
    jsonobj["side"] = this.side;
    jsonobj["masterName"] = this.masterName;

    
    var jsonitems = new Array();
    var i, icnt = this.page_items.length;
    for(i=0;i<icnt;i++) {

        var pageitem = this.page_items[i];
        var jsonitem = pageitem.jsonObj();
        jsonitems.push(jsonitem);
        }
    jsonobj["page_items"] = jsonitems;
    
    var doc_file = new File(this.document.path);
    var red_folder_path = doc_file.parent.fullName;

 //   var page_num_str = zeroFilledStrFromNum(pgnum);
    
    if(this.page_number == 1) {
 
        if(this.org_page.constructor.name == "Spread") {
            var sub_pages = this.org_page.pages;
            var j, jcnt = sub_pages.length;
            for(j=0;j<jcnt;j++) {
                var left_right = "";
                pg_idx = i + 1;
                var sub_page = sub_pages[j];
                if(sub_page.side == PageSideOptions.RIGHT_HAND) {
                    left_right = "r";
                }
                else if(sub_page.side == PageSideOptions.LEFT_HAND){
                    left_right = "l";
                }
                else {
                    left_right = "m";
                }
                var file_path = red_folder_path + "/template-dp-"+left_right+".png";
 //               var file_path = red_folder_path + "/template-dp-"+this.page_number+"-"+left_right+".png";
                var dpi = dpi_for_thumb_size(this.document.page_size.width, this.document.page_size.height);
                this.savePageAsPNG(sub_page.name, file_path, dpi); 
            }
        }
        else {
          //  var file_path = red_folder_path + "/template-dp-"+this.page_number+".png";
            var file_path = red_folder_path + "/template-dp.png";
            var file_path2 = red_folder_path + "/page-0.png";
            var dpi = dpi_for_thumb_size(this.rect.size.width, this.rect.size.height);
            this.savePageAsPNG(this.org_page.name, file_path, dpi);
            this.savePageAsPNG(this.org_page.name, file_path2, dpi);
        }
 
    }

    return jsonobj;
}
//deplicated
/*
REDPage.prototype.addLabelText = function(label, text) {
    var label_text = new Object;
    label_text.label = label;
    label_text.text = text;
    this.text_list.push(label_text);
}
*/
REDPage.prototype.savePageAsJpeg = function(pgname, file_path, dpi) {
    var img_file = new File(file_path);

    app.jpegExportPreferences.antiAlias = USE_ANTIALIAS;
    app.jpegExportPreferences.jpegExportRange = ExportRangeOrAllPages.exportRange;  
    app.jpegExportPreferences.jpegColorSpace = JpegColorSpaceEnum.RGB;  
 
    app.jpegExportPreferences.embedColorProfile = true;
    app.jpegExportPreferences.jpegRenderingStyle = JPEGOptionsFormat.PROGRESSIVE_ENCODING;
    app.jpegExportPreferences.pageString = pgname;     
    app.jpegExportPreferences.exportResolution =  dpi;
    app.jpegExportPreferences.jpegQuality =  JPEGOptionsQuality.MEDIUM; //jpg_quality_list[this.section.quality];
    cur_doc.exportFile (ExportFormat.jpg, img_file ); 
}

REDPage.prototype.savePageAsPNG = function(pgname, file_path, dpi){
    var img_file = new File(file_path);
    
    app.pngExportPreferences.antiAlias = USE_ANTIALIAS;
    app.pngExportPreferences.pngExportRange = PNGExportRangeEnum.EXPORT_RANGE;  
    app.pngExportPreferences.pngColorSpace =PNGColorSpaceEnum.RGB;  
    
    app.pngExportPreferences.pageString = pgname;     
    app.pngExportPreferences.exportResolution =  dpi;
    app.pngExportPreferences.pngQuality = PNGQualityEnum.MAXIMUM;
    
    app.pngExportPreferences.transparentBackground = true;
    cur_doc.exportFile (ExportFormat.PNG_FORMAT, img_file ); 
}
////////////////////////////////////////////////// //////////////////////////////////////////////////  REDLayer
function REDLayer(org_layer) {
    this.type = "Layer";  // <Type> Tag
    this.name = org_layer.name;
    this.locked = org_layer.locked;
    this.visible = org_layer.visible;
    this.org_layer = org_layer;
    
     }

REDLayer.prototype = new REDObject();
REDLayer.prototype.constructor = REDLayer;

REDLayer.prototype.jsonObj = function() {
    var jsonobj = new Object;
    jsonobj["name"] = this.name;
    jsonobj["locked"] = this.locked;
    jsonobj["visible"] = this.visible;
    return jsonobj;
    }

////////////////////////////////////////////////// //////////////////////////////////////////////////  REDParaStyle
function REDParaStyle(org_pstyle)
{
    this.name = org_pstyle.name;
 //   this.font_name = org_pstyle.appliedFont.name;
    this.font_family = getFontFamilyName(org_pstyle.appliedFont); // .fontFamily;
//    this.font_family = org_pstyle.appliedFont.toString();
    this.font_style = org_pstyle.fontStyle;
    this.font_size = org_pstyle.pointSize;
    this.auto_leading = (org_pstyle.leading == Leading.AUTO);
    this.leading = this.auto_leading ? 0 : org_pstyle.leading;
    this.tracking = org_pstyle.tracking;
    this.skew = org_pstyle.skew;
    this.justification = org_pstyle.justification;
    this.kerning = org_pstyle.kerningMethod;
    this.h_scale = org_pstyle.horizontalScale;
    this.v_scale = org_pstyle.verticalScale;
    this.baseline = org_pstyle.baselineShift;
    this.gridAlignment = org_pstyle.gridAlignment;
    this.space_after = org_pstyle.spaceAfter;
    this.space_bdfore = org_pstyle.spaceBefore;
    this.first_line_indent = org_pstyle.firstLineIndent;
    this.last_line_indent = org_pstyle.lastLineIndent;
    this.left_indent = org_pstyle.leftIndent;
    this.right_indent = org_pstyle.rightIndent;
    this.capitalization = capitalization(org_pstyle.capitalization);

    }

////////////////////////////////////////////////// //////////////////////////////////////////////////  REDCharStyle
function REDCharStyle(org_cstyle)
{
    this.name = org_cstyle.name;
//    this.font_name = org_cstyle.appliedFont.name;
    this.font_family = getFontFamilyName(org_cstyle.appliedFont); //.fontFamily;
//    this.font_family = org_cstyle.appliedFont.toString();
    this.font_style = org_cstyle.fontStyle == NothingEnum.NOTHING ? 0 :org_cstyle.fontStyle ;
    this.font_size = org_cstyle.pointSize == NothingEnum.NOTHING ? 0 :org_cstyle.pointSize ;
    this.auto_leading = (org_cstyle.leading == Leading.AUTO);
    this.leading = this.auto_leading  ? 0 : (org_cstyle.leading == NothingEnum.NOTHING ? 0 : org_cstyle.leading);
    this.tracking = org_cstyle.tracking == NothingEnum.NOTHING ? 0 : org_cstyle.tracking;
    this.kerning = org_cstyle.kerningMethod == NothingEnum.NOTHING ? 0 : org_cstyle.kerningMethod;
    this.h_scale = org_cstyle.horizontalScale == NothingEnum.NOTHING ? 0 : org_cstyle.horizontalScale;
    this.v_scale = org_cstyle.verticalScale == NothingEnum.NOTHING ? 0 : org_cstyle.verticalScale ;
    this.skew = org_cstyle.skew == NothingEnum.NOTHING ? 0 : org_cstyle.skew;
    this.baseline = org_cstyle.baselineShift == NothingEnum.NOTHING ? 0 : org_cstyle.baselineShift;

    this.capitalization = org_cstyle.capitalization == NothingEnum.NOTHING ? 0 :capitalization(org_cstyle.capitalization) ;
    this.position = org_cstyle.position == NothingEnum.NOTHING ? 0 : position(org_cstyle.position) ;
    this.underline = org_cstyle.underline == NothingEnum.NOTHING ? 0 :org_cstyle.underline ;
    this.strike_thru = org_cstyle.strikeThru == NothingEnum.NOTHING ? 0 :org_cstyle.strikeThru ;
    }

function SpreadPage(spread, red_doc)
{
    this.spread = spread;
    var org_pages = spread.pages;
    var page_size =red_doc.page_size
    this.bounds = [0, 0, page_size.height, page_size.width];

}

SpreadPage.prototype.constructor = SpreadPage;

SpreadPage.prototype.rectangles = function(){
    return this.spread.rectangles;
}

////////////////////////////////////////////////// //////////////////////////////////////////////////  REDDocument
function REDDocument(org_doc, json_path) {
    var w = org_doc.documentPreferences.pageWidth;
    var h = org_doc.documentPreferences.pageHeight;
    this.type = "Document";  // <Type> Tag
    this.path = json_path;
    this.page_size = new REDSize(w, h);
    this.top_bleed = org_doc.documentPreferences.documentBleedTopOffset;
    this.bottom_bleed = org_doc.documentPreferences.documentBleedBottomOffset;
    this.in_or_left_bleed = org_doc.documentPreferences.documentBleedInsideOrLeftOffset;
    this.out_or_right_bleed = org_doc.documentPreferences.documentBleedOutsideOrRightOffset;
    this.start_page = -1;
    this.end_page = -1;
//    this.number_of_pages = 0;
    this.spreads = new Array();
    this.pages = new Array();
    this.layers = new Array();
    this.para_styles = new Array();
    this.char_styles = new Array();
    this.rsc_files = new Array();
    this.org_doc = org_doc;
    this.font_list = new Object;
    this.added_groups = new Array();
    this.added_items = new Array();
    this.option = "";
    this.suboption = "";
 //   this.doc_path = path;
//    this.buildObjectModel();
    }

REDDocument.prototype = new REDObject();
REDDocument.prototype.constructor = REDDocument;

REDDocument.prototype.buildObjectModel = function() {

    var i, icnt;
    var p_styles = this.org_doc.allParagraphStyles;
    icnt = p_styles.length;
    for(i=0;i<icnt;i++) {
        var p_style = p_styles[i];
        var red_p_style = new REDParaStyle(p_style);
        this.registerFont(p_style.appliedFont, p_style.fontStyle);
        this.para_styles.push(red_p_style);
    }

    var c_styles = this.org_doc.allCharacterStyles;
    icnt = c_styles.length;
    for(i=0;i<icnt;i++) {
        var c_style = c_styles[i];
        var red_c_style = new REDCharStyle(c_style);
        this.registerFont(c_style.appliedFont, c_style.fontStyle);
        this.char_styles.push(red_c_style);
    }
     
    var has_background = false;
    var layers = this.org_doc.layers;
    icnt = layers.length;
    for(i=0;i<icnt;i++) {
        var org_layer = layers[i];

        //Background Anotation시 처리 20190401 Kraken
        //if(org_layer.name == "Background") {
        if(org_layer.name.includes("Background")) {
            has_background = true;
        }        
        var red_layer = new REDLayer(org_layer);
        this.layers.push(red_layer);
    }
    
    if(has_background == false) {
        this.addBackgroundLayer();
    }
    
    var org_pages = this.org_doc.pages;
    var st = 0, ed = org_pages.length-1;
    if(this.start_page >= 0) {
        st = this.start_page;
    }
    
    if(this.end_page >= 0 && this.end_page < ed) {
        ed = this.end_page;
    }
    
    //Background Anotation시 처리 20190401 Kraken
    //this.org_doc.activeLayer = "Background";
    
    if(org_layer.name.includes("Background@")){
        this.org_doc.activeLayer = org_layer.name;
    }else{
        this.org_doc.activeLayer = "Background";
    }
    
    var pgno = 0;
    var y1 = - this.top_bleed;
    if(this.option == "P") { //  Photo Book 
        // Working PhotoBook
        var new_size = this.page_size;
        var y2 = this.page_size.height + this.bottom_bleed;
        this.page_size.width = new_size.width + new_size.width;
        var x1 = - this.out_or_right_bleed;
        var x2 = this.page_size.width + this.out_or_right_bleed;

        var spreads = this.org_doc.spreads;
        icnt = spreads.length;
        for(i=0;i<icnt;i++) {
            var spread = spreads[i];
            var bg_rectangle = spread.rectangles.add({geometricBounds:[y1, x1, y2, x2]});
            var swatch = app.activeDocument.swatches.itemByName("Paper");
            bg_rectangle.fillColor = swatch;
            bg_rectangle.strokeWeight = 0; 
            bg_rectangle.insertLabel("TYPE", "TEMP");
            bg_rectangle.sendToBack();
            this.added_items.push(bg_rectangle);
           
            pgno ++;
            var spread_name = "" + pgno;
            spread.name = spread_name;
            //마스터정보 추출을 위해 REDPage에 페이지번호 인자값 i를 추가 --20190619 Kraken
            var red_page = new REDPage(spread, pgno, this, i);
            this.pages.push(red_page);
        }
    }
    else if ( org_layer.name.includes("Background") && (backgroundType == 'png' || backgroundType == 'svg')) {
        for(i=st;i<=ed;i++) {
            var org_page = org_pages[i];
            var org_rect = REDRect.RectFromArray(org_page.bounds);      
            var org_page_size = org_rect.size;
            var x1 = 0;
            var x2 = 0;
            var y2 = org_page_size.height + this.bottom_bleed;
            switch(org_page.side ) {
                case PageSideOptions.RIGHT_HAND:
                case PageSideOptions.SINGLE_SIDED:
                    x1 = - this.in_or_left_bleed;
                    x2 = org_page_size.width + this.out_or_right_bleed;
                    break;
                case PageSideOptions.LEFT_HAND:
                    x1 = - this.out_or_right_bleed;
                    x2 = org_page_size.width + this.in_or_left_bleed;
                    break;
            }
            // background 확장자를 png, svg 선택한 경우 빈 사각형(투명) 추가하기 => 백그라운드 레이어가 없거나 레이어에 객체가 한개인 경우 백그라운드 파일이 안떨어져서 주문 시 오류 발생함.
            var bg_rectangle = org_page.rectangles.add({geometricBounds:[y1, x1, y2, x2]});
            var swatch = app.activeDocument.swatches.itemByName("None");
            bg_rectangle.fillColor = swatch;
            bg_rectangle.strokeWeight = 0;
            bg_rectangle.strokeColor = swatch;
            bg_rectangle.sendToBack();
            this.added_items.push(bg_rectangle);
            
            pgno ++;

            //마스터정보 추출을 위해 REDPage에 페이지번호 인자값 i를 추가 --20190619 Kraken
            var red_page = new REDPage(org_page,pgno, this, i);
            this.pages.push(red_page);
        }
    }
    else {
        for(i=st;i<=ed;i++) {
            var org_page = org_pages[i];
            var org_rect = REDRect.RectFromArray(org_page.bounds);      
            var org_page_size = org_rect.size;
            var x1 = 0;
            var x2 = 0;
            var y2 = org_page_size.height + this.bottom_bleed;
            switch(org_page.side ) {
                case PageSideOptions.RIGHT_HAND:
                case PageSideOptions.SINGLE_SIDED:
                    x1 = - this.in_or_left_bleed;
                    x2 = org_page_size.width + this.out_or_right_bleed;
                    break;
                case PageSideOptions.LEFT_HAND:
                    x1 = - this.out_or_right_bleed;
                    x2 = org_page_size.width + this.in_or_left_bleed;
                    break;
            }
            var bg_rectangle = org_page.rectangles.add({geometricBounds:[y1, x1, y2, x2]});
            var swatch = app.activeDocument.swatches.itemByName("Paper");
            bg_rectangle.fillColor = swatch;
            bg_rectangle.strokeWeight = 0;
            bg_rectangle.strokeColor = swatch;
            bg_rectangle.sendToBack();
            this.added_items.push(bg_rectangle);
            
            pgno ++;

            //마스터정보 추출을 위해 REDPage에 페이지번호 인자값 i를 추가 --20190619 Kraken
            var red_page = new REDPage(org_page,pgno, this, i);
            this.pages.push(red_page);
        }
     }
}

REDDocument.prototype.addBackgroundLayer = function()
{
    var bg_layer = this.org_doc.layers.add({name: "Background"});
    bg_layer.move(LocationOptions.AT_END);    
}

REDDocument.prototype.docJsonObj = function(custom_options) {

    var jsonobj = new Object;
    //jsonobj["Type"] = "Document";
    jsonobj["version"] = red_version;
    jsonobj["build"] = build_date;
    jsonobj["export_option"] = this.option + this.suboption;
    if(typeof this.token != "undefined" && this.token.length > 0) {
        jsonobj["token"] = this.token;
    }
    jsonobj["page_size"] = this.page_size.jsonObj();
    jsonobj["top_bleed"] = this.top_bleed;
    jsonobj["bottom_bleed"] = this.bottom_bleed;
    jsonobj["in_or_left_bleed"] = this.in_or_left_bleed;
    jsonobj["out_or_right_bleed"] = this.out_or_right_bleed;
    jsonobj["facing_pages"] = this.org_doc.documentPreferences.facingPages;
    jsonobj["number_of_pages"] = this.pages.length;
    jsonobj["cmyk_profile"] = this.org_doc.cmykProfile;
    jsonobj["rgb_profile"] = this.org_doc.rgbProfile;

    // 2018 02 06 [ 문서 컷라인 설정을 위한 bleed 값 추가 ]
    jsonobj["bleed_offset"] = new Object();
    var doc_preference = this.org_doc.documentPreferences;
    jsonobj["bleed_offset"].left   = doc_preference.documentBleedInsideOrLeftOffset;
    jsonobj["bleed_offset"].right  = doc_preference.documentBleedOutsideOrRightOffset;
    jsonobj["bleed_offset"].top    = doc_preference.documentBleedTopOffset;
    jsonobj["bleed_offset"].bottom = doc_preference.documentBleedBottomOffset;

    // 2018 10 01 custom_options을 설정한다. Irelander_kai
   if (custom_options){
        jsonobj["custom_options"] = custom_options;
    }

    var jsonlayers = new Array;
    var i, icnt  = this.layers.length;
    for(i=0;i<icnt;i++) {
        var layer = this.layers[i];
        var jsonlayer = layer.jsonObj();
        jsonlayers.push(jsonlayer);
    }
    jsonobj["layers"] = jsonlayers;
    
    jsonobj["paragraph_styles"] = this.para_styles;
    jsonobj["character_styles"] = this.char_styles;

    var jsonpages = new Array;
    icnt = this.pages.length;
    for(i=0;i<icnt;i++) {
        var page = this.pages[i];
        var jsonpage = page.jsonObj();
        jsonpages.push(jsonpage);
    }
    jsonobj["pages"] = jsonpages;
    jsonobj["font_list"] = this.font_list;
   var cover_info = getCoverInfo(this.org_doc);
   if(cover_info) {
        jsonobj["cover"] = cover_info; // Irelander_kai 20180907 | cover schema
   }
    return jsonobj;
//    var json_str  = JSON.stringify(jsonobj,null, "\t");
//    return json_str;
}

//########
REDDocument.prototype.load_page_items = function( container, page_item, sorted_items)
 {
  
    if(sorted_items == null || typeof sorted_items == "undefined"){
        var all_items = container.allPageItems;
        sorted_items =  new Array;
        var i=0, icnt = all_items.length;
        while(i<icnt) {
            var pitem = all_items[i];
            sorted_items.push(pitem);
            var length = pitem.allPageItems.length;
            i += length+1;
        }
        sorted_items = sorted_items.reverse();
    }
    
    //============================================================
    var i, icnt = sorted_items.length;
    var new_page_items = page_item.page_items;
    
    for(i=0;i<icnt;i++) {
        var org_pageitem = sorted_items[i];
        var org_class_constructor_name = org_pageitem.constructor.name;
        var org_class_str = org_pageitem.toString();
        var org_class_name = org_class_str.substring(8, org_class_str.length - 1);
        var red_class_name = "RED"+org_class_name;
        var pitem_class =  red_global[red_class_name];
        var red_pageitem_obj = null;
        
        if(pitem_class ==  undefined) {
            red_pageitem_obj = new REDPageItem(org_pageitem, this);
        }
        else {
            red_pageitem_obj = new pitem_class(org_pageitem, this); //red_global[red_class_name]();
        }
        
        red_pageitem_obj.parent = page_item;
        red_pageitem_obj.frame = red_pageitem_obj.relativeFrameFrom(page_item);
		if(red_pageitem_obj.frame.size.width == 0 || red_pageitem_obj.frame.size.height == 0 )
            continue;
        
        new_page_items.push(red_pageitem_obj);
    }
}

REDDocument.prototype.copyImageFiles = function(red_folder_path, org_doc_path)
{
    var file_list = this.rsc_files;
    var i, icnt = file_list.length;
    for(i=0;i<icnt;i++) {
        var src_path = file_list[i];
        var src_file = new File(src_path);
        if(!src_file.exists && org_doc_path != null) {
            var src_name = src_file.name;
            var doc_file = new File(org_doc_path);
            var doc_folder_path = doc_file.parent;
            var link_path = doc_folder_path + "/Links/";
            src_path = link_path + src_name;
            src_file = new File(src_path);
            }
        if(!src_file.exists) {
            // error 
            continue;
            }
        var tar_path = red_folder_path + "/" + src_file.name;
        var tar_file = new File(tar_path);
        if(src_file.copy(tar_file) == false) {
            // copy failed!!!
            continue;
            }
        }
    }

REDDocument.prototype.makeLayoutThumbnails = function()
{
    var iv_layers = new Array;
    var i, icnt = this.layers.length;
    for(i=0;i<icnt;i++) {
        var org_layer = this.layers[i].org_layer;
        if(org_layer.name != "PicFrame" && org_layer.visible == true) {
             org_layer.visible = false;
             iv_layers.push(org_layer);
            }
        }
        
    icnt = this.pages.length;
    for(i=0;i<icnt;i++) {
        var page = this.pages[i];
        var doc_file = new File(this.path);
        var red_folder = doc_file.parent;
        var red_folder_path = red_folder.fullName;
        var pg_idx = page.page_number - 1;
        if(page.org_page.constructor.name == "Spread") {
            var sub_pages = page.org_page.pages;
            var j, jcnt = sub_pages.length;
            for(j=0;j<jcnt;j++) {
                var left_right = "";
                var sub_page = sub_pages[j];
                if(sub_page.side == PageSideOptions.RIGHT_HAND) {
                    left_right = "r";
                }
                else if(sub_page.side == PageSideOptions.LEFT_HAND){
                    left_right = "l";
                }
                else {
                    left_right = "m";
                }
                var file_path = red_folder_path + "/layout-dp-"+pg_idx+"-"+left_right+".png";
                var dpi = dpi_for_thumb_size(this.page_size.width, this.page_size.height);
                page.savePageAsPNG(sub_page.name, file_path, dpi); 
            }
        }
        else {
            var file_path = red_folder_path + "/layout-dp-"+pg_idx+".png";             
            var dpi = dpi_for_thumb_size(page.rect.size.width, page.rect.size.height);
            page.savePageAsPNG(page.org_page.name, file_path, dpi); 
         }
    }

    icnt = iv_layers.length;
    for(i=0;i<icnt;i++) {
        var iv_layer = iv_layers[i];
        iv_layer.visible = true;
    }
}


function getStyleName(org_str)
{
    var word_list = org_str.split(" ");
    word_list.pop();
    var sname = word_list.join(" ");
    return sname;
}

function getFontFamilyName(font)
{
    if(font.constructor.name != "Font") {
        return "";
    }
    var fstyle = "";
    var ffullname = "";
    try {
        fstyle = font.fontStyleName;
        ffullname = font.name; //fullName;
    }
    catch (e) {
//        ffullname = font.name;
        var family = font.fontFamily;
        return family;
//        fstyle = ffullname.substring(family.length+1, ffullname.length);
        
    }
    
    var fstyle_len = fstyle.length;
    var ffullname_len = ffullname.length;
    var f_end = ffullname_len - fstyle_len;
    var f_tail = ffullname.substring(f_end, ffullname_len);
    if(f_tail == fstyle) {
        var ffamilyname = ffullname.substring(0, f_end);
        var endchar = ffamilyname[f_end-1] ;
        if(endchar == ' ' || endchar == '-' || endchar == '\t') {
            ffamilyname = ffullname.substring(0, f_end-1)
        }
        return ffamilyname; //.replace("-", "");
    }
    return ffullname;
}

function getFontFamilyNameNative(font)
{
    var fstyle = font.fontStyleNameNative;
    var fstyle_len = fstyle.length;
    var ffullname = font.fullNameNative;
    var ffullname_len = ffullname.length;
    var f_end = ffullname_len - fstyle_len;
    var f_tail = ffullname.substring(f_end, ffullname_len);
    if(f_tail == fstyle) {
        var ffamilyname = ffullname.substring(0, f_end);
        if(ffamilyname[f_end-1] == ' ') {
            ffamilyname = ffullname.substring(0, f_end-1)
        }
        return ffamilyname;
    }
    return ffullname;
}
REDDocument.prototype.registerFont = function(font, style_name)
{
    if(font.constructor.name != "Font") {
        return;
     }
//    var org_locale = $.locale;
//   $.locale = "en";
    try {
        var fontFamilyName = getFontFamilyName(font); //font.fullName; //getStyleName(font.fullName);
    }
    catch (ex) {
        return;
    }
    if(fontFamilyName == "" || fontFamilyName == null) {
        return;
    }
    if(fontFamilyName == "BM DoHyeon") {
        Dwriteln (fontFamilyName);
    }
    if(fontFamilyName.length == 0) {
        return;
    }
    var font_dict = this.font_list[fontFamilyName];
    if(font_dict == null) {
        font_dict = new Object;
        font_dict["family"] = new Object;
        font_dict["family"]["locale"] = new Object;
        font_dict["family"]["locale"]["en"] = fontFamilyName;
//       $.locale = "ko";
        try {
            font_dict["family"]["locale"]["kr"] = getFontFamilyNameNative(font);//font.fullNameNative; //getStyleName(font.fullNameNative);;
        }
        catch (e) {
            font_dict["family"]["locale"]["kr"] = fontFamilyName;
        }
        font_dict["style"] = new Object;        
        this.font_list[fontFamilyName] = font_dict;
    }
//    $.locale = "en";
    try {
        //var style_name = font.fontStyleName; //  이 라인이 없으면 exception 처리 필요 X.
        var style_dict = font_dict["style"][style_name];
        if(style_dict == null) {
            style_dict = new Object;
            style_dict["locale"] = new Object;
            style_dict["locale"]["en"] = style_name;
//            $locale = "ko";
            try {
                style_dict["locale"]["kr"] = font.fontStyleNameNative;
            }
            catch (e) {
                style_dict["locale"]["kr"] = style_name;
            }
            font_dict["style"][style_name] = style_dict;
        }
    }
    catch (ex)  {
    }
//    if($.locale != org_locale) {
//        $.locale = org_locale;
//    }
    
}

REDDocument.prototype.ungroup_added = function()
{
    var icnt = this.added_groups.length;
    var i;
    for(i=0;i<icnt;i++) {
        var agroup = this.added_groups[i];
        agroup.ungroup();
    }
}

REDDocument.prototype.remove_added = function()
{
    var icnt = this.added_items.length;
    var i;
    for(i=0;i<icnt;i++) {
        var anitem = this.added_items[i];
        anitem.remove();
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////  End of REDDocument

////////////////////////////////////////////////// //////////////////////////////////////////////////  Start Function

function saveRedFolder(red_path, option, meta_data_list, page_id_list, custom_options, features) // invoked from litsener
{   
    cur_doc = app.activeDocument;
    color_dict = new Object;

    var red_folder = new Folder(red_path);
    decimalPointLength   =  features.allowDecimalPoint //소수점 자리수 체크 Kraken
    whiteLayerVisible    =  features.whiteVisible //화이트레이어 visible 여부 Kraken
    _productCode         =  features.productCode.split('@')[1] // 특정상품 Variable Id를 임의로 부여하기위해 전역변수처리 Kraken
    variableType         =  features.variableType // 타입에따라 특정상품군 item에 variable Info를 세팅한다. Kraken
    isStickerMovable     =  meta_data_list[0].sticker_movable
    isStickerSelectable  =  meta_data_list[0].sticker_selectable
    isTextSelectable     =  meta_data_list[0].text_selectable
    isTextMovable        =  meta_data_list[0].text_movable
    isCellMovable        =  meta_data_list[0].cell_movable
    pngData = [];  // png로 만들어지는 파일들 정보 모아두는 배열
    backgroundType       =  meta_data_list[0].backgroundType   // background 파일 확장자

    //isStickerSelectable = true
    // Irelander_kai 수정 // 덮어쓰기 여부 물어보지 않고 자동으로 덮어쓴다.		
    if(red_folder.exists) {
        // if(!confirm("Overwrite ?")) {
        //     return;
        // }
        removeFolder(red_folder);
    }
    
    red_folder.create();
    
    Folder.current = red_folder.parent;
    app.scriptPreferences.userInteractionLevel = UserInteractionLevels.interactWithAll;
    var prev_hunit = cur_doc.viewPreferences.horizontalMeasurementUnits;
    var prev_vunit = cur_doc.viewPreferences.verticalMeasurementUnits;
    cur_doc.viewPreferences.horizontalMeasurementUnits = MeasurementUnits.MILLIMETERS;
    cur_doc.viewPreferences.verticalMeasurementUnits = MeasurementUnits.MILLIMETERS;
    cur_doc.pageItems.everyItem().locked = false;
    
//    try {
        saveTemplates(red_path, option, meta_data_list, page_id_list, custom_options);
    
//    }
//    catch  (err) {
//        alert(err.name+"\n"+err.message);
//    }

    cur_doc.viewPreferences.horizontalMeasurementUnits = prev_hunit;
    cur_doc.viewPreferences.verticalMeasurementUnits = prev_vunit;
    
    cur_doc = null;

    console.log(pngData);  // 변홤&미리보기 클릭 후 pngData 출력
    
}

function hasPageID(idx, list)
{
    if(list == null) {
        return true;
    }
    var i, icnt = list.length;
    for(i=0;i<icnt;i++) {
        if(list[i] == idx) {
            return true;
        }
    }
    return false;
}

function saveTemplates(red_path, option, meta_data_list, page_id_list, custom_options)
{   
    //작업
    var token_list = getTokenList(cur_doc);
    var token_cnt = token_list.length;
    var main_opt = option.charAt(0);
    if(main_opt == "S") {
        var i, icnt = cur_doc.pages.length;
        for(i=0;i<icnt;i++) {
            var page = cur_doc.pages[i];
            // if(i < token_cnt) {  -> getTokenList가 의미가 없다. 모든 페이지는 토큰을 가져야한다.
                // Irelander kai Modify... ( 해당 페이지에 설정된 토큰 값을 가져와 쓴다.)
                var token = getPageToken(cur_doc.pages.item(i));
            // }
            if(hasPageID(page.id, page_id_list)) {
                saveATemplate(red_path, i+1, i, i, meta_data_list[i], token, option, custom_options);
            }
        }
    }else if(main_opt == "D") {
        
        var i, icnt = (cur_doc.pages.length +1)>>> 1;
        var num = 1;
        for(i=0;i<icnt;i++) {
            var st = i * 2;
            var page = cur_doc.pages[st];

            
            if(i < token_cnt) {
                // Irelander kai Modify... ( 해당 페이지에 설정된 토큰 값을 가져와 쓴다.)      
                var token = getPageToken(cur_doc.pages.item(st)); // 홀수 페이지 인덱스를 가져와 세팅하도록 설정 ( 짝에 맞는 토큰으로 설정)
            }

            if(hasPageID(page.id, page_id_list)) {
                saveATemplate(red_path,num, st, st+1, meta_data_list[i], token, option, custom_options);
            }
            
            num++;
        }
    }else if(main_opt == "P") {  // Photo Book
//        if(cur_doc.documentPreferences.facingPages == false) {
 //           option = "Mr";
 //       }
        if(option.length <= 1) {
            option = "Pr";
        }
        if(token_cnt >= 1) { 
            var token = getPageToken(cur_doc.pages.item(0));
        }
        saveATemplate(red_path,1, -1, -1, meta_data_list[0], token, option, custom_options);
    }
    else { // main_opt == "M"
             if(token_cnt >= 1) { 
                var token = getPageToken(cur_doc.pages.item(0));
            }
        saveATemplate(red_path,1, -1, -1, meta_data_list[0], token, option, custom_options);
    }
}

function numgerOfTemplates(doc, option)
{
    if(option == "S" || option == "Sr" ) {
        var icnt = doc.pages.length;
        return icnt;
    }
    else if(option == "D" || option == "Dr") {
        var icnt = doc.pages.length >>> 1;
        return icnt;
    }
    else{
        return 1;
    }
    
}

function saveMetaObject(meta_obj, meta_path)
{
    var json_str  = JSON.stringify(meta_obj, null, "\t");
    var json_file = new File(meta_path);
    json_file.encoding = "UTF-8";
    json_file.open("w");
    json_file.write(json_str);
    json_file.close();
}

function saveMetadata(meta_obj, meta_path)
{
/*
{
    "template_type": "redp-indd-json",
    "partner_code": "redp",
    "ps_codes": ["150x300@MB"],
    "tags": [],
    "generate_layout": false,
    "cell_movable": false,
    "sticker_movable": false,
    "sticker_selectable": true
}
*/
    var meta_dict = new Object;
    meta_dict.template_type = "redp-indd-json";
    meta_dict.partner_code = "redp";
    meta_dict.ps_codes = "undefined";
    meta_dict.tags = [];
    meta_dict.generate_layout = false;
    meta_dict.default_values = {
        "div"  : meta_obj.userDiv,
        "lang" : meta_obj.userLang
    };
    
    // 프론트에서 사용자설정으로 값을 받을 수 있게 수정 20181203 -Kraken
    meta_dict.cell_movable = meta_obj.cell_movable;
    meta_dict.sticker_movable = meta_obj.sticker_movable;
    meta_dict.rsc_type = meta_obj.rsc_type;
    meta_dict.sticker_selectable = meta_obj.sticker_selectable;
    
    

    // 후가공 공정 정보 생성 Meta Data ( 메타 데이터에는 있지만 ) 
    meta_dict.post_layers = [ "Crease","Cut","DashRule","KissCut","Punch","Scodix","ScodixGold","ScodixSilver","White","Transparent","GoldGlossyFoil","GoldMatteFoil","SilverGlossyFoil","SilverMatteFoil","HologramFoil","BlackFoil", "Guide","BevelOutlineCut","BevelCut" ];
    
    if(typeof meta_obj.template_type != "undefined") {
        meta_dict.template_type = meta_obj.template_type;
    }
    if(typeof meta_obj.partner_code != "undefined") {
        meta_dict.partner_code = meta_obj.partner_code;
    }
    
    if(typeof meta_obj.tags != "undefined") {
        meta_dict.tags = meta_obj.tags;
    }
    if(typeof meta_obj.generate_layout != "undefined") {
        meta_dict.generate_layout = meta_obj.generate_layout;
    }

    if(typeof meta_obj.ps_codes != "undefined") {
        meta_dict.ps_codes = meta_obj.ps_codes;
        
        // if (productCode === "GSPNDFT" || meta_obj.ps_codes[0].split('@')[1] === "Pencil" || meta_obj.ps_codes[0].split('@')[1] === "BALLPEN" || meta_obj.ps_codes[0].split('@')[1] === "GSPNBAL" ) {
        if (variableType == "typeA") {
            meta_dict.text_focusible = false;
            meta_dict.sticker_unlockable = false;
        } 
    }

    if(typeof meta_obj.ps_codes != "undefined") {
        
    }
    // if(typeof meta_obj.cell_movable != "undefined") {
    //     meta_dict.cell_movable = meta_obj.cell_movable;
    // }
    // if(typeof meta_obj.sticker_movable != "undefined") {
    //     meta_dict.sticker_movable = meta_obj.sticker_movable;
    // }
    // if(typeof meta_obj.sticker_selectable != "undefined") {
    //     meta_dict.sticker_selectable = meta_obj.sticker_selectable;
    // }
    saveMetaObject(meta_dict, meta_path);
}
function saveATemplate(red_path, num, start, end, meta_data, token, option, custom_options)
{   
    var zero_num = zeroFilledStrFromNum(num);
    var temp_path = red_path+"/"+zero_num+".temp";
    var temp_folder = new Folder(temp_path);
    temp_folder.create();
    var json_path = temp_path+"/template.json";
    var red_doc = new REDDocument(cur_doc, json_path);
    red_doc.option = option.charAt(0);
    
    if(option.length > 1) {
        red_doc.suboption = option.charAt(1);
    }
    
    if(start >= 0 && end >= 0) {
        red_doc.start_page = start;
        red_doc.end_page = end;
    }
    
    red_doc.token = token;
    red_doc.buildObjectModel();
    var docJson = red_doc.docJsonObj(custom_options);
    var json_str  = JSON.stringify(docJson, null, "\t");
//      var json_str = red_doc.toJsonStr(custom_options);
    
    var json_file = new File(json_path);
    json_file.encoding = "UTF-8";
    json_file.open("w");
    json_file.write(json_str);
    json_file.close();
    var productCode = meta_data.ps_codes[0].split('@')[1]
    

    // 20180910 연동될 아이템의 리스트를 따로 관리하도록 추출 한다. Irelander_kai
    var variableItemList = new Array();
    // var docJson = JSON.parse(json_str);
    // variable items extract
    for(var i=0; i < docJson.pages.length; i++){
        var page = docJson.pages[i];
        
        for(var j=0; j < page.page_items.length; j++){
            var pageItem = page.page_items[j];
            if (pageItem.variable != null && typeof pageItem.variable != 'undefined' && variableType == ""){
                var itemDoc = {
                    name       : pageItem.name,
                    id         : pageItem.variable.id,
                    groupId    : pageItem.variable.group_id,
                    type       : pageItem.variable.type,
                    title      : pageItem.variable.title,
                    extra      : pageItem.variable.extra,
                    pageNumber : page.page_number,
                    layer      : pageItem.layer,
                    attribute  : {
                        selectable : pageItem.selectable,
                        movable    : pageItem.movable,
                        deletable  : pageItem.deletable,
                        focusible  : pageItem.focusible,
                        hidden     : pageItem.hidden
                    }
                };
                variableItemList.push(itemDoc);

            //특정상품 variable Item을 예외처리
            }else if(pageItem.variable != null && typeof pageItem.variable != 'undefined' && (variableType == "typeA" || variableType == "typeB")){

                     // text_obj.variable = {
                    //     id : item.name,
                    //     extra : item.extractLabel("FIELD")
                    // };
                    // text_obj.field = item.extractLabel("FIELD");
                    // text_obj.text = item.contents;
                    // text_obj.item_id = item.name;
                    // item_list.push(text_obj);
                
                var typeInfo = { text : {}, sticker : {} }
                if(variableType == "typeA"){
                    typeInfo.text.id            = 'commonText'
                    typeInfo.text.selectable    = true
                    typeInfo.text.movable       = false
                    typeInfo.text.deletable     = false
                    typeInfo.text.focusible     = pageItem.focusible
                    typeInfo.text.hidden        = pageItem.hidden
                    typeInfo.sticker.id         = 'commonSticker'
                    typeInfo.sticker.selectable = true
                    typeInfo.sticker.movable    = false
                    typeInfo.sticker.deletable  = false
                    typeInfo.sticker.focusible  = pageItem.focusible
                    typeInfo.sticker.hidden     = pageItem.hidden


                }else if(variableType == "typeB"){
                    typeInfo.text.id            = pageItem.variable.id
                    typeInfo.text.selectable    = false
                    typeInfo.text.movable       = false
                    typeInfo.text.deletable     = false
                    typeInfo.text.focusible     = pageItem.focusible
                    typeInfo.text.hidden        = pageItem.hidden
                    typeInfo.sticker.id         = pageItem.variable.id
                    typeInfo.sticker.selectable = true
                    typeInfo.sticker.movable    = false
                    typeInfo.sticker.deletable  = false
                    typeInfo.sticker.focusible  = pageItem.focusible
                    typeInfo.sticker.hidden     = pageItem.hidden
                }

                if (pageItem.name == "Text"){
                    var itemDoc = {
                        name       : pageItem.name,
                        id         : typeInfo.text.id,
                        groupId    : pageItem.variable.group_id,
                        type       : pageItem.variable.type,
                        title      : pageItem.variable.title,
                        extra      : pageItem.variable.extra,
                        pageNumber : page.page_number,
                        layer      : pageItem.layer,
                        attribute  : {
                            selectable : typeInfo.text.selectable,
                            movable    : typeInfo.text.movable,
                            deletable  : typeInfo.text.deletable,
                            focusible  : typeInfo.text.focusible,
                            hidden     : typeInfo.text.hidden
                        }
                    }
                    variableItemList.push(itemDoc);

                }else if (pageItem.layer == "Sticker"){
                    var itemDoc = {
                        name       : pageItem.name,
                        id         : typeInfo.sticker.id,
                        groupId    : pageItem.variable.group_id,
                        type       : pageItem.variable.type,
                        title      : pageItem.variable.title,
                        extra      : pageItem.variable.extra,
                        pageNumber : page.page_number,
                        layer      : pageItem.layer,
                        attribute  : {
                            selectable : typeInfo.sticker.selectable,
                            movable    : typeInfo.sticker.movable,
                            deletable  : typeInfo.sticker.deletable,
                            focusible  : typeInfo.sticker.focusible,
                            hidden     : typeInfo.sticker.hidden
                        }
                    }
                    variableItemList.push(itemDoc);

                }
            }
        }
    }

    if (variableItemList.length > 0){
        var variableItemPath = temp_path+"/variableItemList.json";
        var variable_str = JSON.stringify(variableItemList, null, "\t");
        var varialbe_json_file = new File(variableItemPath);
        varialbe_json_file.encoding = "UTF-8";
        varialbe_json_file.open("w");
        varialbe_json_file.write(variable_str);
        varialbe_json_file.close();
    }
    
    var meta_path = temp_path + "/metadata.json";
    
    saveMetadata(meta_data, meta_path);
    var org_doc_path = app.activeDocument.fullName; // File    
    //red_doc.copyImageFiles(temp_path, org_doc_path);
    //red_doc.makeLayoutThumbnails();
    red_doc.ungroup_added();    
    red_doc.remove_added();
    
}

function setTokenList(doc, list)
{
    var token_str = list.join(",");
    doc.insertLabel("RED_Teamplate_Token", token_str);
}

function getTokenList(doc)
{   
    
    var token_str = doc.extractLabel("RED_Teamplate_Token");
    if(typeof token_str != "undefined" && token_str.length > 0) {
        var token_list = token_str.split(",");
        return token_list;
    }
    return null;
}

function getTextListFromPageItems(page_items, text_list)
{   
    
    var i, icnt = page_items.length;
    for(i=0;i<icnt;i++) {
        var item = page_items[i];
        var org_class_constructor_name = item.constructor.name;
        var org_class_str = item.toString();
        var org_class_name = org_class_str.substring(8, org_class_str.length - 1);

        if(org_class_name == "TextFrame") {
            var text_obj = new Object;
//            text_obj.field = item.label;
            if (item.extractLabel("FIELD")){
                text_obj.variable = {
                    id : makeId(),
                    extra : item.extractLabel("FIELD")
                };
            }
            text_obj.field = item.extractLabel("FIELD");
            text_obj.text = item.contents;
            text_obj.item_id = item.id;
            text_list.push(text_obj);
        }
        if(typeof item.pageItems != "undefined") {
            getTextListFromPageItems(item.pageItems, text_list);
        }
    }
}

function getTextList(page)
{
    var items  = page.pageItems;
    var text_list = new Array;
    getTextListFromPageItems(items, text_list);
    return text_list;
}

//각 레이어의 아이템들을 불러온다. Kraken
function getItemList(page, optionType)
{
    var items  = page.pageItems;  // ex) {"length":8} 저장됨.  현재 페이지 내에 아이템들 갯수.
    var item_list = new Array;

    getItemListFromPageItems(items, item_list, optionType);
    
    return item_list;  // getItemListFromPageItems함수에서 각각의 아이템에 대해 만든 object(variable, field, text, item_id) 배열을 리턴
}



//지정된layer의 item에 variable Id 부여 Kraken
function getItemListFromPageItems(page_items, item_list, optionType)
{
    var i, icnt = page_items.length;   

    for(i=0;i<icnt;i++) {
        var item = page_items[i];
        var org_class_str = item.toString();  // [object PageItem]
        var org_class_name = org_class_str.substring(8, org_class_str.length - 1);
        
        if(optionType == "typeA"){
            if(item.itemLayer.name == "Text") {
                var text_obj = new Object;
    
                    text_obj.variable = {
                        id : 'commonText',
                        extra : item.extractLabel("FIELD")
                    };
                text_obj.field = 'commonText';
                text_obj.text = item.contents;
                text_obj.item_id = item.id;
                item_list.push(text_obj);

            }else if(item.itemLayer.name == "Sticker") {
                var sticker_obj = new Object;
                sticker_obj.variable = {
                    id : 'commonSticker',
                    extra : item.extractLabel("FIELD")
                };
                sticker_obj.field = "commonSticker";
                //text_obj.text = item.contents;
                sticker_obj.item_id = item.id;
                item_list.push(sticker_obj);
            }

        }else if(optionType == "typeB"){
            if(item.itemLayer.name == "Text") {
                var text_obj = new Object;
                    text_obj.variable = {
                        id : item.name,
                        extra : item.extractLabel("FIELD")
                    };
                text_obj.field = item.extractLabel("FIELD");
                text_obj.text = item.contents;
                text_obj.item_id = item.name;
                item_list.push(text_obj);

            }
        }


        if(typeof item.pageItems != "undefined") {
            getItemListFromPageItems(item.pageItems, item_list, optionType);
        }
    }
}


function pageItemByID(page_items, item_id)
{
    var ret_item = page_items.itemByID(item_id);  // itemByID : item_id로 page 찾아옴
    if(typeof ret_item == "undefined"  || ret_item == null  ) {  // 일치하는 페이지가 없으면
        var i, icnt = page_items.length;
        for(i=0;i<icnt;i++) {
            var item = page_items[i];
            if(typeof item.pageItems != "undefined" && item.pageItems != null && item.pageItems.length > 0) {
                ret_item = pageItemsByID(item.pageItems, item_id);
                if(typeof ret_item == "undefined" || ret_item == null  ) {
                    break;
                }
            }
        }
    }
    return ret_item;
}


function setTextList(page, text_list)
{
    var i, icnt = text_list.length;
    for(i=0;i<icnt;i++) {
        var obj = text_list[i];
        var page_item = pageItemByID(page.pageItems, obj.item_id);
        if( typeof page_item != "undefined" && page_item != null) {
            page_item.insertLabel("FIELD", obj.field);
            page_item.contents = obj.text;
        }
    }
}


function setAppointItemList(page, item_list, type, optionType)
{
    var i, icnt = item_list.length;

    for(i=0;i<icnt;i++) {  // 배열 길이만큼 반복해서 object 하나씩 처리
        var obj = item_list[i];  // i번째 객체(obj) 
        var page_item = pageItemByID(page.pageItems, obj.item_id);
        if(optionType == "typeA"){
            if (type == "Text"){
                if( typeof page_item != "undefined" && page_item != null) {
                    page_item.insertLabel("FIELD", "commonText");
                    page_item.contents = obj.text;
                }
            }else if (type == "Sticker"){
                page_item.insertLabel("FIELD", "commonSticker");
            }
        }else if(optionType == "typeB"){
            if (type == "Text"){
                if( typeof page_item != "undefined" && page_item != null) {
                    page_item.insertLabel("FIELD", obj.field);
                    page_item.contents = obj.text;
                }
            }else if (type == "Sticker"){
                page_item.insertLabel("FIELD", obj.field);
            }
        }    
    }
}



function setPageLabel(page, key, label) 
{
    page.insertLabel(key, label);
}

function getPageLabel(page, key)
{
    return page.extractLabel(key);
}

function setPageToken(page, token)
{
    setPageLabel(page,  "TOKEN", token);
}

// Irelander_kai Modifiy..
function getPageToken(page)
{   
    var token = page.extractLabel("TOKEN");
    return token;
}

function TestMenuListener(){
    var page = app.activeWindow.activePage; // Current Page
    var text_list = getTextList(page); // page 에서 텍스트 리스트를 가져옴
    var text_str = "";
    var i, icnt = text_list.length;
    for(i=0;i<icnt;i++) {
        var textobj = text_list[i];
        text_str += "field:'" + textobj.field + "'";
        text_str += " => text:'"+textobj.text+"'";
        text_str += "\n";
    }

    alert(text_str,"Results");
    //text_list 에는 원래 TextFrame 아이템의 정보를 가지고 있으므로 그대로 유지하여야 한다.

    for(i=0;i<icnt;i++) {
        var textobj = text_list[i];
        // 텍스트는 있지만 필드명이 없는 아이템에 필드명을 지정한다.
        if(textobj.text.length > 0 && textobj.field.length == 0) {
            textobj.field = "FIELD_" + i;
        }
    }
    setTextList(page, text_list);  // page에 텍스트 리스트를 적용한다.
}

function RenderBound(layer_name)
{
	var doc = app.activeDocument;
	
	// RenderBound용 컬러를 만듦 
    //var color_group_swatch = null;
    var layer_color = doc.colors.item("render_bound");
    if(layer_color == null) {
        //var color_group = doc.colorGroups.add({name:"CG"});  
   
       layer_color = doc.colors.add({  
        model: ColorModel.PROCESS,  
        space: ColorSpace.CMYK,  
        colorValue: [0,100,100,0],  
        name: "render_bound"}  
       );  
       //color_group_swatch = color_group.colorGroupSwatches.add(layer_color);  
   }
   else {
        //color_group_swatch = color_group.colorGroupSwatches
   }

/*	
	var layer_color_swatch = color_group.colorGroupSwatches.add(ref_swatch);  
  
	$.writeln([  
	  color_group_swatch.swatchItemRef.colorValue,  
	  color_group_swatch.swatchItemRef.name  
	]);  
*/
	// 기존에 RenderBound 레이어가 있는지 체크 
	var render_bound_layer = null;
    var layers = doc.layers;
    var i, icnt = layers.length;
    for(i=0;i<icnt;i++) {
        var layer = layers[i];
        if(layer.name == "RenderBound") {
            render_bound_layer = layer;
			break;
        }        
    }
	if(render_bound_layer == null) {
		// RenderBound 레이어가 없으면 추가
		render_bound_layer = doc.layers.add ({name: 'RenderBound'}); 		
        render_bound_layer.layerColor = new Array(255, 0, 0);
	}
	doc.activeLayer = "RenderBound";
	// 모든 페이지에 RenderBound 객체를 만듦
	icnt = doc.pages.length;
	for(i=0;i<icnt;i++) {
		var page = doc.pages[i];
		MakeRenderBound(doc, page, layer_color, render_bound_layer, layer_name);
	}
	
}

function MakeRenderBound(doc, page, color, render_bound_layer, layer_name)
{	
    var has_target = false;
	var i, icnt = page.pageItems.length;
	var y1 = 999999, x1 = 999999, y2 = -999999, x2 = -999999;
	for(i=0;i<icnt;i++) {
		var page_item = page.pageItems[i];
		var layer = page_item.itemLayer;
		if(layer.name != layer_name) {
            continue;
		}
         has_target = true;
		var bounds = page_item.visibleBounds;
		if(bounds[0] < y1)
			y1 = bounds[0];
		if(bounds[1] < x1)
			x1 = bounds[1];
		if(bounds[2] > y2)
			y2 = bounds[2];
		if(bounds[3] > x2)
			x2 = bounds[3];
	}
    if(has_target ==  false) {
        return;
    }
    
	// 먼저 기존에 RenderBound 객체가 있는지 확인 
	var render_bound_item = null;
	for(i=0;i<icnt;i++) {
		var page_item = page.pageItems[i];
		var layer = page_item.itemLayer;
		if(layer.name == "RenderBound") {
			render_bound_item = page_item;
			break;
		}
    }

	// 기존에 RenderBound 객체가 없으면 만들기
	if(render_bound_item == null) {
		render_bound_item = page.rectangles.add({layer:render_bound_layer ,geometricBounds:[y1, x1, y2, x2]});;
		//render_bound_item.itemLayer = render_bound_layer;
        render_bound_item.strokeColor = color;
        render_bound_item.fillColor = "None";
        render_bound_item.strokeWeight = 1;
	}
    else {
        var new_bounds = new Array(y1, x1, y2, x2);
        render_bound_item.geometricBounds = new_bounds;
   }
    
        //var swatch = app.activeDocument.swatches.itemByName("RenderBounds");
}


//  Photo Book Cover Metadata
//////////////////////////////////////////////////////////////////////////////////////////

function setCoverInfoStr(doc, info_str)
{
    doc.insertLabel("RED_Cover_Info", info_str);
}

function setCoverInfo(doc, key, value)
{
    var cover_info =  getCoverInfo(doc);
    if(cover_info == null) {
        cover_info = new Object;
    }
    cover_info[key] = value;
    var cover_info_str = JSON.stringify(cover_info, null, "\t");
    setCoverInfoStr(doc, cover_info_str);
}

function getCoverInfo(doc)
{
    var info_str = doc.extractLabel("RED_Cover_Info");
    var info_json = null;
    try {
        info_json = JSON.parse(info_str);
    }
    catch (e) {
        doc.insertLabel("RED_Cover_Info", "");
        info_json = null;
    }
    return info_json;
}
