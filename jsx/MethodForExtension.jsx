// export 
function exportToData(productInfo){

    if(app.documents.length == 0) {
      alert("No document opned!");
      return
    }
    var doc_path = null;
    var doc_name = "";
    if(app.activeDocument.saved) {  // 저장되어있는 상태이면
        doc_path = app.activeDocument.fullName; // File  // The full path to the Document, including the name of the Document.
        doc_name = remove_ext(doc_path.name);  // 현재 인디자인 파일 이름
        var doc_file = new File(doc_name);
        Folder.current = doc_file.parent;
    }
    else {
        alert("문서를 먼저 저장해 주시기 바랍니다.");
        return;
    }

    
    var savePath = app.activeDocument.extractLabel('savePath');
    var isOverwrite = true;
    var red_file;
  
    if (savePath !== ""){  // 사전에 저장된 경로가 있으면
      red_file = JSON.parse(savePath);  // red_file = {"fullName": "인디자인 파일 경로", "name": "파일 이름"}
      isOverwrite = confirm("기존에 저장된 경로 ["+red_file.fullName+"] 에 저장하시겠습니까? 저장 후 도큐먼트 수정시 다시 변환을 실행해야합니다. ");
      if(!isOverwrite){  // 기존에 저장된 경로에 저장하지 않는다고 누르면 (cancel 클릭시)
          red_file = doc_file.saveDlg("기본 드라이브에만 저장 할 수 있습니다.");  // 파일 저장 위치 설정하는 dialog 띄워서 위치 선택하면 {"fullName": "인디자인 파일 경로", "name": "파일 이름"} 이렇게 저장됨
      }else{
      }
    }else {  // 사전에 저장된 경로가 없으면
      red_file = doc_file.saveDlg("기본 드라이브에만 저장 할 수 있습니다.");      
    }
    
    var absolutePath = "";
    if(red_file) { 
        var red_path = red_file.fullName;  // 인디자인 파일 경로
  
        // 파일 저장 경로 저장...
        var fileInfo = new Object();
        fileInfo.fullName = red_file.fullName;  // 인디자인 파일 경로
        fileInfo.name = red_file.name;  // 인디자인 파일 이름
        app.activeDocument.insertLabel('savePath', JSON.stringify(fileInfo));  // savePath 라벨에 fileInfo 저장 {fullName : 인디자인 파일 경로, name : 파일 이름}
  
        absolutePath = File(red_path).parent.fsName.toString().replace(/\\/g, '/')+"/"+red_file.name;  // String으로 인디자인파일의 경로 저장 ex) "/Users/yujin/Desktop/test/8SyLM88hXAdgATtpW%20(1)"
        

        var temp_cnt = numgerOfTemplates(app.activeDocument, productInfo.uploadType);  // 해당 옵션에 대한 템플릿 수를 가져온다.  // uploadType에 따라 템플릿 페이지 수를 계산하는 방법이 달라짐. 템플릿 페이지 갯수 리턴받아서 저장함
        
        // 토큰관련 부분 ( 해당 토큰 설정 부분은 사용하지 않는다. Page 토큰을 사용...)
        //var token_list = getTokenList(app.activeDocument);// 문서에 이미 저장된 토큰들을 가져옴
        var token_list = null
        var i;
        if(token_list == null) { // 문서에 이미 저장된 토큰들이 없다면
            token_list = new Array;
            for(i=0;i<temp_cnt;i++) {  // 템플릿 페이지 갯수만큼 반복
                var token = "token-"+i; // 토큰값을 서버에서 받아옴. 
                token_list.push(token);  // token_list에 페이지마다 토큰 저장해서 push함
            }
            setTokenList(app.activeDocument, token_list); // 문서에 토큰 값들을 저장함
        }
        
        var meta_data_list = new Array; // 메타데이터를 담을 배열을 만든다.
        
        // 템플릿 수 만큼 메타데이터를 만든다.
        for(i=0;i<temp_cnt;i++) {
            meta_data_list.push(productInfo.metaData);  // 메타데이터를 배열에 담는다.  // metaData : ps_codes, cell_movable, sitcker_movable, sticker_selectaqble, text_movable, text_selectable, rsc_type, userDiv
        }
        
        // productInfo : uploadType, modifiedPages, features, metaData
        saveRedFolder(red_path, productInfo.uploadType, meta_data_list, productInfo.modifiedPages, productInfo.customOptions, productInfo.features);  // 메타데이터를 담은 배열을 파라미터로 넘긴다.
        
    }
    
    return decodeURI(absolutePath);  // 인디자인파일경로 리턴
  }
  
  function hasToken(){
      return getTokenList(app.activeDocument);  // 템플릿 페이지마다 저장한 토큰들 list 리턴
  }
  
  // message로 받은 문자열을 alert창으로 띄워주는 함수
  function sendMessageToEndUser(message){
      alert(message);
      return null;
  }
  
  function getCurrentPageText(){
      var page = app.activeWindow.activePage; // Current Page (맨 앞에서 보고 있는 페이지)
      var text_list = getTextList(page); // page 에서 텍스트 리스트를 가져옴

      return JSON.stringify(text_list);
  }

  // typeA 또는 typeB인 경우 실행되는 함수.
  function getAllPageItems(optionType){  // optionType : typeA or typeB

      var doc = app.activeDocument;
      var pageLength = doc.pages.count();  // 템플릿 페이지 갯수

      for(var i=0; i < pageLength; i++){  // 페이지 갯수만큼 반복
        var page = doc.pages.item(i);  
        var text_list = getItemList(page, optionType); // getItemList에서 리턴받은 object 배열 저장됨 (text만 있는거 아님)

        setAppointItemList(page, text_list, 'Text', optionType);
      }
      
      for(var i=0; i < pageLength; i++){
        var page = doc.pages.item(i);        
        var sticker_list = getItemList(page); // page 에서 텍스트 리스트를 가져옴
        setAppointItemList(page, sticker_list, 'Sticker', optionType);
      }

      // setAppointItemList로 page_item의 field라벨값과 contents 설정함
      return true
  }
  
  function setCurrentPageText(texts){  // texts = 현재 페이지에 있는 텍스트들 모아둔 배열
      var page = app.activeWindow.activePage; // Current Page
      setTextList(page, texts);  // 페이지 아이템 field라벫값과 contents 부분 설정함
  }
  
  function getCurrentPageId(){  // 현재 페이지 아이디 알아내는 함수
      return JSON.stringify(app.activeWindow.activePage.id);
  }
  
  function checkHasToken(){
      var doc = app.activeDocument.pages;  // {"length" : ~}
      var result = new Array;
      var tokenResult = new Array;
      var duplicateCheck = new Array;
      var pageLength = doc.count();  // 페이지 수

      for(var i=0; i < pageLength; i++){
          var page = doc.item(i);  // 각각의 페이지 
          var token = getPageToken(page);  // 페이지 별 토큰값
          // 토큰이 있으면
          if(token){
            tokenResult.push(token)  // tokenResult 배열에 push
            duplicateCheck.push(token)  // duplicateCheck 배열에 push
          }
          // 토큰이 없는 경우 페이지 아이템 번호를 가져온다.
          if( token == ""){
              result.push(page.id);
          }
      }
      
      if(tokenResult.length > 1){  // 위에서 토큰이 있었던 경우만 이쪽으로 들어옴
        if (checkDuplicateToken(tokenResult,duplicateCheck) === false) return false  // false인 경우는 중복된 토큰값이 존재하는 경우
      }

      return JSON.stringify(result);  // 토큰이 없었으면 result에 page.id가 담겨서 리턴됨
  }
  
  // component.js test함수에서 쓰이는 중
  function checkToken(){  

    var doc = app.activeDocument.pages;
      var result = new Array;
      var tokenResult = new Array;
      
      var pageLength = doc.count();
    
      for(var i=0; i < pageLength; i++){
          var page = doc.item(i);
          var token = getPageToken(page);
            tokenResult.push(token)
      }

      return JSON.stringify(tokenResult);  // 페이지 별 토큰값 리턴
  }


  // 토큰 초기화 하기 전에 가지고 있던 토큰들 확인할 때 쓰는거 같음
  function allTokenInfo(){

    var doc = app.activeDocument.pages;
      //var result = new Array;
      var pageLength = doc.count();
      var result = {}
      var test = []
    
      for(var i=0; i < pageLength; i++){
          var page = doc.item(i);
          var token = getPageToken(page);
          result[i] = token;
      }
      
      return JSON.stringify(result);  // 초기화 전 가지고 있던 토큰 배열 리턴
  }


  //중복토큰체크  // Boolean값 리턴
  function checkDuplicateToken(tokenResult, duplicateCheck){  // tokenResult(토큰 배열), duplicateCheck(중복 확인하기 위한 배열) => 페이지 수만큼 토큰 들어있음
    
      var k=-1;
      var j=duplicateCheck.length;
      while(++k<j-1) {
          if(duplicateCheck[k]==duplicateCheck[k+1]){  // 페이지 토큰값이 똑같은 경우
              duplicateCheck[k]='';  // 값을 없애서 duplicateCheck의 길이가 짧아짐
          }
      }
      duplicateCheck=duplicateCheck.toString().match(/[^,]+/g);  // match : 문자열이 정규식과 일치하는 것들을 모아서 배열로 리턴함
    
      if(duplicateCheck.length !== tokenResult.length){  // 위에서 토큰값이 같으면 값을 없애기 때문에 중복된 토큰이 있으면 duplicateCheck길이와 tokenResult 길이가 다름
          alert("페이지에 중복된 토큰이 존재합니다. 페이지를 다시 한 번 확인해주시기바랍니다.");
          return false;
      }else{
          return true;   // 중복되는 토큰이 없으면 true 리턴
      }
  }

  // 모든 토큰 초기화하는 함수
  function resetAllPageTokens(){
      var doc = app.activeDocument.pages;
      var pageLength = doc.count();
      for(var i=0; i < pageLength; i++){
          var page = doc.item(i);
          var token = setPageToken(page, "");  // 페이지마다 "Token" 라벨에 "" 값 넣어줌
      }
  }
  
  // 선택한 페이지 복제하는 함수
  function currentPageDuplicate(){
      app.activeWindow.activePage.duplicate();  // active되어있는 페이지 복제함
      var pages = app.activeDocument.pages;
      var newPage = pages[pages.length-1];  // 복제한 후의 length이기 떄문에 한개 늘어나있음 (4개였을 때 복제버튼 누르면 이 시점에서 pages.length는 4가 아니라 5) 
      // 현재 템플릿 페이지의 맨 마지막에 새로운 페이지 만들어지기 때문에 새로운 페이지를 사용하려면(지정하려면?) pages[index] 이런식으로 쓰는거 같음
      setPageToken(newPage, "");  // 새로운 페이지의 "Token" 라벨값을 빈값으로 세팅
      return null
  }

  // 레이어 생성하는 함수
  function createLayerPage(layerName){  
        app.activeDocument.layers.add({name: layerName});  // 입력한 레이어명을 받아와서 레이어 추가함
    return null
  }
  
function splitPageToDocument(path, docInfo, uuids){
    //   var pages = app.activeDocument.pages;
    var spreads = app.activeDocument.spreads;
    var folderPath = path+"/splitTemplate";
    var red_folder = new Folder(folderPath);  // 폴더 객체 생성. 객체의 경로를 folderPath로 설정
    var templateInfo = app.activeDocument.extractLabel("templateInfo");
    red_folder.create();  // folderPath위치에 폴더 생성함

    //Get document width,height
    var doc = app.activeDocument,  
    docPages = doc.pages,  
    docPage, docWidth, docHeight;  
    
    for (var i = 0; i < docPages.length; i++) {
        docPage   = docPages[i];  
        docWidth  = docPage.bounds[3] - docPage.bounds[1];  // bounds => [y1, x1, y2, x2] 이렇게 저장되어있음.  x2 - x1
        docheight = docPage.bounds[2] - docPage.bounds[0];  // y2 - y1
    }
      
    try{
        if (docInfo.type == "S"){  // 싱글 템플릿이면
            for(var i=0; i< spreads.length; i++) {
                with(app.marginPreferences) {  // margin 설정
                    top     = 0+"mm"
                    left    = 0+"mm"
                    right   = 0+"mm"
                    bottom  = 0+"mm"
                }

                var newDocument = app.documents.add();
                with(newDocument.documentPreferences) {  // 도큐먼트 설정하기
                //   pageHeight  = docInfo.pageHeight+"mm";
                //   pageWidth   = docInfo.pageWidth+"mm";
                    pageHeight  = docheight+"mm";
                    pageWidth   = docWidth+"mm";
                    facingPages = false;  // facingPages => true이면 facing page를 가지고 있다는 의미. facing page = 맞은편 페이지?(책을 예로 들면 74p(왼쪽), 75p(오른쪽)이 있을 때 74p의 facing page는 75p)
                    documentBleedUniformSize = false;  // documentBleedUniformSize : 기본값은 true. true면 bleed top offset을 모든 면에 적용함. false로 설정해야 각각 다르게 설정할 수 있음
                    documentBleedTopOffset = docInfo.marginHeight;
                    documentBleedBottomOffset = docInfo.marginHeight;
                    documentBleedOutsideOrRightOffset = docInfo.marginWidth;
                    documentBleedInsideOrLeftOffset = docInfo.marginWidth;
                }
                  
                var spread = spreads[i];
                //   page.duplicate(LocationOptions.AT_END, newDocument.pages.item(0));  // duplicate(복제할 위치, 참조 객체(복제할 페이지))  LocationOptions.AT_END : 맨 끝에 배치함
                spread.duplicate(LocationOptions.AT_END, newDocument);  // duplicate(복제할 위치, 참조 객체(복제할 페이지))  LocationOptions.AT_END : 맨 끝에 배치함
                newDocument.pages.item(0).remove();
                newDocument.marginPreferences.properties = {top : 0,left: 0,right: 0,bottom:0};  // properties : 한번에 여러 속성 정의. 
                var file = new File(folderPath+"/"+uuids[i]+".indd");
                newDocument.layers[newDocument.layers.length-1].remove();
                newDocument.insertLabel('templateInfo', templateInfo);
                newDocument.insertLabel("isSplitPage", "Y");
                newDocument.insertLabel("isProduction", "Y");
                newDocument.close(SaveOptions.YES , file);  // close(문서 닫기전 저장할지 여부(default: SaveOptions.ASK), 문서를 저장할 파일 )  SaveOptions.YES : Saves changes. 닫을 때 무조건 저장되고 닫힘.
            }
        } else if (docInfo.type == "D") {  // 페어 템플릿이면
            //Get document width,height
            var doc = app.activeDocument,  
            docPages = doc.pages,  
            docPage, docWidth, docHeight;  
            
            for (var i = 0; i < docPages.length; i++) {  
                docPage   = docPages[i];  
                docWidth  = docPage.bounds[3] - docPage.bounds[1];  
                docHeight = docPage.bounds[2] - docPage.bounds[0];       
            }

            with(app.marginPreferences){
                top     = 0+"mm"
                left    = 0+"mm"
                right   = 0+"mm"
                bottom  = 0+"mm"
            }


            for(var i=0; i< spreads.length; i+=2) {
                var newDocument = app.documents.add();
                with(newDocument.documentPreferences) {
                //   pageHeight = docInfo.pageHeight+"mm";
                //   pageWidth = docInfo.pageWidth+"mm";
                    pageHeight = docHeight
                    pageWidth  = docWidth
                    facingPages = false;
                    
                    documentBleedUniformSize = false;
                    documentBleedTopOffset = docInfo.marginHeight;
                    documentBleedBottomOffset = docInfo.marginHeight;
                    documentBleedOutsideOrRightOffset = docInfo.marginWidth;
                    documentBleedInsideOrLeftOffset = docInfo.marginWidth;
                }
                var frontPage = spreads[i];
                frontPage.duplicate(LocationOptions.AT_END, newDocument.spreads.item(0));
                var backPage = spreads[i+1];
                backPage.duplicate(LocationOptions.AT_END, newDocument.spreads.item(1));
                newDocument.spreads.item(0).remove();
                newDocument.marginPreferences.properties = {top : 0,left: 0,right: 0,bottom:0};
                var file = new File(folderPath+"/"+uuids[i/2]+".indd");
                newDocument.layers[newDocument.layers.length-1].remove();
                newDocument.insertLabel("isSplitPage", "Y");
                newDocument.insertLabel("isProduction", "Y");
                newDocument.insertLabel('templateInfo', templateInfo);
                newDocument.close(SaveOptions.YES , file);
            }
        } else if (docInfo.type == "M") {  // 멀티 템플릿
            var filePath = File(folderPath+"/"+uuids[0]+".indd");
            var currentDoc = app.activeDocument;
            currentDoc.insertLabel("isSplitPage", "Y");
            currentDoc.saveACopy(filePath);  // saveACopy : 사본 저장. filePath에 저장함
            currentDoc.insertLabel("isSplitPage", "");
        }
      } catch(e) {
          alert(e);
      }
  }
  
  function loadJSX(){
      var selectFile = File.openDialog();  // 파일을 열 수 있는 창 띄워서 선택한 파일 객체가 selectFile에 저장됨. 여러개 선택한 경우 파일 객체 배열이 저장됨
      var absolutePath = File(selectFile.fullName).parent.fsName.toString().replace(/\\/g, '/')+"/"+selectFile.name;  // 선택한 파일의 경로 ex)"/Users/yujin/Desktop/test/test.jsx"

      return absolutePath;
  }
  
  function createNewDocument(options){
      var newDocument = app.documents.add();  // 도큐먼트 생성
      with(newDocument.documentPreferences){  
          pageHeight = options.pageHeight+"mm";
          pageWidth = options.pageWidth+"mm";
          facingPages = false;
          
          // bleed 값 설정
          if (options.marginWidth === options.marginHeight){
              documentBleedTopOffset = options.marginHeight;
          }else{
              documentBleedUniformSize = false;
              documentBleedTopOffset = options.marginHeight;
              documentBleedBottomOffset = options.marginHeight;
              documentBleedOutsideOrRightOffset = options.marginWidth;
              documentBleedInsideOrLeftOffset = options.marginWidth;
          }
      }
  
      // margin 설정 ( Default Page )
      var page = newDocument.pages.item(0);
  
      page.marginPreferences.properties = {top : 0,left: 0,right: 0,bottom:0};
  
      newDocument.layers.add({name: 'Guide'});
      newDocument.layers.add({name: 'Text'});
      newDocument.layers.add({name: 'PicFrame'});
      newDocument.layers.add({name: 'Sticker'});
      newDocument.layers.add({name: 'Background'});
  
      for(var i=0; i < options.processLayers.length; i++){
          newDocument.layers.add({name: options.processLayers[i]});
      }
  
      newDocument.layers[newDocument.layers.length-1].remove();
  
      return null
  }
  
  function saveTemplateInfoToDocument(templateInfo){
      app.activeDocument.insertLabel('templateInfo', JSON.stringify(templateInfo));  //  템플릿정보 담은 templateInfo 라벨 생성
      app.activeDocument.save();  // 도큐먼트 저장
  }
  
  function getTemplateInfoFromDocument(){
      return app.activeDocument.extractLabel('templateInfo');  // templateInfo 라벨값 가져오기
  }
  
  
  function setDocumentLabel(label, value){
      app.activeDocument.insertLabel(label, value);  // 라벨 생성
  }

  //마스터정보 추출
  function createMaster(type){  // type = cover,content,..@{pageAlias:입력받은값}
    //마스터 생성
    app.activeDocument.masterSpreads.add();
    var newMasterName = app.activeDocument.masterSpreads.lastItem().name;  // 마지막 마스터 이름 (위에서 생성한 마스터)
    app.activeDocument.masterSpreads.item(newMasterName).baseName = type;  // 새로만든 마스터의 이름을 type으로 받은 걸로 저장함
  }
  
  function getDocumentLabel(label){
      return app.activeDocument.extractLabel(label);  // 라벨 값 가져오기
  }
  
  function getCurrentDocumentPath(isOriginDocument){  // isOriginDocument => true/false
      var currentDocument = app.activeDocument;
      var path = currentDocument.filePath;  // 현재 도큐먼트 파일 경로
      var name = currentDocument.name;  // 현재 도큐먼트 파일 이름
      var target;

      if (isOriginDocument){
          target = File(path).fsName.toString().replace(/\\/g, '/')+"/Links";  // 현재 도큐먼트 경로 뒤에 Links 붙여서 리턴
      }else{
          target = File(path).fsName.toString().replace(/\\/g, '/')+"/"+name;  // 현재 도큐먼트의 경로에 파일 이름 붙여서 리턴
      }
      return target
  }
  
  function makeId(){
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    
      for (var i = 0; i < 15; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    
      return text;  // 15글자 랜덤으로 만들어서 리턴
  }
  
  if (!String.prototype.includes) {
      String.prototype.includes = function(search, start) {
        'use strict';
        if (typeof start !== 'number') {
          start = 0;
        }
        
        if (start + search.length > this.length) {
          return false;
        } else {
          return this.indexOf(search, start) !== -1;
        }
      };
    }

// Amazon Access Token Config
function getAWSConfig(){
    var config = {accessKeyId : "AKIAJUQHOGKTV3MP34FQ",secretAccessKey : "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",region : "ap-northeast-2"};
    return JSON.stringify(config);
}

function getRestAPITokenInfo(){
    var token = {
        edicus : "d8251d25-1f15-44fd-a9c6-f2bff64b3902",
        red    : "QWRvYmVFeHRlbnNpb24="
    };
    return JSON.stringify(token); 
}
