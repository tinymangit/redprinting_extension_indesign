function init_ddp(userInfo, isConfirm){
  var DDP = cep_node.require('ddp');
  var Client = new DDP({

    host : "192.16.0.156",
    //host : "https://api2.redprinting.co.kr"
    port : 3000,
    //port : 80,
    ssl  : false,
    //ssl : true
    autoReconnect : true,
    autoReconnectTimer : 500,
    maintainCollections : true,
    ddpVersion : '1',
    useSockJs: true,
    url: 'wss://192.16.0.156:3000/websocket'
    //url : 'wss://api2.redprinting.co.kr/websocket'
  });

  Client.connect( function(error, wasReconnect){
    
    if (error) {
      console.log('DDP connection error!');
      return;
    }
    
    if (wasReconnect) {
      return null;
    }
    
    // if (!(isConfirm)){
    //     console.log('added listener');
    //     listenUserConfirm(Client, userInfo._id);
    // }

    listenNotification(Client, userInfo._id, userInfo.allowNotification);
    

  });
}

// 승인된 사용자 알람 리스너
function listenUserConfirm(ddp, userId){

  ddp.subscribe('extensionUserInformation', [userId], function () {
      return null;
    }
  );
  
  var observer = ddp.observe("Extension_Users");
  console.log(ddp.collections);
  observer.changed = function(id, oldFields, clearedFields, newFields) {
    var newUserInfo = ddp.collections.Extension_Users[id];
    
    console.log(newUserInfo);
    // 유저 정보 설정
    if (RedComponent){
      RedComponent.user = newUserInfo;
    }

    // 승인된 사용자 알람 ( 최초 승인시.. 권한 변경은 없는걸로 하고 중지시 삭제...)
    if (newUserInfo.isConfirm){
      var script = "sendMessageToEndUser(" +"'승인되었습니다. 이제부터 사용가능합니다.'" + ")";
      CSLibrary.evalScript(script);
    }

    // 알람 범위 변동 시
    if (oldFields["allowNotification"] !== newFields["allowNotification"]){
      var oldObserve = ddp.sbserve("Extension_Notification");
      oldObserve.stop();
      listenNotification(ddp, id, newUserInfo.allowNotification)
    }
    
  };
}

// 알림 리스너
function listenNotification(ddp, userId, allowPublicNotification){

  ddp.subscribe('extensionNotification', [userId, allowPublicNotification], function () {
      
      var observer = ddp.observe("Extension_Notification");
      observer.added = function(id) {
        var message = ddp.collections.Extension_Notification[id].message;
        var script = "sendMessageToEndUser('" +message+ "')";
        CSLibrary.evalScript(script);
      };

      return null;
    }
  );
  
}