// init previewer
function init(){
  // 객체 드랍 버블링 방지
  window.addEventListener("dragover",function(e){
    
    e = e || event;
    e.preventDefault();
  },false);

  window.addEventListener("drop",function(e){
    e = e || event;
    window.test = e;
    e.preventDefault();
  },false);

  // Global Interface define
  
  window.CSLibrary = new CSInterface();
  
  CSLibrary.addEventListener("kr.co.RedPrinting.Previewer", function(event){
    token = {
        edicus : event.data.RestAPI.EdicusSecureKey,
        red    : event.data.RestAPI.EditorAPIToken
    }
    window.RestAPI = new RestAPI_Interface(token);    
  })
  window.errorDir = [];
  window.serverConnFailText = "";

  // Image Slider Init


  // 이벤트 리스너 설정
  CSLibrary.addEventListener("kr.co.RedPrinting.Previewer", function(event){
    if (event.data.type === "event"){
        console.log("event",event)
        window.sliderLength = 0;

        var wrapper = $('.row');

        for(var i=0; i<event.data.templates.length; i++){  // 템플릿 페이지 갯수만큼 반복.

            previewTemplate(event.data.templates[i], i+1).then(function(result){ 
                console.log("result=",result)  // API 통신 성공시 result에 템플릿 페이지 정보 들어옴
                console.log(result.pageInfos);
                for(var j=0; j<result.pageInfos.length; j++){ 
                    
                    var svg = result.pageInfos[j].svg;

                    var carousel = document.createElement('div');
                    var items = document.createElement('div');

                    carousel.classList.add('carousel-cell');
                    carousel.classList.add('col-2');
                    items.classList.add('items');
                    items.innerHTML = svg;
                    items.children[0].setAttribute("width", "100%");
                    items.children[0].setAttribute("height", "100%");

                    carousel.appendChild(items);
                    $(carousel).on('click', function(e){
                        var target = $(e.currentTarget)
                        target.toggleClass('active');
                        target.hasClass('active') ? target.css({top : document.documentElement.scrollTop}) : target.css({top : 'auto'})
                    });
                    var loadingContent = document.getElementById("loadingContent");
                    loadingContent.style.display = 'none';        

                    wrapper.append(carousel);
                }
                console.log("result.unregistered_fonts_found.length1",result.unregistered_fonts_found.length);
                if (result.unregistered_fonts_found.length > 0){
                    //CSLibrary.evalScript("sendMessageToEndUser('업로드 되지 않은 폰트가 존재합니다. 업로드를 진행하신다면 후에 반드시 수정해주세요.')", function(g){console.log(g)});
                    console.log("result.unregistered_fonts_found.length2",result.unregistered_fonts_found.length);
                    // alert(`업로드 되지 않은 폰트가 존재합니다. 업로드 후 재시도 해주세요. \r\n ${JSON.stringify(result.unregistered_fonts_found, null, 2)}`)
                    
                    //폰트에러시 업로드진행 불가하게.. -> 2018 12 14 디자인팀 요청에따라 폰트에러시에만 업로드 가능하게 진행 
                    var fontErrorText = `업로드 되지 않은 폰트가 존재합니다. 업로드 후 재시도 해주세요. 오류 검출시 서버업로드 진행이 불가능합니다.  \r\n ${JSON.stringify(result.unregistered_fonts_found, null, 2)}`
                    errorMessageAlert(fontErrorText);
                }

            }).catch(function(err){  // api 통신에 실패하면 여기로
                var errorStack = JSON.stringify(err, null, 2);

                var carousel = document.createElement('div');
                var items = document.createElement('div');

                carousel.classList.add('carousel-cell');
                carousel.classList.add('col-2');
                items.classList.add('items');
                items.innerText = errorStack;
                
                carousel.appendChild(items);
                $(carousel).on('click', function(e){
                    var target = $(e.currentTarget)
                    target.toggleClass('active');
                    target.hasClass('active') ? target.css({top : document.documentElement.scrollTop}) : target.css({top : 'auto'})
                });
                
                wrapper.append(carousel);

                console.log("error.message",err);
                errorMessageAlert(err.message);

            });
        }
        
        errorMessageAlert(window.serverConnFailText);
        // var event = new CSEvent("kr.co.RedPrinting.Panel", "APPLICATION");
        // event.data = {
        //                 errorText   :  window.serverConnFailText
        //              };
        // CSLibrary.dispatchEvent(event);         

    }
  });

    requestPreviewData();
  
}


//에러메세지 전송

function errorMessageAlert(errText){

    var event = new CSEvent("kr.co.RedPrinting.Panel", "APPLICATION");
    event.data = {
                    errorText   :  errText
                 };
    CSLibrary.dispatchEvent(event);
}


// 렌더링 승인 여부 전송
function requestPreviewData(){
    var event = new CSEvent("kr.co.RedPrinting.Panel", "APPLICATION");
     event.data = {
                   requestType :  "previewRequest",
                  };

    CSLibrary.dispatchEvent(event);
}

function previewTemplate(templateInfo, index){
    console.log("templateInfo",templateInfo)  // {pageIdList, templatePath(template.json), templateDir(.temp), metadata(metadata.json)}
    console.log(" index", index)
    // 업로드된 템플릿 상품 미리보기
    var viewer = document.querySelector("#previewArea");
    
    var form = new FormData();

    var base64 = cep.fs.readFile(templateInfo.templatePath , cep.encoding.Base64).data;
    var file = base64ToFile(base64, "template.json");

    var metaJson = cep.fs.readFile(templateInfo.metadata , cep.encoding.UTF8).data;
    // var metaFile = base64ToFile(metaJson, "metadata.json");

    form.append("doc", file);
    form.append("meta", metaJson);
    form.append("dir", templateInfo.templateDir);
    console.log("form",form)
    console.log("file",file)
    console.log("dir",templateInfo.templateDir)

    return getPreviewSVG(form, index);
}


// Edicus Server API 호출
async function getPreviewSVG(form, index){

    
    try {
        var result = await RestAPI.getPreviewSVG(form);  // api 통신 성공하면 { pageInfos, unregistered_fonts_found, unresolved_font_group_ids } 가져옴
        result.index = index  // index 어디에 쓰이는지 모르겠음
        return result;
    }catch (error){  // api 통신 실패하면 reject로 보내준 http.responseText를 error로 받음
        window.errorDir.push(form);
        var dirPath = form.get("dir");  // .temp 파일의 경로
        result.errorDir = dirPath;  // result 없는게 당연.. 여기서 에러남
        Promise.reject(result)  // 없는 result 반환중..
        //서버오류시 전역변수에 에러메세지를 담아 requestPreviewData() -> event 객체에 담는다.
        console.log("error.message",error.message)
        window.serverConnFailText = error.message;
        errorMessageAlert(window.serverConnFailText);
        
    }
}

function initLoading(){

    document.querySelector(".loading-area").style.opacity = 1;

    var lineDrawing = anime({
        targets: '#loading-animation .lines path',
        strokeDashoffset: [anime.setDashoffset, 0],
        easing: 'easeInOutSine',
        duration: 1500,
        delay: function(el, i) { return i * 250 },
        direction: 'alternate',
        loop: true
    });
}

function endLoading(){
    document.querySelector(".loading-area").style.opacity = 0;
    $("#mainWrapper").css("opacity", "1");
    
}