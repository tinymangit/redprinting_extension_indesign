
//----------------------------------------------- 여기부터 이벤트 설정영역 초기 이벤트만 설정하고 나머지는 모두 vue method 로 변경한다.  -------------------------------

async function init(){
  
  // 객체 드랍 버블링 방지
  window.addEventListener("dragover",function(e){
    e = e || event;
    e.preventDefault();
  },false);

  window.addEventListener("drop",function(e){
    e = e || event;
    window.test = e;
    e.preventDefault();
  },false);


  // Global Interface define
  window.CSLibrary = new CSInterface(); 

  // // 이벤트 리스터

  CSLibrary.addEventListener("kr.co.RedPrinting.Panel", function(event){
    // preview 정보 요청 판단
    if (event.data.requestType === "previewRequest"){
       
      var event = new CSEvent("kr.co.RedPrinting.Previewer", "APPLICATION");
      event.data = {
        type      : "event",
        templates : RedComponent.previewInfo,
        RestAPI   : window.RestAPI 
      }
      console.log("event.data",event.data)
      CSLibrary.dispatchEvent(event);

    }

    //변환오류시 서버 업로드 잠금처리
    console.log("event.data.errorText",event.data.errorText);
    if (event.data.errorText){
      RedComponent.uploadErrorState = true;
      alert(event.data.errorText + ". / 오류 검출시 서버업로드 진행이 불가능합니다.");
    }else{
      RedComponent.uploadErrorState = false;
    }
  });
  
  // ExtendScript Init
  var mainJSX =  CSLibrary.getSystemPath(SystemPath.EXTENSION) + '/jsx/main.jsx';
  var script = '$.evalFile("' + mainJSX + '");';
  CSLibrary.evalScript(script);

  await initRestAPI();
  

  // 로그인 정보 조회 ( 존재할 경우 바로 메인페널로... )
  var user           =  localStorage.getItem('connectUser');
  var serverConnInfo =  localStorage.getItem('connectServer') || "ops";

  if (user === null || serverConnInfo === "null"){
    init_component(function(){}, 'loginView', null, serverConnInfo); 
  }else {
    user = JSON.parse(user);
    // init_ddp(user._id, user.isConfirm);
    init_component(function(){}, 'mainView', user, serverConnInfo);
  }

  initLoading();

}

function splashAnimation(selector, callback){
  $(selector).velocity('fadeIn', { duration: 100, complete: function(e) {
        return $(e).velocity({
          skewX: 10,
          skewY: 10
        }, 100).velocity({
          skewX: -10,
          skewY: -10
        }, 100).velocity({
          skewX: 10,
          skewY: 10
        }, 100).velocity({
          skewX: -10,
          skewY: -10
        }, 100).velocity({
          skewX: 10,
          skewY: 10
        }, 100).velocity({
          skewX: -10,
          skewY: -10
        }, 100).velocity({
          skewX: 10,
          skewY: 10
        }, 100).velocity({
          skewX: 0,
          skewY: 0
        }, 500).velocity('fadeOut', {
          duration: 1000,
          complete: function(e) {
            callback();
          }
        });
      }
    });
}

function initLoading(){

  window.loadingSection = anime({
      targets: '#loading-animation .lines path',
      strokeDashoffset: [anime.setDashoffset, 0],
      easing: 'easeInOutSine',
      duration: 1500,
      delay: function(el, i) { return i * 250 },
      direction: 'alternate',
      loop: true
  });
}

function showLoading(){
  document.querySelector(".loading-area").style.opacity = 1;
  document.querySelector(".loading-area").style["z-index"] = 1000;
  loadingSection.restart();
}

function endLoading(){
  document.querySelector(".loading-area").style.opacity = 0;
  document.querySelector(".loading-area").style["z-index"] = -1;
  loadingSection.pause();
}

function getObjectAttribute(){
  // var script = "getAttributes()"
	// CSLibrary.evalScript(script, function(result) {
  //   if (result != "EvalScript error."){
  //     var obj = JSON.parse(result);
  //     if(obj.res_type == 1) {
      
  //       var fields = Object.keys(obj);
  //       for(field of fields){
  //         if (obj[field] && (field !=="res_type" && field !== "objname") ){
  //           var target = document.getElementById(field);
  //           target.checked = (obj[field] === "true") ? true : false
  //         }
  //       }
  //     }
  //   }
  // });
}



// Tested function 
// Red Export Function 
function exportData(){
  CSLibrary.evalScript("exportToData({uploadType : 'S', productCode : '23x23@DFSADSF'})", function(path){
    console.log("path!!!",path);
  });
}

// Get Current Page Text layout Infomaiton Function
function testMenu(){
  CSLibrary.evalScript("TestMenuListener()");
}

// Text Return Test
function testGetText(){
  CSLibrary.evalScript("getTextList(app.activeWindow.activePage)", function(text){
    console.log(text);
  });
};

// Token Async Function 
function initRestAPI(){
  return new Promise(function(resolve, reject){

    CSLibrary.evalScript("getRestAPITokenInfo()", function(token){
      window.RestAPI = new RestAPI_Interface(JSON.parse(token));
      resolve(true)
    })

  });
}