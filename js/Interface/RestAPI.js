function RestAPI_Interface(token){

    var _self = this;
    this.RedBaseURL = "https://makers.redprinting.net";   // 운영 Base Url
    this.EdicusServerURL = "https://api-dot-edicusbase.com/";
    this.EdicusResourceURL = "https://resource-dot-edicusbase.appspot.com/";
    this.EdicusSecureKey = token.edicus;
    this.EditorAPIToken = token.red;

    this.Http = new XMLHttpRequest();

    this._getRedAPI = function(){
        return {
            "authorize" : {
                method : "POST",
                url    : _self.RedBaseURL+"/v1/auth"
            },
            "createDesigner" : {
                method : "POST",
                url    : _self.RedBaseURL+"/v1/designers"
            },
            "addTemplate" : {
                method : "POST",
                url    : _self.RedBaseURL+"/v1/templates"
            },
            "status" : {
                method : "GET",
                url    : _self.RedBaseURL+"/status"
            },
            "deleteTemplates" : {
                method : "DELETE",
                url    : _self.RedBaseURL+"/v1/templates"
            },
            "updateTemplateInfo" : {
                method : "PUT",
                url    : (templateId)=> _self.RedBaseURL+"/api/template/"+templateId
            }
        }
    }
    // Red Printing Server API
    this.RedAPI = this._getRedAPI();

    // Edicus Server API ( CEP 사용 여부 미지수 )
    this.EdicusServerAPI = {
        "getToken" : {
            method : "POST",
            url    : this.EdicusServerURL+"api/auth/token"
        },
        "getProjects" : {
            method : "GET",
            url    : this.EdicusServerURL+"api/projects"
        },
        "deleteProject" : {
            method : "DELETE",
            url    : function(projectId){
             return _self.EdicusServerURL+"api/projects/"+projectId;
            }
        }
    };

    // Edicus Resource API
    this.EdicusResourceAPI = {
        "getProductList" : {
            method : "GET",
            url    : this.EdicusResourceURL+"resapi/product/list"
        },
        "issueResourceToken" : {
            method : "GET",
            url    : this.EdicusResourceURL+"resapi/token"
        },
        "uploadTemplate": {
            method : "POST",
            url    : this.EdicusResourceURL+"resapi/package"
        },
        "previewTemplate": {
            method : "POST",
            url    : this.EdicusResourceURL+"resapi/preview"
        },
        "getBulkUploadUrl": {
            method : "GET",
            url    : this.EdicusResourceURL+"resapi/pkg-upload-url"
        },
        "enrollBulkUpload": {
            method : "POST",
            url    : this.EdicusResourceURL+"resapi/pkg-uploaded"
        }
    }

}

// Edicus api key setting
RestAPI_Interface.prototype.setTargetServer = function(mode){

    var _self = this;

    if (mode === "ops"){
        
        this.RedBaseURL = "https://makers.redprinting.net";   // 운영 Base Url

    }else if (mode === "dev" ){
        this.RedBaseURL = "http://dev.makers.redprinting.net";   // 개발 Base Url
        //this.RedBaseURL = "http://739d19cd.ngrok.io";   // 개발 Base Url

    }else if (mode === "local"){
        this.RedBaseURL = "http://172.16.5.35:3500";          // 로컬 Base Url
    }

    this.RedAPI = _self._getRedAPI();
}

// Edicus api key setting
RestAPI_Interface.prototype.setSecureKey = function(key){
    this.EdicusSecureKey = key;
}

// Login user uuid setting
RestAPI_Interface.prototype.setEdicusUUID = function(uuid){
    this.EdicusUUID = uuid;
}

// Authorization for Edicus 
RestAPI_Interface.prototype.authorization = function(form, cb){

    var _self = this;    
    var http = _self.Http;
    var requestInfo = _self.RedAPI.authorize;

    // for (var key of form.keys()) {
    //     console.log("key",key);
    //   }
    //   for (var value of form.values()) {
    //     console.log("value",value); 
    // }

    http.onreadystatechange = function(){
        
      if(http.readyState == 4 && http.status == 200){
        
        cb(null, http.responseText);
        try{
            cb(null, JSON.parse(http.responseText));
        }catch(e){
            alert(e)
        }
        
      }else if (http.readyState == 4 && http.status != 200) {
        cb(JSON.parse(http.responseText));
      }
    };

    http.open(requestInfo.method, requestInfo.url);
    http.setRequestHeader("Authorization", _self.EditorAPIToken);
    http.send(form);
    return null;
}

// Create RedPrinting Extension Users
RestAPI_Interface.prototype.createDesigner = function(form, cb){

    _self = this;
    var http = _self.Http;
    var requestInfo = _self.RedAPI.createDesigner;

    http.onreadystatechange = function(){
      if(http.readyState == 4 && http.status == 200){
        cb(null, JSON.parse(http.responseText));
      }else if (http.readyState == 4 && http.status != 200) {
        cb(JSON.parse(http.responseText));
      }
    };
    http.open(requestInfo.method, requestInfo.url);
    http.setRequestHeader("Authorization", _self.EditorAPIToken);
    http.send(form);

    return null;

}

// Add Templale To Extension User After Template Uploaded 
RestAPI_Interface.prototype.addTemplateInfo = function(templateInfo){
    var _self = this;

    console.log("templateInfo@@@@@@",templateInfo)

    return new Promise(function(resolve, reject){

        var http = _self.Http;
        var requestInfo = _self.RedAPI.addTemplate;

        http.onreadystatechange = function(){
            if(http.readyState == 4 && http.status == 200){
                resolve(JSON.parse(http.responseText));
            }else if (http.readyState == 4 && http.status != 200) {
                reject(JSON.parse(http.responseText));
            }
        };
        
        http.open(requestInfo.method, requestInfo.url);
        //http.open(requestInfo.method, "http://172.16.6.47:6500/v1/templates");
        http.setRequestHeader("Authorization", _self.EditorAPIToken);
        http.setRequestHeader("Content-Type", "Application/json");
        http.send(JSON.stringify(templateInfo));
    });

}

// Upload Template To Edicus Server
RestAPI_Interface.prototype.uploadTemplates = function(form, templateDocument){

    var _self = this;
       
    // for (var key of form.keys()) {
    //     console.log("key",key);
    //   }
    //   for (var value of form.values()) {
    //     console.log("value",value); 
    // }

    rscType = form.get('rsc_type')
    form.delete('rsc_type')
    return new Promise(function(resolve, reject){

        var http = new XMLHttpRequest();
        var requestInfo = _self.EdicusResourceAPI.uploadTemplate;
        console.log("requestInfo@@@@",requestInfo)
        http.onreadystatechange = function(){
           if(http.readyState == 4 && http.status == 200){

            var result = JSON.parse(http.responseText);
            
            if(rscType !== "template"){
                console.log("response",http.response)
                var response = JSON.parse(http.response)
                result.edicusResource =  JSON.parse(response.doc).resources[0]
                console.log(JSON.parse(response.doc).resources[0])
                result.resource_id    =  JSON.parse(response.doc).resources[0].rid
            }

            result.templatePath   = templateDocument.templatePath;
            result.templateDir    = templateDocument.templateDir;
            result.token          = templateDocument.token;
            result.index          = templateDocument.index;
            console.log('result',)
                    
            // variable items setting
            if (templateDocument.variableItemList){
                result.variableItemList = templateDocument.variableItemList;
            }
            resolve(result);
            
          }else if (http.readyState == 4 && http.status != 200) {
            reject(JSON.parse(http.responseText));
          }
        };

        
        http.open(requestInfo.method, requestInfo.url);
        http.setRequestHeader("edicus-api-key", _self.EdicusSecureKey);
        http.setRequestHeader("download-json", "true");
        http.setRequestHeader("rsc-type", rscType);
        if(rscType !== "template"){
            http.setRequestHeader("skip-rsclist", "true");
        }
        http.send(form);
    });
}

// Get Product List
RestAPI_Interface.prototype.getProductList = function(cb){

    var _self = this;
    var http = _self.Http;
    var requestInfo = _self.EdicusResourceAPI.getProductList;

    http.onreadystatechange = function(){
        if(http.readyState == 4 && http.status == 200){
         // 콜백 있을 경우
         if (cb){
             cb(null, JSON.parse(http.responseText));
         }
       }else if (http.readyState == 4 && http.status != 200) {
         cb(JSON.parse(http.responseText));
       } 
     };
     http.open(requestInfo.method, requestInfo.url);
     http.setRequestHeader("edicus-api-key", _self.EdicusSecureKey);
     http.send();
}

// Issue Template Token ( Sync XMLHttprequest ! )
RestAPI_Interface.prototype.generateTemplateToken = function(cb){

    var _self = this;
    var http = _self.Http;
    var requestInfo = _self.EdicusResourceAPI.issueResourceToken;

    http.onreadystatechange = function(){
        if(http.readyState == 4 && http.status == 200){
         
         // 콜백 있을 경우
         if (cb){
             cb(null, JSON.parse(http.responseText));
         }
       }else if (http.readyState == 4 && http.status != 200) {
         cb(JSON.parse(http.responseText));
         alert("응용프로그램과 Rest서버와의 연결이 원활하지않습니다. 프로그램 재시작 후 다시 시도 해주시기 바랍니다.")
       } 
    };
    http.open(requestInfo.method, requestInfo.url, false);
    http.setRequestHeader("edicus-api-key", _self.EdicusSecureKey);
    http.send();

    var token = JSON.parse(http.responseText).token;
    return token
}

RestAPI_Interface.prototype.getPreviewSVG = function(form){  // form = FormData()
    
    _self = this;
    var requestInfo = _self.EdicusResourceAPI.previewTemplate;
    console.log("requestInfo=",requestInfo)
    return new Promise(function(resolve, reject){
        var http = _self.Http;
        http.onreadystatechange = function(){
            if(http.readyState == 4 && http.status == 200){
                resolve(JSON.parse(http.responseText));
            }else if (http.readyState == 4 && http.status != 200) {
                console.log("http.responseText",http.responseText)  // error내용. (왜 api 통신 실패했는지)
                reject(JSON.parse(http.responseText));
            }
        };
        http.open(requestInfo.method, requestInfo.url);
        http.setRequestHeader("edicus-api-key", _self.EdicusSecureKey);
        http.send(form);
    });
}

RestAPI_Interface.prototype.getServerStatus = function(){

    _self = this;
    var requestInfo = _self.RedAPI.status;
    console.log("requestInfo!",requestInfo)

    return new Promise(function(resolve, reject){

        var http = _self.Http;
        http.onreadystatechange = function(){
            if(http.readyState == 4 && http.status == 200){
                resolve();
            }else if (http.readyState == 4 && http.status != 200) {
                console.log("reject",reject)   
                reject();
            }
        };

        http.open(requestInfo.method, requestInfo.url);
        http.setRequestHeader("Authorization", "QWRvYmVFeHRlbnNpb24=");
        http.send();

    });
}

RestAPI_Interface.prototype.uploadSplitTemplates = function(form){

    // S3로 변경함에 따라 해당 메소드는 Depreacated

    // _self = this;
    // var requestInfo = _self.ManagementAPI.uploadSplitTemplate;
    // console.log("rest1:",form);
    // console.log("rest2:",requestInfo);
    // return new Promise(function(resolve, reject){
       
    //     var http = _self.Http;
    //     http.onreadystatechange = function(){
    //         if(http.readyState == 4 && http.status == 200){
    //             console.log("rest3:",http.responseText);
    //             resolve(http.responseText);
    //         }else if (http.readyState == 4 && http.status != 200) {
    //             console.log("rest4:",http.responseText);
    //             reject(http.responseText);
    //         }
    //     }
    //     console.log("requestInfo.method",requestInfo.method)
    //     console.log("requestInfo.url",requestInfo.url)
        
    //     http.open(requestInfo.method, requestInfo.url);
    //     http.send(form);
        
    // })

}

RestAPI_Interface.prototype.deleteTemplates = function(form){
    
    _self = this;
    var requestInfo = _self.RedAPI.deleteTemplates;

    return new Promise(function(resolve, reject){
       
        var http = _self.Http;
        http.onreadystatechange = function(){
            if(http.readyState == 4 && http.status == 200){
                resolve(http.responseText);
            }else if (http.readyState == 4 && http.status != 200) {
                reject(http.responseText);
            }
        }
        http.open(requestInfo.method, requestInfo.url);
        http.setRequestHeader("Authorization", _self.EditorAPIToken);
        http.send(form);
        
    })
}

RestAPI_Interface.prototype.getBulkUploadUrl = function(){
    
    _self = this;
    var requestInfo = _self.EdicusResourceAPI.getBulkUploadUrl;
    

    return new Promise(function(resolve, reject){

        var http = _self.Http;
        http.onreadystatechange = function(){
            if(http.readyState == 4 && http.status == 200){
                resolve(JSON.parse(http.responseText));
            }else if (http.readyState == 4 && http.status != 200) {
                reject(http.responseText);
            }
        }
        http.open(requestInfo.method, requestInfo.url);
        http.setRequestHeader("edicus-api-key", _self.EdicusSecureKey);
        http.send();

    });
}

RestAPI_Interface.prototype.bulkFileUpload = function(url, zipFile){
    console.log("url=",url)
    _self = this;
    return new Promise(function(resolve, reject){

        var http = _self.Http;
        http.onreadystatechange = function(){
            if(http.readyState == 4 && http.status == 200){
                resolve(http.responseText);
            }else if (http.readyState == 4 && http.status != 200) {
                reject(http.responseText);
            }
        }
        http.open("PUT", url);
        http.setRequestHeader("Content-Type", "application/x-zip-compressed");
        http.send(zipFile);

    });
}

RestAPI_Interface.prototype.enrollBulkUpload = function(fileName, templateDocument){
    
    console.log("fileName=", fileName)
    console.log("templateDocument", templateDocument)
    _self = this;
    var requestInfo = _self.EdicusResourceAPI.enrollBulkUpload;
    console.log("requestInfo1=",requestInfo)

    return new Promise(function(resolve, reject){

        var http = _self.Http;
        http.onreadystatechange = function(){
            if(http.readyState == 4 && http.status == 200){
                var result = JSON.parse(http.responseText);
                result.templatePath = templateDocument.templatePath;
                result.templateDir  = templateDocument.templateDir;
                result.token        = templateDocument.token;
                result.index        = templateDocument.index;

                // variable items setting
                if (templateDocument.variableItemList){
                    result.variableItemList = templateDocument.variableItemList;
                }

                resolve(result);
            }else if (http.readyState == 4 && http.status != 200) {
                console.log("http.responseText",http.responseText)
                reject(http.responseText);
            }
        }
        console.log("requestInfo.url",requestInfo.url)
        console.log("requestInfo.method",requestInfo.method)

        http.open(requestInfo.method, requestInfo.url);
        http.setRequestHeader("edicus-api-key", _self.EdicusSecureKey);
        http.setRequestHeader("filename", fileName);
        http.send();

    });
}


//DB Data update.
RestAPI_Interface.prototype.setUpdateFieldData = function(templateId , doc, qry){

    _self = this;
    var requestInfo = _self.RedAPI.updateTemplateInfo;
    let body = {doc, qry}

    return new Promise(function(resolve, reject){

        var http = _self.Http;
        http.onreadystatechange = function(){
            if(http.readyState == 4 && http.status == 200){
                resolve(http.responseText);
                
            }else if (http.readyState == 4 && http.status != 200) {
                reject(http.responseText);
            }
        }

        console.log("requestInfo.url(templateId)",requestInfo.url(templateId))
        console.log("body",body)
        http.open(requestInfo.method, requestInfo.url(templateId));
        http.setRequestHeader("Authorization", _self.EditorAPIToken);
        http.setRequestHeader("Content-Type", "Application/json");
        http.send(JSON.stringify(body));
    });

}