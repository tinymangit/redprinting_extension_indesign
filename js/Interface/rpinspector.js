

/*
documentAfterActivate
documentAfterDeactivate
documentAfterSave
applicationBeforeQuit
applicationActivate

afterActivate
afterClose
afterContextChanged
afterDelete,
afterEmbed
afterInvoke
afterLinksChanged
afterMove
afterNew
afterOpen
afterPlace
afterQuit
afterSelectionAttributeChanged
afterSelectionChanged
afterUnembed
afterUpdate
beforeClose
beforeDeactivate
beforeDelete
beforeDisplay
beforeEmbed
beforeInvoke
beforeMove
beforePlace
beforeQuit
beforeUnembed
beforeUpdate
onInvoke

beforeExport
beforeImport
beforePrint
beforeRevert
beforeSave
beforeSaveACopy 
beforeSaveAs

afterExport
afterImport
afterPrint
afterRevert
afterSave
afterSaveACopy
afterSaveAs



*/
function callback(event)
{
  console.log("type=" + event.type + ", data=" + event.data);
}
     
var csInterface = new CSInterface();
csInterface.addEventListener("kr.co.RedPrinting.Inspector", callback); //invoke the function