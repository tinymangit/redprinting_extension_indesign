function init_component(cb, startTarget, userInfo, serverConnInfo){
    userInfo = userInfo || {};
    Vue.component('v-select', VueSelect.VueSelect);
    window.RedComponent = new Vue({
        el   :'#component',
        data : {
            user            : userInfo,
            currentPage     : startTarget,
            categories      : [],
            selectCategory  : "카테고리선택",
            products        : [],
            selectProduct   : "상품선택",
            sizes           : [],
            selectSize      : "사이즈선택",
            textList        : [],
            uploadType      : "템플릿타입선택",
            templatesPath   : null,
            previewInfo     : [],
            modifiedPages   : [],
            serverStatus    : false,
            templateUUIDs   : [],
            lockTemplateInfo: false,  // select 활성화 시키는 데 사용됨 (true이면 비활성화됨)
            targetServer    : serverConnInfo,
            isPhotoBook     : false,
            coverOptions    : {},
            psCodes         : [],
            extendScript    : "", // -> 대표님 용
            anotationLayerNames : ["Text","Sticker","PicFrame","Background","Cut","Crease","DashRule","KissCut","Punch","Scodix","ScodixGold","ScodixSilver","White","Transparent","GoldGlossyFoil","GlodMatteFoil","HologramFoil","BlackFoil","Guide","RenderBound","Numbering"],
            PicFrameWithoutPic  : false,
            cell_movable        : false,
            sticker_movable     : false,
            sticker_selectable  : false,
            text_movable        : false,
            text_selectable     : false,
            license             : false,
            multiSizeChoice     : false,
            uploadErrorState    : false, //미리보기 변환 오류시 true
            selectNotification  : userInfo.allowNotification,
            selectedServer      : "", //현재접속된 서버
            userPermission      : "", //사용자의 권한
            programVersion      : "1.8.4",
            localStorage        : [],
            isSplitState        : "",
            importantCutSizeWidth  : "",
            importantCutSizeHeight : "",
            allowDecimalPoint      : 0,
            whiteVisible           : false, //화이트레이어 visible 처리
            rsc_type               : "template",
            backgroundType         : "default",
            variableType           : "",
            uploadTypes            : [
                {value: "S", content: "싱글 템플릿"},
                {value: "D", content: "페어 템플릿"},
                {value: "M", content: "멀티 템플릿"},
            ],
            firstPageIsFullside : false, // 내지 첫째장 활성화여부
            lastPageIsFullside : false   // 내지 마지막장 활성화여부
        },
 
        mounted: function(){

            // 컴포넌트 init 완료시 이벤트 초기화
            var v = this;
            var startPage = $('#'+startTarget);
            startPage.velocity("fadeIn", { duration: 100 });
            //문서가 열릴때 실행.
            CSLibrary.addEventListener("documentAfterActivate", function(){
                v.getCustomOptions();
                v.checkTemplateInfo();
                v.getMetaData();
                v.setTargetServer();
                v.checkOpsUploadingFromDevelopment();
                v.saveTemplateInfo()
                v.variableType = ""
                
            })

            //문서안에서 객체에 이벤트가 발생했을때 실행
            CSLibrary.addEventListener("afterSelectionChanged", function(){       
                CSLibrary.evalScript("getCurrentPageId()", function(pageId){                
                    if (v.activePageId !== pageId){
                        v.activePageId = pageId;
                        v.getTextList();
                    }
                });
            });

            //스플릿상태 확인
            CSLibrary.evalScript("getDocumentLabel('isSplitPage')", function(value){
                if(value === "Y"){
                    v.isSplitState = "Y"
                }
            });

            //컷사이즈 강제설정시 불러오기
            CSLibrary.evalScript("getDocumentLabel('importantCutSizeWidth')", function(value){
                
                if(!isNaN(value)){
                  v.importantCutSizeWidth = value
                  document.getElementById("importantCutSizeWidth").value = value
                }
            })
            CSLibrary.evalScript("getDocumentLabel('importantCutSizeHeight')", function(value){
                
                if(!isNaN(value)){
                    v.importantCutSizeHeight = value
                    document.getElementById("importantCutSizeHeight").value = value
                }
            })

            // CSLibrary.evalScript("checkToken()", function(value){
            //     console.log("checkToken",value)
            // });

            v.checkOpsUploadingFromDevelopment();
            v.getCustomOptions();
            v.checkTemplateInfo();
            v.getMetaData();
            v.setTargetServer();


            if (v.user.profile && v.user.profile.divCode_master){
                v.getEdicusProductList();
                v.checkPermission();
            }
            
        },
        computed: {
            filterCategory : function() {
                return this.categories.filter(item => (this.targetServer==='ops' && item.dpName !== '테스트') || (this.targetServer !== 'ops' && item.dpName === '테스트'));
            },
            filterProduct : function() {
                return this.products.filter(item => item.cateCode === this.selectCategory.cateCode);
            }
        },
        methods: {
            test : function(){
                var v = this
                console.log("v.textList=", v.textList)
                CSLibrary.evalScript("checkToken()", function(value){
                    console.log("value",JSON.parse(value))
                })

                //var templateJSON = JSON.parse(cep.fs.readFile('/Users/kraken/Desktop/IndesignTest/27.키링/WbTRkLkn4kfo4GWQZ/WbTRkLkn4kfo4GWQZ/0001.temp/template.json').data);
            },

            createMaster : function(type) {
                var anotationName = document.getElementById("masterAnotationName").value  // 마스터 생성 창에서 입력받는 값
                
                if (anotationName) {  // 입력받은 값이 있으면
                    var  type = type + "@{pageTypeAlias:"+anotationName+"}"  // 마스터타입과 입력받은 값을 하나의 변수에 저장
                    CSLibrary.evalScript(`createMaster('${type}')`);  // 마스터 생성 함수에 전달
                }else{
                    CSLibrary.evalScript(`createMaster('${type}')`);
                }

                document.getElementById("masterAnotationName").value = ""  // 입력하는 부분 비워둠
                
            },

            checkPermission : function(){
                var v = this
                if(v.user.profile.permission.indesign.readWrite === true){
                    v.userPermission = "readWrite"
                    console.log("읽기쓰기권한")
                    //$("#checkPermission").find("*").prop('disabled', null);
                    $("#checkPermission").removeAttr("disabled");
                }else if(v.user.profile.permission.indesign.read === true && v.user.profile.permission.indesign.readWrite == false){
                    v.userPermission = "read"
                    console.log("읽기권한")
                    $("#checkPermission").find("*").prop('disabled', true);
                }else{
                    alert("접속권한이 없습니다.")
                    v.logout();
                }

            },
            // 강제로 운영에 올라간 상품설정 변경할수 있도록 한다.
            unlockTemplate : function(){

                if (confirm("**주의**  Unlock Template 실행시 운영서버에 업로드된 템플릿이라면 템플릿 수정사항이 반영되며, 다시 되돌릴 수 없습니다. 그래도 실행하시겠습니까?") ==  true){
                    this.lockTemplateInfo = false;  // select 활성화됨
                }

            },
            convertToTest : function(){  // 안쓰고 있음

                if (confirm("**주의**  Convert To Test 실행시 운영중인 템플릿을 강제로 테스트서버에 업로드 시키며, 다시 되돌릴 수 없습니다. 그래도 실행하시겠습니까?") ==  true){
                    //테스트상품 강제 isProduction N 작업
                    CSLibrary.evalScript("setDocumentLabel('isProduction', 'N')")  // isProduction 라벨에 N 저장
                }
            },
            convertToSplit : function(){

                if (confirm("**주의**  Convert To Split 실행시 indd/link파일 업로드 가능상태로 전환됩니다. 그래도 실행하시겠습니까?") ==  true){
                    //테스트상품 강제 isProduction Y 작업  ( ?? isSplitPage N 인뎅..)
                    CSLibrary.evalScript("setDocumentLabel('isSplitPage', 'N')")
                }
            },
            // PSCODE 생성
            togglePsCode : function(psCode, event){

                var v = this;
                var hasCode = v.psCodes.includes(psCode);
                if (event.target.checked){
                    if(!hasCode){
                        v.psCodes.push(psCode);
                    }
                }else {
                    if(hasCode){
                        v.psCodes = v.psCodes.filter(function(c){console.log(c, psCode); return c !== psCode });
                    }
                }
            },

            // 커스텀 옵션 관련 함수
            getCustomOptions : function(){
                var v = this;

                CSLibrary.evalScript("getDocumentLabel('customOptions')", function(options){  // customOptions 라벨 값을 options로 받아옴 (options에 포토북일때 나오는 커버탭에 설정한 값들 들어있음)                    
                    if (options !== "EvalScript error." && options){  
                        var customOptions = JSON.parse(options);
                        
                        // 포토북 관련 옵션 유무 확인 후 세팅
                        var hasPhotoInfo    = customOptions.filter(function(doc){return doc.type === "photobook"})  // type이 photobook인 것 찾아서 저장

                        if (hasPhotoInfo.length > 0){  // 포토북 타입이면
                            v.isPhotoBook = true;
                            v.coverOptions = hasPhotoInfo[0].info.cover;
                            
                        }
                    }
                });

            },

            //메타데이터 가져와서 옵션설정
            getMetaData : function(){
                var v = this;
                CSLibrary.evalScript("getDocumentLabel('metaInfo')", function(metaInfo){
                    if(metaInfo){
                        var customMetaInfo = JSON.parse(metaInfo);
                        v.cell_movable       = (customMetaInfo.cell_movable       == true) ? true : false,
                        v.sticker_movable    = (customMetaInfo.sticker_movable    == true) ? true : false,
                        v.sticker_selectable = (customMetaInfo.sticker_selectable == true) ? true : false,
                        v.text_movable       = (customMetaInfo.text_movable       == true) ? true : false,
                        v.text_selectable    = (customMetaInfo.text_selectable    == true) ? true : false,
                        v.rsc_type           = customMetaInfo.rsc_type
                        return metaInfo;
                    }
                });
            },

            // 모든 페이지의 토큰 초기화
            resetAllPageToken : function(e){
                var v = this;

                if (confirm("**주의**  Reset Token 실행시 이미 등록되어있는 템플릿이 있다면 별도의 템플릿으로 분리되어 업로드가 되며, 다시 되돌릴 수 없습니다. 그래도 실행하시겠습니까?") ==  true){
                    v.historyLog(e)
                    CSLibrary.evalScript("resetAllPageTokens()", function(result){
                        if (result !== "EvalScript error."){
                            CSLibrary.evalScript("sendMessageToEndUser('Reset Complete')", function(g){console.log(g)});  // null 리턴받음
                            CSLibrary.evalScript("setDocumentLabel('isProduction', 'N')")
                            CSLibrary.evalScript("setDocumentLabel('isSplitPage', 'N')")

                        }
                    });
                }
            },

            //초기화 전 로그
            historyLog : function(e){
                var v = this;
                var TemplatelogInfo = {}

                CSLibrary.evalScript("allTokenInfo()", function(value){
                    TemplatelogInfo = JSON.parse(value)
                    TemplatelogInfo.userInfo = v.user
                    var dateObj = new Date();
                    var dateFormat = (()=>{
                        return `${dateObj.getFullYear()}${dateObj.getMonth()+1}${dateObj.getDate()}${dateObj.getHours()}${dateObj.getMinutes()}${dateObj.getSeconds()}`;
                    })();
                    CSLibrary.evalScript("getDocumentLabel('isSplitPage')", function(value){
                        TemplatelogInfo.isSplit = value
                    });
                    CSLibrary.evalScript("getDocumentLabel('isProduction')", function(value){
                        TemplatelogInfo.isProduction = value
                    });

                    TemplatelogInfo.programVersion  = v.programVersion
                    TemplatelogInfo.templatePath    = v.templatePath
                    TemplatelogInfo.targetServer    = v.targetServer
                    TemplatelogInfo.rdatetime       = dateFormat
                    TemplatelogInfo.event           = e

                    CSLibrary.evalScript("getDocumentLabel('resetLog')", function(resetLog){
                        if (!resetLog){
                            var rsTemplatelogInfo = []
                            rsTemplatelogInfo.push(resetLog)
                            rsTemplatelogInfo.push(TemplatelogInfo)
                            CSLibrary.evalScript(`setDocumentLabel('resetLog', '${JSON.stringify(rsTemplatelogInfo)}')`);
                        }else{
                            try{
                                var resetLog = JSON.parse(resetLog)
                                
                                resetLog.push(TemplatelogInfo)
                                CSLibrary.evalScript(`setDocumentLabel('resetLog', '${JSON.stringify(resetLog)}')`);
                            }catch(e){
                                CSLibrary.evalScript(`setDocumentLabel('resetLog', '')`);
                                console.log("ResetLogError",e)
                            }

                        }

                    });
                });

            },
            // 타겟 서버 변경
            setTargetServer : function(){
              var v = this;
              if (v.targetServer === 'ops' && v.selectCategory.dpName === '테스트'){
                  v.selectCategory = '카테고리선택';
              }

              if (v.targetServer === 'dev' && v.selectCategory.dpName !== '테스트'){
                  v.selectCategory = '카테고리선택';
              }

              //개발서버일시 border color red
              if (v.targetServer === 'dev'){
                  document.getElementById("component").style.border = "5px solid red";
                  v.selectedServer = 'KoiExtension'
              }else if(v.targetServer === 'ops'){
                  document.getElementById("component").style.border = "0px";
                  v.selectedServer = 'KoiExtension'
              }else{
                document.getElementById("component").style.border = "0px";
                v.selectedServer = ''
              }

              RestAPI.setTargetServer(v.targetServer);
              v.checkServerStatus();
            },
            // 접속 서버 상태 점검
            checkServerStatus : function(){

                var v = this;
                RestAPI.getServerStatus().then(function(){v.serverStatus = true;}).catch(function(err){ console.log('에러실행됨', err); v.serverStatus = false;});

                setInterval(function(){
                    RestAPI.getServerStatus().then(function(){
                        if (!v.serverStatus){
                            v.serverStatus = true;
                            iziToast.info({backgroundColor : 'green', theme : 'dark', timeout: 5000, position: "topRight", title: '서버에 연결되었습니다.'});
                        }
                    }).catch(function(){
                        if (v.serverStatus){
                            v.serverStatus = false;
                            iziToast.info({backgroundColor : 'orange', theme : 'dark', timeout: 5000, position: "topRight", title: '서버와의 연결이 끊켰습니다. 서버 상태를 확인해주세요.'});
                        }
                    });
                }, 10 * 60000)  // 10분마다 실행
            },
            // 페이지 이동 ( Router )
            goToPage : function(nextTarget){
                var currentPage = $("#"+this.currentPage);
                var nextPage = $("#"+nextTarget);
                currentPage.velocity("fadeOut", { duration: 200, complete : function(){
                    nextPage.velocity("fadeIn", { duration: 200 });
                }});
                this.currentPage = nextTarget;
            },
            // 로그인
            login : function(){
                var v = this;
                var form = new FormData(document.querySelector('#authForm'));
                console.log("form",form)
                RestAPI.authorization(form, function(err ,result){
                  if (err){
                    v.indesignAlert("아이디 또는 비밀번호를 다시 확인하세요. 에디터에 등록되지 않은 아이디이거나, 아이디 또는 비밀번호를 잘못 입력하셨습니다.");
                    console.log(err);
                  }else if(!form.get('serverChoice')){
                    v.indesignAlert("서버선택을 확인해주세요.");
                  }else{
                    v.goToPage("introView");
                    splashAnimation('#introView', function(){
                        //init 두 번 실행으로 타입처리
                        if (typeof(result) === "string"){
                            v.user = JSON.parse(result)
                        }else{
                            v.user = result
                        }

                        var user = localStorage.setItem('connectUser', JSON.stringify(v.user));
                        var serverConnInfo = localStorage.setItem('connectServer', v.targetServer);

                        //init_ddp(result, result.isConfirm);
                        v.loginInfoSave(form.get('id'),form.get('password'))
                        v.getEdicusProductList();
                        v.checkPermission();
                        v.goToPage("mainView");

                    });
                  }
                });
            },
            //로컬스토리지에 계정정보 저장
            loginInfoSave : function(userId,userPassword){
                var v = this;
                //localStorage.clear();
                if(!localStorage.userInfo){
                    var userInfo = {};
                    userInfo[userId] = userPassword;
                    localStorage.userInfo = JSON.stringify(userInfo);

                }else{
                    if(JSON.parse(localStorage.userInfo).hasOwnProperty(userId)){
                        //이미 저장되어있는 계정
                    }else{
                        var userInfo = JSON.parse(localStorage.userInfo);
                        userInfo[userId] = userPassword;
                        localStorage.userInfo = JSON.stringify(userInfo);
                    }
                }
            },
            //사용자전환 모달창에서 로컬스토리지값 가져오기..
            loginInfoCall : function(){
                var v =this;
                var json = JSON.parse(localStorage.userInfo)
                v.localStorage = Object.entries(json)

            },
            // 회원 로그아웃
            logout : function(e){
                //e.preventDefault();
                var v = this;
                v.user = null;
                v.targetServer = null;
                localStorage.removeItem("connectUser");
                localStorage.removeItem("connectServer");

                this.goToPage('loginView');
            },

            userChange : function(loginInfo){
                var v = this
                v.logout()
                if(loginInfo[0]){
                    document.getElementById("userId").value = loginInfo[0]
                    document.getElementById("userPassword").value = loginInfo[1]
                }
            },

            //비교값이 다를때false
            matchingIgnoreCheck : function(fitstArg,secondArg,message){
                var v = this;

                if(fitstArg !== "" && secondArg !== "" && message !== ""){
                    if (fitstArg !== secondArg){
                        v.indesignAlert(message);
                        return false;
                    }
                }else{
                    return false;
                }

                return true
            },
            
            // 디자이너 회원 가입 요청
            // createDesigner : function(){
            //     var v = this;
            //     var form = new FormData(document.querySelector('#signupForm'));

            //     v.matchingIgnoreCheck(form.get('password'),form.get('password2'),"패스워드가 일치하지않습니다. 패스워드가 일치하는지 다시확인해주세요.");

            //     form.set('allowNotification', $('#notification').is(":checked"));
            //     form.set('confirmPwd', true);

            //     RestAPI.createDesigner(form, function(err ,result){
            //       if (err){
            //         console.log(err);
            //       }else{
            //         v.goToPage("introView");
            //         splashAnimation('#introView', function(){
            //             v.user = result.content;
            //             var user = localStorage.setItem('connectUser', JSON.stringify(result.content));
            //             init_ddp(result.content, false);
            //             v.goToPage("mainView");
            //         });
            //       }
            //     });
            // },

            // 디자이너 정보수정
            // infoUpdateDesigner : function(){
            //     var v = this;
            //     var form = new FormData(document.querySelector('#infoUpdateForm'));

            //     v.matchingIgnoreCheck(form.get('password'),form.get('password2'),"패스워드가 일치하지않습니다. 패스워드가 일치하는지 다시확인해주세요.");

            //     form.set('confirmPwd', true);

            //     RestAPI.createDesigner(form, function(err ,result){
            //       if (err){
            //         console.log(err);
            //       }else{
            //         v.goToPage("introView");
            //         splashAnimation('#introView', function(){
            //             v.user = result.content;
            //             var user = localStorage.setItem('connectUser', JSON.stringify(result.content));
            //             init_ddp(result.content, false);
            //             v.goToPage("mainView");
            //         });
            //       }
            //     });
            // },

            // 인디자인 파일 템플릿 파일로 변환
            convertToTemplate : function(cb){
                var v = this;
                var productInfo = {};

                productInfo.uploadType  = v.uploadType;
                productInfo.modifiedPages = (v.modifiedPages.length == 0) ? null : v.modifiedPages;
                productInfo.features = {
                    variableType      : v.variableType,
                    whiteVisible      : v.whiteVisible,
                    allowDecimalPoint : v.allowDecimalPoint,
                    productCode       : v.selectSize.sizeCode+"@"+v.selectProduct.prodCode
                };

                //픽프레임 미사용시
                if (v.PicFrameWithoutPic === true && v.isPhotoBook === false){
                    console.log("픽프레임 미포함");
                    productInfo.uploadType += 'r';
                }
                    
                if (v.isPhotoBook){
                    //엽서북 테스트올릴시 변경
                    if (v.selectProduct.prodCode == "POSTCARDBOOK" || v.selectProduct.prodCode == "TPCASET"){
                        productInfo.customOptions = [{
                            type: "photobook",
                            info: {
                                // cover: {
                                //     mode: v.coverOptions.mode || "split",
                                //     width: "320",
                                //     height: "114",
                                //     effective_width: "320",
                                //     effective_height: "114",
                                //     spine: 10,
                                //     spine_mm: 10
                                // },
                                cover: {
                                    mode: v.coverOptions.mode || "split",
                                    width: parseInt(v.coverOptions.width) || 316,
                                    height: parseInt(v.coverOptions.height) || 110,
                                    effective_width: parseInt(v.coverOptions.effective_width) || 316,
                                    effective_height: parseInt(v.coverOptions.effective_height) || 110,
                                    spine: parseInt(v.coverOptions.spine) || 10,
                                    spine_mm: parseInt(v.coverOptions.spine_mm) || 10
                                },
                                content: {
                                    first_page_is_fullside: true,
                                    last_page_is_fullside : true
                                }
                            }
                        }];

                    //북클릿 A5일시
                    }else if(v.selectProduct.prodCode === "PHBKSTA" || v.selectProduct.prodCode === "PHBKSTA"){
                        productInfo.customOptions = [{
                            type: "photobook",
                            info: {
                                // cover: {
                                //     mode: v.coverOptions.mode || "split",
                                //     width: "320",
                                //     height: "114",
                                //     effective_width: "320",
                                //     effective_height: "114",
                                //     spine: 10,
                                //     spine_mm: 10
                                // },
                                cover: {
                                    mode: v.coverOptions.mode || "split",
                                    width: parseInt(v.coverOptions.width) || 316,
                                    height: parseInt(v.coverOptions.height) || 110,
                                    effective_width: parseInt(v.coverOptions.effective_width) || 316,
                                    effective_height: parseInt(v.coverOptions.effective_height) || 110,
                                    spine: parseInt(v.coverOptions.spine) || 10,
                                    spine_mm: parseInt(v.coverOptions.spine_mm) || 10
                                },
                                content: {
                                    first_page_is_fullside: true,
                                    last_page_is_fullside : true
                                }
                            }
                        }];
                    }else if(v.selectProduct.prodCode === "TPPHSETTEST"){

                        if(v.selectSize.dpName == "127x89(5x3)"){
                            productInfo.customOptions = [{
                                type: "photobook",
                                info: {
                                    cover: {
                                        mode: v.coverOptions.mode || "split",
                                        width: parseInt(v.coverOptions.width) || 269,
                                        height: parseInt(v.coverOptions.height) || 99,
                                        effective_width: parseInt(v.coverOptions.effective_width) || 269,
                                        effective_height: parseInt(v.coverOptions.effective_height) || 99,
                                        spine: parseInt(v.coverOptions.spine) || 5,
                                        spine_mm: parseInt(v.coverOptions.spine_mm) || 5
                                    },
                                    content: {
                                        first_page_is_fullside: true,
                                        last_page_is_fullside : true
                                    }
                                }
                            }];

                        }else{
                            productInfo.customOptions = [{
                                type: "photobook",
                                info: {
                                    cover: {
                                        mode: v.coverOptions.mode || "split",
                                        width: parseInt(v.coverOptions.width) || 315,
                                        height: parseInt(v.coverOptions.height) || 110,
                                        effective_width: parseInt(v.coverOptions.effective_width) || 310,
                                        effective_height: parseInt(v.coverOptions.effective_height) || 110,
                                        spine: parseInt(v.coverOptions.spine) || 5,
                                        spine_mm: parseInt(v.coverOptions.spine_mm) || 5
                                    },
                                    content: {
                                        first_page_is_fullside: true,
                                        last_page_is_fullside : true
                                    }
                                }
                            }];
                        }

                    } else{
                        //10x10 사이즈 고정
                        productInfo.customOptions = [{
                            type: "photobook",
                            info: {
                                cover: {
                                    mode: v.coverOptions.mode || "split",
                                    width: parseInt(v.coverOptions.width) || 602,
                                    height: parseInt(v.coverOptions.height) || 306,
                                    effective_width: parseInt(v.coverOptions.effective_width) || 552,
                                    effective_height: parseInt(v.coverOptions.effective_height) || 256,
                                    spine: parseInt(v.coverOptions.spine) == 0 ? 0 : parseInt(v.coverOptions.spine),  // spine을 0으로 설정할 수 있도록 수정함   입력한 값으로 저장되도록 수정(20210112)
                                    spine_mm: parseInt(v.coverOptions.spine) == 0 ? 0 : parseInt(v.coverOptions.spine)  // spine_mm을 0으로 설정할 수 있도록 수정함   입력한 값으로 저장되도록 수정(20210112)
                                },
                                content: {
                                    first_page_is_fullside: v.firstPageIsFullside,
                                    last_page_is_fullside : v.lastPageIsFullside
                                }
                            },
                        }];
                    }

                    productInfo.uploadType += 'r';
                }

                if (productInfo.customOptions){
                    CSLibrary.evalScript(`setDocumentLabel('customOptions', '${JSON.stringify(productInfo.customOptions)}')`);
                }else{
                    CSLibrary.evalScript(`setDocumentLabel('customOptions', '')`);
                }

                productInfo.metaData = {
                    ps_codes               : v.psCodes,
                    cell_movable           : v.cell_movable,
                    sticker_movable        : v.sticker_movable,
                    sticker_selectable     : v.sticker_selectable,
                    text_movable           : v.text_movable,
                    text_selectable        : v.text_selectable,
                    rsc_type               : v.rsc_type,
                    userDiv                : v.user.profile.div,
                    userLang               : v.user.profile.lang,
                    backgroundType         : v.backgroundType
                }
                
                if (productInfo.metaData){
                    CSLibrary.evalScript(`setDocumentLabel('metaInfo', '${JSON.stringify(productInfo.metaData)}')`);
                }
                    console.log("productInfo", productInfo)
                    CSLibrary.evalScript("exportToData("+JSON.stringify(productInfo)+")", function(path){
                            console.log("path!!!",path)
                            if (path !== "EvalScript error."){
                                v.templatesPath = path;
                                if (cb){
                                    cb();
                                }
                            }
                    });
            },
            // 프리뷰를 위한 데이터 저장
            savePreviewData : function(){
                
                var v = this;
                var templateDirs = cep.fs.readdir(v.templatesPath).data.sort();
                // 템플릿 폴더 불러와 로딩...
                for(var j=0; j<templateDirs.length;j++){

                    var templateDir = v.templatesPath+"/"+templateDirs[j];
                    var filePath = templateDir+"/template.json";
                    var pageIdList = JSON.parse(cep.fs.readFile(templateDir+'/template.json').data).pages.map(function(page){

                        return page.id
                    });

                    var metaData = templateDir+"/metadata.json";

                    previewDoc = {
                        pageIdList   : pageIdList,
                        templatePath : filePath,
                        templateDir  : templateDir,
                        metadata     : metaData
                    }

                    // 성공시 preview를 위한 데이터 저장 ( Local )
                    v.previewInfo.push(previewDoc);
                }
            },
            // 운영서버에 업로드가 완료된 원본파일인지, 아닌지를 판단후 다음 프로세스를 진행한다.
            validateIndesignFile : function(e){

                var v = this;
                v.saveTemplateInfo();

                CSLibrary.evalScript("getDocumentLabel('isProduction')", function(value){
                    v.saveTemplateInfo();
                    console.log("isProduction",value)
                    if (value === 'Y'){
                        if (confirm("운영서버로 업로드합니다 실행하시겠습니까? ") ==  true){
                            //운영서버에 업로드가 완료된 템플릿 -> 운영서버에만 업로드 하도록 한다
                            if (v.targetServer !== "ops"){
                                v.targetServer = "ops";
                                v.setTargetServer();
                                iziToast.warning({message : '해당 파일은 운영서버에만 업로드가 가능하므로 운영서버로 강제 변경 됩니다.'});
                            }
                        }else{
                            return false;
                        }
                    }
                    v.uploadTemplates();
                })
            },
            // 최종 승인 완료된 템플릿 서버로 업로드
            uploadTemplates : function(){
                var v = this;
                v.saveTemplateInfo();

                //변환작업 건너띄기 테스트 v.templatesPath = "/Users/kraken/Downloads/54x94 Layout"
                //v.templatesPath = "/Users/kraken/Downloads/54x94 Layout"
                var templateDirs = cep.fs.readdir(v.templatesPath).data.sort().filter((folder)=> folder !== '.DS_Store');

                console.log("uploadTemplates02", templateDirs)
                var isComplatePopup = false;
                showLoading();
                document.getElementById('uploadStateText').innerHTML = "템플릿업로드 진행중";

                var uploadQueue = [];

                // 템플릿 폴더별 업로드
                for(var j=0; j<templateDirs.length;j++){

                    var templateForm = new FormData();
                    var variableItemList = [];

                    var templateDir = v.templatesPath+"/"+templateDirs[j];
                    var templateFiles = cep.fs.readdir(templateDir).data.sort();

                    var isOverSize = isOverLimitSize(templateDir);
                    console.log("templateDir",templateDir);
                    console.log("templateFiles", templateFiles);

                    if (!isOverSize){

                        // 해당 템플릿 폴더의 모든 파일을 추출
                        for(var i=0; i<templateFiles.length; i++){

                            var fileName = templateFiles[i];
                            var filePath = templateDir+"/"+fileName;

                            if (fileName === "variableItemList.json"){
                                var json_str = cep.fs.readFile(filePath, cep.encoding.UTF8).data;
                                variableItemList = JSON.parse(json_str);
                            }else{
                                var base64 = cep.fs.readFile(filePath, cep.encoding.Base64).data;
                                var file = base64ToFile(base64, fileName);

                                // 템플릿 정보 등록
                                if (fileName === "template.json"){
                                    templateForm.append("doc", file);
                                // 템플릿 메타 데이터 등록
                                }else if(fileName === "metadata.json"){
                                    templateForm.append("meta", file);
                                // 템플릿 dp 썸네일 등록
                                }else if(fileName.includes("template-dp")){
                                    var file = base64ToFile(base64, "template-dp.png");
                                    templateForm.append("dp", file);
                                }else if(fileName.includes("page-0")){
                                    var file = base64ToFile(base64, "page-0.png");
                                    templateForm.append("dp", file);
                                // 나머지 리소스 처리
                                }else{
                                    // 레이아웃 dp 썸네일 등록
                                    if(fileName.includes("dp")){
                                        templateForm.append("dp", file);
                                    // 리소스 이미지 파일 등록 처리
                                    }else{
                                        templateForm.append("res", file);
                                    }
                                }

                                // 썸네일 파일들을 업로드 하기 위해 큐 정보에 담는다.
                                if (fileName.includes("template-dp")){
                                    console.log("thumbnails",filePath)
                                    templateForm.append("thumbnails", filePath);
                                }
                                base64 = null;
                                file = null;
                            }
                        }
                    }

                    // 템플릿 서버 전송
                    // 토큰을 해당 Template.json에 설정된 거에서 추출해서 사용한다.
                    var templateJSON = JSON.parse(cep.fs.readFile(templateDir+'/template.json').data);
                    var tokenString  = templateJSON.token;
                    var tokenName    = templateDir+"/token.jwt";
                    cep.fs.writeFile(tokenName, tokenString);

                    // 설정된 토큰 파일 토대로 해당 파일을 form에 업로드 하기 위해 File의 형태로 전환
                    var tokenFile = base64ToFile(cep.fs.readFile(tokenName, cep.encoding.Base64).data, "token.jwt");
                    templateForm.append("token", tokenFile);
                    templateForm.append("rsc_type", v.rsc_type);

                    var templateDoc = {
                        token : tokenString,
                        templatePath : templateDir+"/"+"template.json",
                        templateDir  : templateDir,
                        index        : j
                    }

                    // variableItemList ( 연동아이템 ) 존재시 도큐먼트 세팅
                    if (variableItemList.length > 0){
                        templateDoc.variableItemList = variableItemList;
                    }

                    var queue = {
                        form : templateForm || null,
                        doc  : templateDoc,
                        isOverSize : isOverSize,
                        templateJSON : templateJSON
                    }

                    //컷 개별설정시 uploadQueueProcess에 SizeInfo를 보내기위한 obj
                    var individualSizeObj = {
                        "individualSize" : false,
                        "width"          : "",
                        "height"         : ""       
                    }

                    // RenderBound 가 존재할경우 실제 PDF 결과물 사이즈 정보를 얻는다.
                    var pageRenderBounds = templateJSON.pages.reduce((result ,page)=>{
                        //컷사이즈 개별설정 체크
                        for (key of page.page_items){
                            if(key.layer === "Cut" || key.layer === "Cut@{render:false}" || key.layer.split('@')[0] == "Cut"){
                                if(key.frame_abs.size.forceSetting === true){
                                    individualSizeObj.individualSize = key.frame_abs.size.forceSetting
                                    individualSizeObj.width  = key.frame_abs.size.individual_width
                                    individualSizeObj.height = key.frame_abs.size.individual_height
                                }else{
                                    individualSizeObj.individualSize = false
                                }
                            }
                        }

                        var renderBound = page.page_items.filter( (item)=>item.layer === "RenderBound" );  // 없으면 빈배열
                        const multiRenderBounds = page.page_items.filter(item => item.layer === "MultiRenderBound");  // 없으면 빈배열

                        //렌더바운드 없고 컷사이즈 개별 설정시 1순위 적용
                        if (individualSizeObj.individualSize === true && !renderBound.length && !multiRenderBounds.length) {
                            //컷사이즈 개별설정시
                            var sizeInfo = {
                                workWidth  : v.decimalPointSplit(v.selectSize.pageInfos[0].width_mm),
                                workHeight : v.decimalPointSplit(v.selectSize.pageInfos[0].height_mm),
                                cutWidth   : v.decimalPointSplit(individualSizeObj.width),
                                cutHeight  : v.decimalPointSplit(individualSizeObj.height), 
                                type       : "individual Size",
                                useTemplateSize : true
                            }
                            result.push(sizeInfo);

                        } else if (renderBound.length > 0 || multiRenderBounds.length > 0) {
                            let renderBoundData = renderBound.length > 0 ? renderBound : multiRenderBounds;
                            let multiRenderBoundObj = [];
                            for (let i = 0; i < renderBoundData.length; i++) {
                                //Cut사이즈 개별 설정시 1순위 적용
                                if (individualSizeObj.individualSize === true){
                                    var sizeInfo = {
                                        workWidth  : v.decimalPointSplit(renderBoundData[i].frame.size.width),
                                        workHeight : v.decimalPointSplit(renderBoundData[i].frame.size.height),
                                        cutWidth   : v.decimalPointSplit(individualSizeObj.width),
                                        cutHeight  : v.decimalPointSplit(individualSizeObj.height), 
                                        type       : "individual Size",
                                        useTemplateSize : true
                                    }
                                //Cut사이즈 강제설정시 2순위 적용
                                }else if(v.importantCutSizeWidth && v.importantCutSizeHeight){
                                    var sizeInfo = {
                                        workWidth  : v.decimalPointSplit(renderBoundData[i].frame.size.width),
                                        workHeight : v.decimalPointSplit(renderBoundData[i].frame.size.height),
                                        cutWidth   : v.decimalPointSplit(v.importantCutSizeWidth),
                                        cutHeight  : v.decimalPointSplit(v.importantCutSizeHeight), 
                                        type       : "All sizes",
                                        useTemplateSize : true
                                    }
                                //Cut사이즈 개별,강제 설정이 없을시 3순위 적용
                                }else{
                                    var sizeInfo = {
                                        workWidth  : v.decimalPointSplit(renderBoundData[i].frame.size.width),
                                        workHeight : v.decimalPointSplit(renderBoundData[i].frame.size.height),
                                        cutWidth   : v.decimalPointSplit(renderBoundData[i].frame.size.width),
                                        cutHeight  : v.decimalPointSplit(renderBoundData[i].frame.size.height), 
                                        type       : renderBound.length > 0 ?  "RenderBound Size" : "MultiRenderBound Size",
                                        useTemplateSize : true
                                    }
                                }

                                var cut = page.page_items.find((item)=>item.layer === "Cut" || item.layer === "Cut@{render:false}" || item.layer.split('@')[0] == "Cut");

                                //Cut사이즈 개별설정시 1순위 적용
                                if(individualSizeObj.individualSize === true){
                                    sizeInfo.cutWidth  =  individualSizeObj.width;
                                    sizeInfo.cutHeight =  individualSizeObj.height;
                                //Cut사이즈 전체설정시 2순위 적용    
                                }else if(v.importantCutSizeWidth && v.importantCutSizeHeight){
                                    sizeInfo.cutWidth  =  v.decimalPointSplit(v.importantCutSizeWidth);
                                    sizeInfo.cutHeight =  v.decimalPointSplit(v.importantCutSizeHeight);
                                }else{
                                    //컷레이어 존재시 컷사이즈 3순위 적용
                                    if(cut){
                                        sizeInfo.cutWidth  =  v.decimalPointSplit(cut.frame.size.width);
                                        sizeInfo.cutHeight =  v.decimalPointSplit(cut.frame.size.height);
                                    //컷레이어 없을시 에디쿠스값 적용 4순위     
                                    }else {
                                        sizeInfo.cutWidth  =  v.decimalPointSplit(sizeInfo.workWidth)  - (v.selectSize.pageInfos[0].cutMargin_mm.width  * 2);
                                        sizeInfo.cutHeight =  v.decimalPointSplit(sizeInfo.workHeight) - (v.selectSize.pageInfos[0].cutMargin_mm.height * 2);
                                    }
                                }
                                if(renderBoundData.length != 1) {
                                    multiRenderBoundObj.push(sizeInfo);
                                    i == renderBoundData.length - 1 ? result.push(multiRenderBoundObj) : null;
                                } else {
                                    result.push(sizeInfo);
                                }
                            }
                        }
                        return result;
                    }, []);
                    
                    if (pageRenderBounds.length > 0){
                        queue.sizeInfo = pageRenderBounds;
                    }

                    uploadQueue.push(queue);
                }
                v.uploadTemplateLength = uploadQueue.length;
                v.uploadQueueProcess(uploadQueue, 1);
            },

            decimalPointSplit : function(val){
                var v = this;

                //소수점이 존재할 경우
                if(isNaN(val) == false && Number.isInteger(val)==false){    
                    //var str = parseFloat(val).toFixed(v.allowDecimalPoint);
                    var str = parseFloat(val).toFixed(0);
                    var retval = Number(str);
                    return retval;    
                }else{
                    return val
                }

                // // isInteger는 es6 임. ie 11 에서는 안되므로 함수 만듬.
                // Number.isInteger = Number.isInteger || function(value) {
                //     return typeof value === "number" && 
                //         isFinite(value) && 
                //         Math.floor(value) === value;
                // };
                
            },

            // 에디쿠스 상품 카테고리 가져오기
            getEdicusProductList : function(){
                var v = this;

                RestAPI.getProductList(function(err ,result){
                    var productList     = []
                    var categoriesList  = []
 
                    
                    //카테고리,상품리스트 분리 및 운영서버시 TEST카테고리,상품 삭제
                    for(key in result){
                        if(key === "products"){
                            for(keys of result[key]){
                                if(v.targetServer === "ops"){
                                    if(keys.cateCode != "TEST"){
                                        productList.push(keys)
                                    }
                                }else{
                                    productList.push(keys)
                                }
                            }
                        }else if(key === "prodCates"){
                            for(keys of result[key]){
                                if(v.targetServer === "ops"){
                                    if(keys.cateCode != "TEST"){
                                        categoriesList.push(keys)
                                    }
                                }else{
                                    categoriesList.push(keys)
                                }
                            }
                        }
                    }

                    if (err === null){
                            var prodArray = []
                            var cateArray = []
                            var user = v.user
                            //div로 상품리스트 매칭 null이면 divCodeMaster로 매칭
                            var div = ""

                            if(user.profile.div)                 div = user.profile.div
                            else if(user.profile.divCode_master) div = user.profile.divCode_master
                            cateArray.push( {cateCode: "none", dpName:"카테고리선택"})
                            //user allowDivisionList 또는 user divCode_master 와 product div 매칭되는 목록 리스트업
                            if(div){
                                for(let key of productList){
                                    let allowDivision = ""
                                    if(key.userData){
                                        allowDivisionList = JSON.parse(key.userData).allowDivisionList
                                        if(allowDivisionList){
                                            allowDivision = allowDivisionList.indexOf(div)
                                        }
                                    }
                                    if(div === key.divCode_master || parseInt(allowDivision) >= 0){
                                        prodArray.push(key)
                                    }
                                }

                                for(let key of prodArray){
                                    for(keys of categoriesList){
                                        if(keys.cateCode === key.cateCode){
                                            cateArray.push(keys)
                                        }
                                    }
                                }
                            }

                            //카테고리명 중복제거 후 재배열 
                            //var uniqueCategories = v.removeDuplicates(cateArray, "cateCode");
                            var cateArray = cateArray.reduce(( first, second ) => {
                                if(first.indexOf(second) < 0) first.push(second);
                                return first;
                            },[]);

                        v.categories = cateArray;
                        v.products   = prodArray;

                        v.checkTemplateInfo();
                        v.checkServerStatus();

                    }else{
                        console.log(err);
                    }
                });
            },

            //중복제거
            removeDuplicates : function(originalArray, prop) {
                var newArray = [];
                var lookupObject  = {};

                for(var i in originalArray) {
                   lookupObject[originalArray[i][prop]] = originalArray[i];
                }

                for(i in lookupObject) {
                    newArray.push(lookupObject[i]);
                }
                 return newArray;
            },

            // 현재 활성 페이지의 텍스트 리스트 불러오기
            getTextList : function(){
                var v = this;
                CSLibrary.evalScript("getCurrentPageText()", function(result){
                    if (result !== "EvalScript error."){
                        var textList = JSON.parse(result);
                        v.textList = textList;
                    }

                });
            },
            // 현재 활성 페이지의 텍스트 정보 저장
            setTextList : function(){
                var v = this;

                var script = "setCurrentPageText("+JSON.stringify(v.textList)+")";
                CSLibrary.evalScript(script);
            },
            // 해당 페이지의 토큰 유무 확인 후 없는 페이지에는 새로운 토큰을 할당한다.
            setTemplateTokens : function(cb){
                var v = this;

                CSLibrary.evalScript("checkHasToken()", function(result){  // result = 토큰 없는 page.id

                    var emptyTokenPages = JSON.parse(result);
                    emptyTokenPages.forEach(function(pageId){
                        var token = RestAPI.generateTemplateToken();
                        var script = "setPageToken(app.activeDocument.pages.itemByID("+pageId+"), '"+token+"')";
                        CSLibrary.evalScript(script);
                    });
                    // Token Setting 완료 후 호출
                    cb();
                    v.saveTemplateInfo();
                    
                });

            },
            // 미리보기 ( 업로드 자동화 )
            convertNpreivew : function(e){
                var v = this;
                v.previewInfo = [];
                var applyVariableOptionList = {
                    'typeA' : ['Pencil','GSPNDFT','GSPNBAL','BALLPEN'], // 모든text와 sticker에 variable ID를 적용한다.(commonSticker, commonText)
                    'typeB' : ['TPSTNME','NAMESTICKER'] // // Text의 순서대로 variable Id를 지정한다. 1~
                }

                for(key in applyVariableOptionList){
                    if(applyVariableOptionList[key].indexOf(v.selectProduct.prodCode) > -1){
                        v.variableType = key
                        CSLibrary.evalScript("getAllPageItems('"+key+"')", function(result){});
                    }
                }
                                
                if(v.importantCutSizeWidth && v.importantCutSizeHeight){
                    CSLibrary.evalScript(`setDocumentLabel('importantCutSizeWidth',  '${v.importantCutSizeWidth}')`);
                    CSLibrary.evalScript(`setDocumentLabel('importantCutSizeHeight', '${v.importantCutSizeHeight}')`);
                }else if ((v.importantCutSizeWidth && !v.importantCutSizeHeight) || (!v.importantCutSizeWidth && v.importantCutSizeHeight)) {
                    v.indesignAlert("[오류] 컷사이즈는 Width, Height 모두 입력되어야 합니다.");
                    return false;
                }
                
                v.setTemplateTokens(function(){
                    v.convertToTemplate(function(){
                        v.savePreviewData();
                        v.openPreviewer();
                        // CSLibrary.evalScript("test()", function(value){
                        //     console.log("this.Value=",value)    
                        // });
                    });
                });
            },
            // 이미지 뷰어 오픈
            openPreviewer : function(){
                CSLibrary.requestOpenExtension("kr.co.RedPrinting.Previewer");
                var event = new CSEvent("kr.co.RedPrinting.Previewer", "APPLICATION");
                event.date = "test data";
                
                CSLibrary.dispatchEvent(event);
            },
            // 커스텀 JSX 로드
            customJSXLoad : function(){
                CSLibrary.evalScript("loadJSX()", function(a){
                    var script = '$.evalFile("' + a + '");';
                    CSLibrary.evalScript(script);
                });
            },
            // 커스텀 ExtendScript Call
            callExtendScript : function(){
                var v = this;
                var script  = v.extendScript;
                var a = script.split(")");
                var info = {
                    product : v.selectProduct,
                    size    : v.selectSize,
                    type    : function(){
                        if (v.uploadType=="S"){
                            return "single template"
                        }else if (v.uploadType=="D"){
                            return "doubleside template"
                        }else{
                            return "multipage template"
                        }
                    }()
                }
                if ( a[0].charAt(a[0].length-1) == "(" ){
                    script = a[0]+ JSON.stringify(info) +")";
                }else{
                    script = a[0]+","+ JSON.stringify(info) +")";
                }

                CSLibrary.evalScript(script);
            },
            indesignAlert : function(alertText){
                var v = this
                CSLibrary.evalScript("sendMessageToEndUser('"+alertText+"')", function(g){console.log(g)});
            },
            
            uploadQueueProcess : function(queues, index){
                
                var v = this;
                var queue  = queues.shift();
                console.log("PageNum",index)
                if (queue){
                    if (!queue.isOverSize){
                        //로딩
                        RestAPI.uploadTemplates(queue.form,  queue.doc).then(async function(uploadResult){
                                var templateInfo                = uploadResult;
                                templateInfo.productCode        = v.selectProduct.prodCode;
                                templateInfo.productName        = v.selectProduct.dpName;
                                templateInfo.sizeCode           = v.selectSize.sizeCode;
                                templateInfo.sizeName           = v.selectSize.dpName;
                                templateInfo.pageInfo           = v.selectSize.pageInfos;
                                templateInfo.psCode             = v.selectSize.sizeCode+"@"+v.selectProduct.prodCode
                                templateInfo.psCodes            = v.psCodes;
                                templateInfo.category           = v.selectProduct.cateCode;
                                templateInfo.categoryName       = v.selectCategory.dpName;
                                // if (v.selectProduct.userData){
                                //     Object.keys(JSON.parse(v.selectProduct.userData)).map(function(key){
                                //         if (JSON.parse(v.selectProduct.userData)[key][v.user.profile.div]){
                                //             console.log("key!=",key)
                                //             console.log("userData",JSON.parse(v.selectProduct.userData))   
                                //             templateInfo[key] = JSON.parse(v.selectProduct.userData)[key][v.user.profile.div]
                                //         }
                                //     })

                                // }else{
                                //     templateInfo.productCustomData = [];
                                // }
                                templateInfo.PicFrameWithoutPic  = v.PicFrameWithoutPic;
                                templateInfo.allowDecimalPoint   = v.allowDecimalPoint 
                                templateInfo.whiteVisible        = v.whiteVisible 
                                templateInfo.firstPageIsFullside = v.firstPageIsFullside 
                                templateInfo.lastPageIsFullside  = v.lastPageIsFullside 
                                templateInfo.cell_movable       = v.cell_movable;
                                templateInfo.sticker_movable    = v.sticker_movable;
                                templateInfo.sticker_selectable = v.sticker_selectable;
                                templateInfo.text_movable       = v.text_movable;
                                templateInfo.text_selectable    = v.text_selectable;
                                templateInfo.rsc_type           = v.rsc_type;
                                templateInfo.owner              = v.user._id;
                                templateInfo.ownerName          = v.user.profile.name
                                templateInfo.ownerType          = ( v.user.profile.div !== null && v.user.profile.accountType !== "Seller" ) ? "COMPANY" : "INDIVIDUAL"
                                templateInfo.company            = v.user.profile.company;
                                templateInfo.pageType           = v.uploadType;
                                templateInfo.thumbnails         = [];
                                templateInfo.license            = v.license;
                                //템플릿정보에 회사코드 추가 -20181221 craken
                                templateInfo.div                = v.user.profile.div;
                                templateInfo.divCode_master     = v.user.profile.divCode_master;
                                templateInfo.accountType        = v.user.profile.accountType;
                                templateInfo.programVersion     = v.programVersion;
                                // background 확장자 추가 -20200827 yujuck
                                templateInfo.backgroundType     = v.backgroundType;
 
                                //에디쿠스의 작업사이즈가 0이라면 frame 사이즈를 sizeInfo 넘겨줌 -KRAKEN
                                // Free Size 검사 분기가 정상적으로 동작하지 않고 ( 상품코드 분기 오류 ) 가독성이 떨어져 코드 수정 - 20204021 Irelander_kai
                                const excludeFreeSizeList = ["FBPODFT", "PHFRACR", "FBPODFT", "PHFRPAP", "TPRNBND", "TPTKBND"];
                                if (  (parseInt(v.selectSize.pageInfos[0].width_mm) === 0 && parseInt(v.selectSize.pageInfos[0].height_mm) === 0) && !excludeFreeSizeList.includes(v.selectProduct.prodCode) ){
                                    templateInfo.sizeInfo = queue.templateJSON.pages.map(function(page){
                                        var doc = {
                                            workWidth       : v.decimalPointSplit(page.frame.size.width),
                                            workHeight      : v.decimalPointSplit(page.frame.size.height),
                                            cutWidth        : v.decimalPointSplit(page.frame.size.width)  - (v.selectSize.pageInfos[0].cutMargin_mm.width * 2), 
                                            cutHeight       : v.decimalPointSplit(page.frame.size.height) - (v.selectSize.pageInfos[0].cutMargin_mm.height * 2),
                                            useTemplateSize : true,
                                            type            : "Free Sizes"
                                        }
                                        return doc
                                    })
                                // 실제 작업 사이즈 정보 추가 for RenderBound ( 실제 사이즈 정보 스키마 통일 ) 20190219 Irelander_kai    
                                }else if (queue.sizeInfo){
                                    templateInfo.sizeInfo       = queue.sizeInfo
                                }else{
                                    templateInfo.sizeInfo       = v.selectSize.pageInfos.reduce(function(result, page){
                                        if(v.importantCutSizeWidth && v.importantCutSizeHeight){
                                            var doc = {
                                                workWidth       : v.decimalPointSplit(page.width_mm),
                                                workHeight      : v.decimalPointSplit(page.height_mm),
                                                cutWidth        : v.importantCutSizeWidth, 
                                                cutHeight       : v.importantCutSizeHeight,
                                                useTemplateSize : true,
                                                type            : "All Sizes"
                                            }
                                            
                                        }else{
                                            var doc = {
                                                workWidth       : v.decimalPointSplit(page.width_mm),
                                                workHeight      : v.decimalPointSplit(page.height_mm),
                                                cutWidth        : v.decimalPointSplit(parseInt(page.width_mm  - (parseInt(page.cutMargin_mm.width) *2))),
                                                cutHeight       : v.decimalPointSplit(parseInt(page.height_mm - (parseInt(page.cutMargin_mm.height) *2))),
                                                useTemplateSize : false,
                                                type            : "Edicus Sizes"
                                            }   
                                        }

                                        result.push(doc);
                                        return result;
                                    }, []);
                                }

                                delete templateInfo["doc"];
                                    
                                var thumbnails = queue.form.getAll("thumbnails");
                                var keyValue = `Edicus/thumbnails/${v.user.profile.div}/${templateInfo.category}/${templateInfo.productCode}/${generateUUID()}.png`
                                for (var i=0; i < thumbnails.length; i++){
                                    var s3Config = createS3Setting(thumbnails[i], keyValue, null, "redprintingweb.cloudfront");
                                    await uploadFileToS3(s3Config, true).then(function(url){templateInfo.thumbnails.push(url)});
                                }
                                
                                return RestAPI.addTemplateInfo(templateInfo)

                        }).then(function(templateUUID){
                            var percent       = index/v.uploadTemplateLength*100;
                            var targetStyle   = document.querySelector("#progress").style;

                            targetStyle.width = percent+"%";
                            v.templateUUIDs.push(templateUUID);
                            v.uploadQueueProcess(queues, index+1);
                        }).catch(function(error){
                            v.indesignAlert('업로드 도중 에러 : '+ error.message);
                            console.log('업로드 도중 에러1', error);
                            v.uploadErrorState = true;
                            endLoading();
                        });

                        //32mb이상 압축해서 파일넘기고 넘으면 url 변경 
                    }else {

                        RestAPI.getBulkUploadUrl().then(async function(uploadInfo){

                            console.log("대용량 이미지 압축중");
                            var zipFile = await generateZipFile(queue.doc.templateDir, v.templatesPath, queue.doc.index);
                            console.log("대용량 이미지 성공");

                            await RestAPI.bulkFileUpload(uploadInfo.upload_url, zipFile);
                            return RestAPI.enrollBulkUpload(uploadInfo.filename, queue.doc);

                        }).then(async function(uploadResult){

                            var templateInfo = uploadResult;
                                templateInfo.productCode        = v.selectProduct.prodCode;
                                templateInfo.productName        = v.selectProduct.dpName;
                                templateInfo.sizeCode           = v.selectSize.sizeCode;
                                templateInfo.sizeName           = v.selectSize.dpName;
                                templateInfo.pageInfo           = v.selectSize.pageInfos;
                                templateInfo.psCode             = v.selectSize.sizeCode+"@"+v.selectProduct.prodCode
                                templateInfo.psCodes            = v.psCodes;
                                templateInfo.category           = v.selectProduct.cateCode;
                                templateInfo.categoryName       = v.selectCategory.dpName;
                                // if (v.selectProduct.userData){
                                //     Object.keys(JSON.parse(v.selectProduct.userData)).map(function(key){
                                //         if (JSON.parse(v.selectProduct.userData)[key][v.user.profile.div]){
                                //             templateInfo[key] = JSON.parse(v.selectProduct.userData)[key][v.user.profile.div]
                                //         }
                                //     })
                                // }else{
                                //     templateInfo.productCustomData = [];
                                // }
                                templateInfo.PicFrameWithoutPic   = v.PicFrameWithoutPic;
                                templateInfo.allowDecimalPoint    = v.allowDecimalPoint 
                                templateInfo.whiteVisible         = v.whiteVisible 
                                templateInfo.firstPageIsFullside  = v.firstPageIsFullside
                                templateInfo.lastPageIsFullside   = v.lastPageIsFullside
                                templateInfo.cell_movable       = v.cell_movable;
                                templateInfo.sticker_movable    = v.sticker_movable;
                                templateInfo.sticker_selectable = v.sticker_selectable;
                                templateInfo.text_movable       = v.text_movable;
                                templateInfo.text_selectable    = v.text_selectable;
                                templateInfo.rsc_type           = v.rsc_type;
                                templateInfo.owner              = v.user._id;
                                templateInfo.ownerName          = v.user.profile.name
                                templateInfo.ownerType          = ( v.user.profile.div !== null && v.user.profile.accountType !== "Seller" ) ? "COMPANY" : "INDIVIDUAL"
                                templateInfo.company            = v.user.profile.company;
                                templateInfo.pageType           = v.uploadType;
                                templateInfo.thumbnails         = [];
                                templateInfo.license            = v.license;
                                //템플릿정보에 회사코드 추가 -20181221 craken
                                templateInfo.div                = v.user.profile.div;
                                templateInfo.divCode_master     = v.user.profile.divCode_master;
                                templateInfo.accountType        = v.user.profile.accountType;
                                templateInfo.programVersion     = v.programVersion;
                                // background 확장자 추가 -20200827 yujuck
                                templateInfo.backgroundType     = v.backgroundType;
                                
                                // 실제 작업 사이즈 정보 추가 for RenderBound ( 실제 사이즈 정보 스키마 통일 ) 20190219 Irelander_kai
                                if (queue.sizeInfo){
                                    templateInfo.sizeInfo       = queue.sizeInfo
                                }else{
                                    templateInfo.sizeInfo       = v.selectSize.pageInfos.reduce(function(result, page){
                                        if(v.importantCutSizeWidth && v.importantCutSizeHeight){
                                            var doc = {
                                                workWidth       : v.decimalPointSplit(page.width_mm),
                                                workHeight      : v.decimalPointSplit(page.height_mm),
                                                cutWidth        : v.importantCutSizeWidth, 
                                                cutHeight       : v.importantCutSizeHeight,
                                                useTemplateSize : true,
                                                type            : "All Sizes"
                                            }
                                        }else{
                                            var doc = {
                                                workWidth       : v.decimalPointSplit(page.width_mm),
                                                workHeight      : v.decimalPointSplit(page.height_mm),
                                                cutWidth        : v.decimalPointSplit(parseInt(page.width_mm  - (parseInt(page.cutMargin_mm.width) *2))),
                                                cutHeight       : v.decimalPointSplit(parseInt(page.height_mm - (parseInt(page.cutMargin_mm.height) *2))),
                                                useTemplateSize : false,
                                                type            : "Edicus Sizes"
                                            }   
                                        }

                                        result.push(doc);
                                        return result;
                                    }, []);
                                }

                            delete templateInfo["doc"];

                            var thumbnails = queue.form.getAll("thumbnails");
                            var keyValue = "Edicus/thumbnails/"+templateInfo.category+"/"+templateInfo.productCode+"/"+generateUUID()+".png"

                            for (var i=0; i < thumbnails.length; i++){
                                var s3Config = createS3Setting(thumbnails[i], keyValue);
                                await uploadFileToS3(s3Config).then(function(url){templateInfo.thumbnails.push(url)});
                            }

                            return RestAPI.addTemplateInfo(templateInfo)

                        }).then(function(templateUUID){

                            var percent = index/v.uploadTemplateLength*100;
                            var targetStyle = document.querySelector("#progress").style;

                            targetStyle.width = percent+"%";
                            v.templateUUIDs.push(templateUUID);
                            v.uploadQueueProcess(queues, index+1);
                        }).catch(function(error){
                            v.indesignAlert('업로드 도중 에러 : '+ error.message);
                            console.log('업로드 도중 에러2', error);
                        });

                    }

                }else{
                    // 운영일때만
                    if (v.targetServer === 'ops'){
                        
                        // 운영에 업로드 된 템플릿 플레그 값 세팅
                        CSLibrary.evalScript("setDocumentLabel('isProduction', 'Y')", function(){
                            // 운영에 올라간 원본인지 , 스플릿된 개별 파일인지 판단 후 다음 프로세스 진행
                            CSLibrary.evalScript("getDocumentLabel('isSplitPage')", function(value){
                                if (value === "Y"){
                                    v.uploadSplitTemplate(false) // 현재 도큐먼트 업로드...
                                }else{
                                    console.log("스플릿 실행")
                                    v.splitPageToDocument(); // 원본이므로 템플릿 스플릿
                                }
                            });
                        })
                    }else{

                        //테스트서버에서는 스플릿된 개별 파일인지 판단만 한 후 다음 프로세스 진행
                        // CSLibrary.evalScript("getDocumentLabel('isSplitPage')", function(value){
                        //     if (value === "Y"){
                        //         console.log("isSplitPage:Y")
                        //         v.uploadSplitTemplate(false) // 현재 도큐먼트 업로드...
                        //     }else{
                        //         console.log("isSplitPage:N")
                        //         v.splitPageToDocument(); // 원본이므로 템플릿 스플릿
                        //     }
                        // });

                        iziToast.show({
                            theme: 'dark',
                            icon: 'icon-person',
                            title: '완료',
                            message: '템플릿 업로드가 정상적으로 완료되었습니다.',
                            position: 'topRight',
                            progressBarColor: 'rgb(0, 255, 184)',
                            onOpening: function(instance, toast){
                            },
                            onClosing: function(instance, toast, closedBy){
                                //v.templatesPath = null;
                            }
                        });

                        endLoading();
                        document.querySelector("#progress").style.width = "0%";
                        templateForm = null;
                        v.uploadErrorState = true;
                        v.saveTemplateInfo();

                    }

                }
            },
            // 현재 페이지 복제
            currentPageDuplicate : function(){
                CSLibrary.evalScript("currentPageDuplicate()", function(g){console.log(g)});  // currentPageDuplicate()는 null을 리턴함
            },
            // 새레이어 생성
            createLayerPage : function(){
                var form = new FormData(document.querySelector('#layerForm'));
                var layerName = form.get('layerName');

                if(!layerName){
                    CSLibrary.evalScript("sendMessageToEndUser('레이어명을 입력해주세요.')", function(g){console.log(g)});
                    return false;
                }

                CSLibrary.evalScript("createLayerPage('"+layerName+"')", function(g){console.log(g)});  // createLayerPage()는 null 리턴
            },
            //어노테이션레이어 생성
            createAnotationLayerPage : function(){
                var form = new FormData(document.querySelector('#anotationLayerForm'));
                
                var anotationLangCode   = form.get('anotationLangCode');
                var anotationLayerName  = form.get('anotationLayerName');
                var anotationOption     = form.get('anotationOption');
                

                if(!anotationLayerName){
                    CSLibrary.evalScript("sendMessageToEndUser('레이어명을 선택해주세요.')", function(g){console.log(g)});
                    return false;
                }

                if(anotationOption === "chain:0" || anotationOption === "chain:1"){
                    if(anotationLayerName)  anotationLayerName = anotationLayerName + "@";

                    layerOption = [anotationOption]
                }else{
                    if(anotationLayerName)  anotationLayerName = anotationLayerName + "@";
                    if(anotationLangCode)   anotationLangCode  = "lang:" + anotationLangCode
                    if(anotationOption)     anotationOption    = "redirect:" + anotationOption

                    layerOption = [anotationLangCode,anotationOption]
                }
                
                emptyDelOption = $.grep(layerOption,function(n){ return n == " " || n; }); //빈배열 삭제
                layerOptionRs = anotationLayerName + "{" +emptyDelOption.join(',') + "}" ;

                CSLibrary.evalScript("createLayerPage('"+layerOptionRs+"')", function(g){console.log(g)});

            },
            // 도큐먼트 페이지 스플릿
            splitPageToDocument : function(){
                var v = this;
                v.saveTemplateInfo()
                
                var margins = this.selectSize.pageInfos.filter(function(page){return page.pageType === "content";})[0].cutMargin_mm;
                var docInfo = {
                    pageWidth : parseInt(this.selectSize.pageInfos[0].width_mm),
                    pageHeight : parseInt(this.selectSize.pageInfos[0].height_mm),
                    marginWidth : margins.width,
                    marginHeight : margins.height,
                    type : v.uploadType
                }
                

                //템플릿 스플릿 실행
                if (v.templatesPath){
                    CSLibrary.evalScript("splitPageToDocument('"+v.templatesPath+"', "+JSON.stringify(docInfo)+", "+JSON.stringify(v.templateUUIDs)+")", function(err){
                        v.templateUUIDs = [];
                        v.uploadSplitTemplate(v.templatesPath+"/splitTemplate")
                        
                    });
                }
            },
            // 새로운 도큐먼트 생성
            createDocument : function(){
                try {
                    var margins = this.selectSize.pageInfos.filter(function(page){return page.pageType === "content";})[0].cutMargin_mm;
                }catch (e){
                    iziToast.error({
                        title: 'Error',
                        message: '사이즈 정보가 선택되지 않았습니다.',
                    });
                }finally{
                    if (sizes){
                        var options = {
                            pageWidth : parseInt(this.selectSize.pageInfos[0].width_mm),
                            pageHeight : parseInt(this.selectSize.pageInfos[0].height_mm),
                            marginWidth : margins.width,
                            marginHeight : margins.height,
                            processLayers : function(){
                                var layers = [];
                                if (document.querySelectorAll('input[name="processLayers"]:checked')){
                                    document.querySelectorAll('input[name="processLayers"]:checked').forEach(function(layer){
                                        layers.push(layer.value);
                                    });
                                }
                                return layers;
                            }()
                        };

                        CSLibrary.evalScript("createNewDocument("+JSON.stringify(options)+")");
                    }
                }
            },
            // 템플릿 정보 도큐먼트에 저장 ( 업로드 후 해당 도큐먼트에 해당 정보를 설정한다. 스플릿된 페이지에도 함께 해당 정보를 저장한다. )
            saveTemplateInfo : function(){

                var v = this;

                var templateInfo = {
                    server : v.targetServer,
                    category : v.selectCategory,
                    product  : v.selectProduct,
                    size     : v.selectSize,
                    type     : v.uploadType,
                    PicFrameWithoutPic  : v.PicFrameWithoutPic,
                    cell_movable        : v.cell_movable,
                    sticker_movable     : v.sticker_movable,
                    sticker_selectable  : v.sticker_selectable,
                    text_movable        : v.text_movable,
                    text_selectable     : v.text_selectable,
                    rsc_type            : v.rsc_type,
                    license             : v.license,
                    allowDecimalPoint   : v.allowDecimalPoint,
                    whiteVisible        : v.whiteVisible,
                    firstPageIsFullside : v.firstPageIsFullside,
                    lastPageIsFullside  : v.lastPageIsFullside,
                    backgroundType      : v.backgroundType

                    
                };
                CSLibrary.evalScript("saveTemplateInfoToDocument("+JSON.stringify(templateInfo)+")");
            },
            // 해당 도큐먼트에 템플릿 정보가 있는 지 확인한다. & 추가로 분리된 템플릿인지 확인해서 해당 템플릿은 상품의 정보를 바꿀수 없도록한다.
            checkTemplateInfo : function(){
                var v = this;
                CSLibrary.evalScript("getTemplateInfoFromDocument()", function(templateInfo){
                    if (templateInfo !== "EvalScript error."){
                        if (templateInfo){
                            var template  = JSON.parse(templateInfo);
                            if (template.category){
                                v.selectCategory = template.category;
                                setTimeout(function(){
                                    if (template.product){
                                        v.selectProduct = template.product;
                                        
                                        setTimeout(function(){
                                            if (template.size){
                                                v.selectSize = template.size;
                                            }    

                                            v.selectSize = template.size;
                                            var edicusData=[];
                                            // v.products에는 모든 상품이 다 들어있기 때문에 템플릿 상품 prodCode 똑같은것만 가져와서 edicustData 배열변수에 저장함
                                            for(var i = 0; i < v.products.length; i++) {  
                                                if(v.products[i].prodCode == v.selectProduct.prodCode) {
                                                    edicusData = v.products[i].sizes;  // prodCode가 똑같은 상품의 sizes 정보를 저장
                                                }
                                            }

                                            // edicusdata에는 상품의 사이즈 정보 배열이 들어있음 (사이즈 종류가 여러개일 수 있기 때문에 배열에 저장함)
                                            // 길이만큼 반복하면서 가져온 템플릿의 sizeCode는 같지만 에디쿠스의 데이터와 다른 경우에 에디쿠스 데이터로 교체해줌
                                            for(var i = 0; i < edicusData.length; i++) {
                                                if(edicusData[i].sizeCode != template.size.sizeCode) {
                                                    continue;
                                                } else {  // 가져온 템플릿의 사이즈 sizeCode가 똑같은 경우
                                                    var edicusCutMargin = edicusData[i].pageInfos[0].cutMargin_mm;  // 에디쿠스에 저장되어있는 cutMargin 데이터
                                                    var templateCutMargin = template.size.pageInfos[0].cutMargin_mm;  // 가져온 템플릿의 cutMargin 데이터
                                                    var compareCutMargin = (edicusCutMargin.height != templateCutMargin.height) || (edicusCutMargin.width != templateCutMargin.width)  
                                                    var compareWidth = edicusData[i].pageInfos[0].width_mm != template.size.pageInfos[0].width_mm  // width 확인
                                                    var compareHeight = edicusData[i].pageInfos[0].height_mm != template.size.pageInfos[0].height_mm  // height 확인

                                                    if(compareCutMargin || edicusCutMargin.height >= 0 || edicusCutMargin.width >= 0 || compareWidth || compareHeight) {  
                                                        v.selectSize = edicusData[i]  // 에디쿠스의 데이터로 교체시켜줌
                                                    }
                                                }
                                            }

                                            // 기존 코드
                                            // if (template.size){
                                            //     v.selectSize = template.size;
                                            // }
                                        },100)
                                    }
                                },1);
                            }

                            if (template.type){
                                v.uploadType = template.type;
                            }

                            //PickFrame값 가져오기
                            if (template.PicFrameWithoutPic) v.PicFrameWithoutPic = template.PicFrameWithoutPic;
                            //OpenLicense 가져오기
                            if (template.license) v.license = template.license;

                            //소수점자리 허용 값 가져오기
                            if (template.allowDecimalPoint) v.allowDecimalPoint = template.allowDecimalPoint;
                            //화이트레이어 투명여부 값 가져오기
                            if (template.whiteVisible) v.whiteVisible = template.whiteVisible;
                            //내지 첫째,마지막 활성화여부 값 가져오기
                            if (template.firstPageIsFullside) v.firstPageIsFullside = template.firstPageIsFullside;
                            if (template.lastPageIsFullside) v.lastPageIsFullside   = template.lastPageIsFullside;
                            // background 확장자 값 가져오기
                            if (template.backgroundType) v.backgroundType = template.backgroundType;
                            

                        }else{
                            v.saveTemplateInfo();
                        }
                    }
                });

                //이미 변환된 템플릿의 경우 해당 템플릿의 정보를 변경할 수 없도록 한다.
                CSLibrary.evalScript("getDocumentLabel('isSplitPage')", function(value){
                    if (value === "Y"){
                        v.lockTemplateInfo = true;
                    }else{
                        v.lockTemplateInfo = false;
                    }
                })

            },
            // 선택된 오브젝트의 에디터 속성 ( 선택, 이동 등등..)의 설정
            setObjectFeature : function(event){
                target = event.currentTarget;
                var script = `set${target.id.substring(0,1).toLocaleUpperCase() + target.id.substring(1)}All(${target.checked})`;
                var csInterface = new CSInterface();
                csInterface.evalScript(script, function(result) {
                    if(result != "ok") {
                        alert(result);
                    }
                });
            },
            // SPLIT Template UPLOAD
            /**
             *  params
             *
             *  path : 스플릿 페이지의 경로 ( 존재하지 않을 경우 현재 열려있는 문서가 이미 분할된 템플릿 문서이므로 현재 도큐먼트를 업로드 한다.)
             */
            uploadSplitTemplate : function(path){
                //로딩작업
                console.log("Start uploadSplitTemplate")
                var v = this;
                v.saveTemplateInfo()
                var isOriginDocument = (path) ? true : false;
                var templateForm = new FormData();
                var savePath = v.selectProduct.cateCode+"/"+v.selectProduct.prodCode+"/"+v.selectSize.sizeCode;
                var dateObj = new Date();
                var dateFormat = (()=>{
                    return `${dateObj.getFullYear()}${dateObj.getMonth()+1}${dateObj.getDate()}`;
                })();
                templateForm.append("path", savePath);
            
                CSLibrary.evalScript("getCurrentDocumentPath("+isOriginDocument+")", async function(target){  // target = Links 폴더 경로
                    //RestAPI.uploadTemplates(queue.form,  queue.doc).then(async function(uploadResult){
                    var resourceType = ""
                    if(v.rsc_type == "template"){
                        resourceType = "Template"
                    }else if (v.rsc_type == "layout"){
                        resourceType = "Layout"
                    }else if (v.rsc_type == "background"){
                        resourceType = "Background"
                    }
                    
                    
                    if (isOriginDocument){
                        var templateFiles = cep.fs.readdir(path).data.sort();
                        showLoading();
                        document.getElementById('uploadStateText').innerHTML = "파일업로드 진행중";
                        //링크이미지파일 압축하여 서버업로드
                        var uUid = Math.random().toString(36).slice(2)  // link파일의 이름이 되는 부분. ex) k0qi6zhbnmq
                        var zipFile = await generateZipFile(target, v.templatesPath, uUid);  // Links폴더 경로, 인디자인파일경로, 링크파일이름아이디
                        if(!zipFile) {
                            var uploadZipFilePath = false;
                            var targetStyle = document.querySelector("#progress").style; 
                        } else {
                           var keyValue = `Indesign/${resourceType}/${v.user.profile.div}/${v.selectProduct.prodCode}/${dateFormat}/${uUid}.zip`
                            // console.log("keyvalue: ",keyValue);  // Indesign/Template/host/1011-1/2020318/k0qi6zhbnmq.zip
                            var s3LinkConfig = createS3Setting(v.templatesPath+"/"+uUid+".zip", keyValue );
                            var uploadZipFilePath = await uploadFileToS3(s3LinkConfig)
                            var targetStyle = document.querySelector("#progress").style; 
                        }
                        
                        //해당 템플릿 폴더의 모든 파일을 추출
                        for(var i=0; i<templateFiles.length; i++){

                            var fileName = templateFiles[i];
                            var filePath = path+"/"+fileName;
                            var templateId = fileName.replace(".indd", "")
                            var keyValue = `Indesign/${resourceType}/${v.user.profile.div}/${v.selectProduct.prodCode}/${dateFormat}/${fileName}`
                            var s3Config = createS3Setting(filePath, keyValue);
                            console.log("s3Config",s3Config)
                            console.log("keyValue",keyValue)
                            uploadFilePath = await uploadFileToS3(s3Config)

                            if(!uploadZipFilePath) {
                                var doc = {
                                    indesignFilePath : uploadFilePath,
                                    resourceType     : resourceType
                                }
                            }else {
                                var doc = {
                                    indesignFilePath : uploadFilePath,
                                    linkFilePath     : uploadZipFilePath,
                                    resourceType     : resourceType
                                };
                            }
                            await RestAPI.setUpdateFieldData(templateId, doc);

                            var percent = parseInt(i) / parseInt(templateFiles.length) * 100;
                            targetStyle.width = percent+"%";
                        }
                        endLoading();

                    }else{
                        var fullPath = target.split("/");
                        var fileName = fullPath[fullPath.length-1];
                        var token = await getSplitDocumentToken();
                        var keyValue = `Indesign/${resourceType}/${v.user.profile.div}/${v.selectProduct.prodCode}/${dateFormat}/${fileName}`
                        
                        var s3Config = createS3Setting(target, keyValue);
                        
                        uploadFilePath = await uploadFileToS3(s3Config)
                        
                        let doc = {
                            indesignFilePath : uploadFilePath,
                            resourceType     : resourceType
                        };
                        await RestAPI.setUpdateFieldData(null, doc, {token , div : v.user.profile.div });
                    }

                    //RestAPI.uploadSplitTemplates(templateForm).then(function(){

                        iziToast.show({
                            theme: 'dark',
                            icon: 'icon-person',
                            title: '완료',
                            message: '파일 업로드가 정상적으로 완료되었습니다.',
                            position: 'topRight',
                            progressBarColor: 'rgb(0, 255, 184)',
                            onOpening: function(instance, toast){
                            },
                            onClosing: function(instance, toast, closedBy){
                                //v.templatesPath = null;
                            }
                        });
                        v.saveTemplateInfo();
                        endLoading();
                        document.querySelector("#progress").style.width = "0%";
                        templateForm = null;
                        v.uploadErrorState = true;


                    // }).catch(function(err){
                    //     console.log(err);
                    //     templateForm = null;
                    // });


                })
            },


            // ).then(function(templateUUID){

            //     var percent = index/v.uploadTemplateLength*100;
            //     var targetStyle = document.querySelector("#progress").style;

            //     targetStyle.width = percent+"%";
            //     v.templateUUIDs.push(templateUUID);
            //     v.uploadQueueProcess(queues, index+1);
            // }).catch(function(error){

            //     v.indesignAlert('업로드 도중 에러 : '+ error.message);
            //     console.log('업로드 도중 에러1', error);
            //     v.uploadErrorState = true;
            //     endLoading();

            // });


            //멀티사이즈 숨김처리
            multiSizeBox : function(){
                var v = this;

                if (v.multiSizeChoice == false){
                    v.multiSizeChoice = true;
                    toggleState = "visible";
                }else{
                    v.multiSizeChoice = false;
                    toggleState = "hidden";
                }

                [].forEach.call(document.querySelectorAll('.multiSizeChoice'), function (el) {
                    el.style.visibility = toggleState;
                  });

            },

            //개발에서 운영템플릿 업로드시
            checkOpsUploadingFromDevelopment : function(e){
                var v = this;

                CSLibrary.evalScript("getDocumentLabel('isProduction')", function(value){

                    if (value === 'Y' && v.targetServer === "dev"){
                        if (confirm("**주의** KoiExtension 테스트서버에서는 운영중인 상품을 테스트 할 수 없습니다. [OK]클릭시 운영서버로 전환되며 [Cancel]클릭시 토큰이 초기화되며 운영과는 별도의 템플릿으로 변환됩니다.") ==  true){
                            v.targetServer = 'ops';
                            CSLibrary.evalScript("setDocumentLabel('isProduction', 'Y')")
                        }else{

                            v.historyLog(e);
                            //토큰초기화
                            CSLibrary.evalScript("resetAllPageTokens()", function(result){
                                if (result !== "EvalScript error."){
                                    CSLibrary.evalScript("setDocumentLabel('isProduction', 'N')")
                                    CSLibrary.evalScript("setDocumentLabel('isSplitPage', 'N')")
                                    CSLibrary.evalScript("sendMessageToEndUser('Reset Complete')", function(g){console.log(g)});


                                }
                            });
                        }
                    }
                });
            }

        },
        watch : {
            selectCategory : {
                handler : function(){
                    var v = this;
                    v.selectProduct = "상품선택";
                    v.sizes         = [];
                    v.selectSize    = "사이즈선택";
                    v.saveTemplateInfo();
                }
            },
            selectProduct : {
                handler : function(){
                    var v = this;
                    if (v.selectProduct.sizes){
                        v.sizes = v.selectProduct.sizes;
                        v.selectSize = "사이즈선택";
                    } else {
                        v.sizes = [];
                        v.selectSize = "사이즈선택"
                    }
                    v.saveTemplateInfo();
                }
            },
            selectSize : {
                handler : function(){
                    var v = this;
                    v.saveTemplateInfo();

                    var psCode = v.selectSize.sizeCode+'@'+v.selectProduct.prodCode;
                    v.psCodes = [];
                    if (!v.psCodes.includes(psCode)){
                        v.psCodes.push(psCode);
                    }
                }
            },
            uploadType : {
                handler : function(){
                    var v = this;
                    v.saveTemplateInfo();
                }
            },
            targetServer : {
                handler : function(){
                    var v = this;
                    v.setTargetServer(v.targetServer);
                }
            },
            PicFrameWithoutPic : {
                handler : function(){
                    var v = this;
                    v.saveTemplateInfo();
                }
            },
            allowDecimalPoint : {
                handler : function(){
                    var v = this;
                    v.saveTemplateInfo();
                }
            },
            whiteVisible : {
                handler : function(){
                    var v = this;
                    v.saveTemplateInfo();
                }
            },
            firstPageIsFullside : {
                handler : function(){
                    var v = this;
                    v.saveTemplateInfo();
                }
            },
            lastPageIsFullside : {
                handler : function(){
                    var v = this;
                    v.saveTemplateInfo();
                }
            },
            license : {
                handler : function(){
                    var v = this;
                    v.saveTemplateInfo();
                }
            },
            cell_movable : {
                handler : function(){
                    var v = this;
                    v.saveTemplateInfo();
                }
            },
            sticker_movable : {
                handler : function(){
                    var v = this;
                    v.saveTemplateInfo();
                }
            },
            sticker_selectable : {
                handler : function(){
                    var v = this;
                    v.saveTemplateInfo();
                }
            },
            text_movable : {
                handler : function(){
                    var v = this;
                    v.saveTemplateInfo();
                }
            },
            text_selectable : {
                handler : function(){
                    var v = this;
                    v.saveTemplateInfo();
                }
            },
            rsc_type : {
                handler : function(){
                    var v = this;
                    v.saveTemplateInfo();
                }
            },
            backgroundType : {
                handler : function() {
                    var v = this;
                    v.saveTemplateInfo();
                }
            }
            

        }
    });
}