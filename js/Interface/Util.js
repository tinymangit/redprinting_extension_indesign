﻿var AWS = cep_node.require('aws-sdk');
var fs = cep_node.require('fs-extra');
var FsUtils = cep_node.require('nodejs-fs-utils');
var Archiver = cep_node.require('archiver');

// 개발시에만 Access 정보 이곳에서 사용.. 

var s3Client = null;

new CSInterface().evalScript("getAWSConfig()", function(config){
  console.log("config",config)
  try{
    AWS.config.update(JSON.parse(config));
    s3Client = new AWS.S3();
  }catch(e){
    console.log(e)
  }
})

function  generateZipFile(target, destiny, fileName){ 
  var isExistfolder = fs.existsSync(target);  // Links 폴더가 있는지 확인
  var isExistLinkFile = [];

  if(isExistfolder) {  // Links 폴더가 있으면
    isExistLinkFile = fs.readdirSync(target);  // Links 폴더 내에 파일이 있는지 확인 (파일 목록 배열 return)
  }
  
  if(isExistLinkFile.length === 0 || isExistLinkFile == '.DS_Store') {  // Links 폴더 내에 파일이 아예 없거나 .DS_Store 파일만 있는 경우
    return false;
  } else {  // Links 폴더에 파일이 있는 경우
    return new Promise(function(resolve, reject){
      var fullPath = `${destiny}/${fileName}.zip`;  
      var output = fs.createWriteStream(fullPath);  // 아카이브 데이터를 스트리밍할 파일 생성
      var archiver = Archiver('zip', { zlib: { level: 9 } });  // 압축 레벨 설정


      // 데이터 소스가 비워지면 시작
      archiver.on('end', function(){
        fs.readFile(fullPath, function(err, data){
          if (err){
            alert("해당 템플릿경로에 이미지 폴더가 존재하지 않습니다.")
            reject(err);
          }else{
            resolve(data);
          }
        });
      });

      archiver.on('error', function(err){
        reject(err);
      })

      archiver.pipe(output);  // 아카이브 데이터를 파일로 전달
      archiver.directory(target+'/', false);  // 하위 디렉토리에서 파일을 추가하여 그 내용을 아카이브의 루트에 둡니다. (??)
      archiver.finalize(); // 아카이브를 마무리합니다 (즉, 파일 추가가 완료되었지만 스트림은 아직 완료되지 않았습니다) //이 메소드를 호출 한 직후 'close', 'end'또는 'finish'가 시작될 수 있으므로 미리 등록하십시오.

    });
  }

  
}

function isOverLimitSize(dirPath){
  var folderSize = FsUtils.fsizeSync(dirPath);
  var convertSizeToMB = parseInt( folderSize / 1024 / 1024 );
  return convertSizeToMB > 32
}

function createS3Setting(resourcePath , keyValue, resourceType, bucket){

    return {
      Bucket : bucket || "koieditor.files",
      ACL: "public-read",
      Key : keyValue,  // 다운로드링크 뒤에 붙는 주소로 쓰임
      //Body : Buffer.from(cep.fs.readFile(resourcePath, cep.encoding.Base64).data, "base64"),
      Body : fs.createReadStream(resourcePath),
      Metadata: {
        "Content-Type": resourceType || "image/png",
        }
    }
}


function uploadFileToS3(uploadInfo, isUseCDN){
  return new Promise(function(resolve, reject){
    s3Client.putObject(uploadInfo, function(err, data){
      
      if (err){
        reject(err);
      }else{
        if(isUseCDN){
          resolve("https://d28xgx3yndyoh6.cloudfront.net/"+uploadInfo.Key);
        }else{
          resolve("https://s3.ap-northeast-2.amazonaws.com/koieditor.files/"+uploadInfo.Key);
        }
      }
    });
  });
}

function generateUUID() {
  var d = new Date().getTime();
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = (d + Math.random()*16)%16 | 0;
      d = Math.floor(d/16);
      return (c=='x' ? r : (r&0x3|0x8)).toString(16);
  });
  return uuid;
};

function base64ToBlob(base64){
    let raw = window.atob(base64);
    let TypeArray = new Uint8Array(new ArrayBuffer(raw.length));
    for(let i = 0; i < raw.length; i++) {
        TypeArray[i] = raw.charCodeAt(i);
    }
    return new Blob( [TypeArray], {type: 'application/octet-binary'} )
}

function base64ToFile(base64, fileName) {
    
    let blob = base64ToBlob(base64);
    let file = new File( [blob], fileName, {type : setFileType(fileName)} )
    return file;
}
  
// Set File Instance Type
function setFileType(fileName){
  let extension = fileName.split(".")[1];
  let type = ""
  let images = ["svg","png","jpg","jpeg"];
  let json = ["json"];
  let jwt = ["jwt"];
  if (images.includes(extension)){
    if (extension === "svg"){
      type = "image/svg+xml";
    }else{
      type = "image/"+extension;
    }
  }else if(json.includes(extension)){
    type = "application/json";
  }else if(jwt.includes(extension)){
    type = "plain/text";
  }
  return type;
}

function getSplitDocumentToken(){

  return new Promise(function(resolve, reject){

      CSLibrary.evalScript("app.activeDocument.pages[0].extractLabel('TOKEN')", function(token){
        resolve(token);
      });
  })

}